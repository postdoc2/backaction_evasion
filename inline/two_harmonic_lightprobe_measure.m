clc
clear all 
close all

%% constants
s  = 1;
ms = 1e-3;
mus = 1e-6;
ns = 1e-9;
mm = 1e-3;
GHz = 1e9;
nm = 1e-9;
pT = 1e-12;
h    = 6.626070040e-34; % [J*s]
hbar = h/(2*pi);


%%
%%%%%%
% Atoms: (X1, P1) with positive mass and (X2, P2) with negative mass 
% Light: (XL, PL)

% y = (X1, P1, X2, P2, XL, PL)'

% H = +hw/2(X1^2 + P1^2) - hw/2(X1^2 + P2^2) + k*P1PL + k*P2PL 

% After interaction: homodyne detection of XL


dim  = 6;
ndim = 2;
mdim = dim - ndim;


xA1 = unitvec(dim, 1);
pA1 = unitvec(dim, 2);
xA2 = unitvec(dim, 3);
pA2 = unitvec(dim, 4);
xL1 = unitvec(dim, 5);
pL1 = unitvec(dim, 6);

%%%%%%

%T  = 1*ms;
T = 1;
ntimesteps = 1e4;
ts = linspace(0,T,ntimesteps);
dt = ts(2)-ts(1);
nt = numel(ts);

%kt  = sqrt(1.83e6) * sqrt(dt);

%k = sqrt(1.83e6);
k = 0;
kt  = k * sqrt(dt);

%w  = 1/dt;
%w  = 0.0025/dt;
%wt = w*dt;
%wt = 0.01;
%wt = 1*kt;
wt  = 0.2;
 
A0 = eye(dim-ndim, dim-ndim);
B0 = eye(ndim, ndim);
C0 = zeros(dim-ndim, ndim);

m0 = [1; 0; 0; -1];
n0 = [0; 0];


y0 = [m0;
      n0];
  

g0 = [A0  C0; 
      C0' B0];

S = eye(dim,dim) ... 
    + xA1*( + wt * pA1   +  kt * pL1 )'        ... % X1 
    + pA1*( - wt * xA1 )'                     ... % P2
    + xA2*( - wt * pA2   +  kt * pL1 )'        ... % X2
    + pA2*( + wt * xA2 )'                     ... % P2
    + xL1*( + kt * pA1 + kt * pA2)'            ... % XL
    + pL1*( 0 )'                             ... % PL      
    



m = m0;
A = A0;
y = y0;
g = g0;

ys = zeros([size(y0), nt]);
gs = zeros([size(g0), nt]);

pi_vec = diag([1; 0]);

for i = 1:nt
    try
   
    
    ys(:,1,i) = y;
    gs(:,:,i) = g;

    y = [m; n0];
    
    g = [A,   C0; 
         C0', B0];
     
    y = S*y;
    g = S*g*S';
    
    m = y(1:mdim,     :);
    n = y(mdim+1:end, :);
    
    A = g(1:mdim, 1:mdim);
    B = g(mdim+1:end, mdim+1:end);
    C = g(1:mdim, mdim+1:end);
    
    xph_measurement = n(1) + sqrt(0.5)*randn();
    
    mpinv = pinv(pi_vec*B*pi_vec);
    A = A - C*mpinv*C';
    m = m + C*mpinv*[xph_measurement - n(1) ; 0];
    catch
        i
    end
    
end


tiledlayout flow 

nexttile
hold on
plot(ts, squeeze(ys(1,1,:)), 'r-' , 'displayname', '$x_1$')
plot(ts, squeeze(ys(2,1,:)), 'r--', 'displayname', '$p_1$')
plot(ts, squeeze(ys(3,1,:)), 'k-' , 'displayname', '$x_2$')
plot(ts, squeeze(ys(4,1,:)), 'k--', 'displayname', '$p_2$')

leg = legend;
leg.Location = 'bestoutside';

nexttile
hold on
%plot(ts, squeeze(gs(1,1,:)), 'displayname', '$\gamma_{11}$')
%plot(ts, squeeze(gs(2,1,:)), 'displayname', '$\gamma_{21}$')
%plot(ts, squeeze(gs(1,2,:)), 'displayname', '$\gamma_{12}$')

plot(ts, sqrt(squeeze(gs(1,1,:))), 'r-', 'displayname', '$\Delta x_{1}$')
plot(ts, sqrt(squeeze(gs(2,2,:))), 'r--', 'displayname', '$\Delta p_{1}$')
plot(ts, sqrt(squeeze(gs(3,3,:))), 'k-' , 'displayname', '$\Delta x_{2}$')
plot(ts, sqrt(squeeze(gs(4,4,:))), 'k--', 'displayname', '$\Delta p_{2}$')
set(gca,'yscale','log')
leg = legend;
leg.Location = 'bestoutside';

pause(0.001)
autoArrangeFigures(2,2,2)