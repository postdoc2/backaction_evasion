clc
clear all 
close all


%%%%%%
% Atom + light variables

% H = hbar w1 / 2(xA1^2 + pA1^2) + hbar w2 / 2(xA2^2 + pA2^2) + k*xAxL 

% y = [x1; p1; x2; p2]

%%%%%%

T = 3*pi;
dt = 1e-4;
ts = 0:dt:T;
nt = numel(ts);

w1 = 2;
w2 = 1;
m1 = 1;
m2 = 1;
k  = 1;

c = sqrt(m1*m2*w1*w2); 
 

A0 = eye(2,2);
B0 = eye(2,2);
C0 = zeros(2,2);

m0 = [1;0];
n0 = [1;0];


y0 = [m0;
      n0];
  

gamma0 = [A0  C0; 
          C0' B0];



      
S = [   1,      w1*dt,           0,         0;
        -w1*dt,     1,     -k*dt/c,         0;
        0,          0,           1,     w2*dt;
        -k*dt/c,    0,      -w2*dt,         1
        ]
        
 

y     = y0;
gamma = gamma0;

ys = zeros([size(y0), nt]);
gammas = zeros([size(gamma), nt]);

ys(:,1) = y0;
gammas(:,:,1) = gamma0;

for i = 1:numel(ts)-1
 
    
    y = S*y;
    gamma = S*gamma*S';
    
    ys(:,1,i+1) = y;
    gammas(:,:,i+1) = gamma;
    
end


tiledlayout flow 

nexttile
plot(ts, squeeze(ys(1,1,:)),'k-', 'displayname', '$x_1$')
hold on
plot(ts, squeeze(ys(2,1,:)),'k--', 'displayname', '$p_1$')

plot(ts, squeeze(ys(3,1,:)),'r-', 'displayname', '$x_2$')
hold on
plot(ts, squeeze(ys(4,1,:)),'r--', 'displayname', '$p_2$')

nexttile
hold on
plot(ts, squeeze(gammas(1,1,:)), 'displayname', '$\sigma_{11}$')
plot(ts, squeeze(gammas(2,1,:)), 'displayname', '$\sigma_{21}$')
plot(ts, squeeze(gammas(1,2,:)), 'displayname', '$\sigma_{12}$')
plot(ts, squeeze(gammas(2,2,:)), 'displayname', '$\sigma_{22}$')

figure
n_pts = 20;
ha = tight_subplot(1,20,0.001,0.0,0.0);
pause(0.01)
set(gcf,'position',[1925 847 2551 94])

pts = floor(linspace(1,nt,n_pts));

clims = [0 2];
for i = 1:n_pts
   imagesc(ha(i), gammas(:,:, pts(i)), clims)
end
    
    


