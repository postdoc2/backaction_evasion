clc
clear all 
close all

%% constants
s  = 1;
ms = 1e-3;
mm = 1e-3;
GHz = 1e9;
nm = 1e-9;
pT = 1e-12;
h    = 6.626070040e-34; % [J*s]
hbar = h/(2*pi);


%%
%%%%%%
% Atom + light variables

% H = k*pApL 
dim = 4;
x1 = unitvec(dim, 1);
p1 = unitvec(dim, 2);
x2 = unitvec(dim, 3);
p2 = unitvec(dim, 4);

%%%%%%

T  = 5*ms;
ntimesteps = 1e4;
ts = linspace(0,T,ntimesteps);
dt = ts(2)-ts(1);
nt = numel(ts);

%kt  = sqrt(1.83e-6) * sqrt(dt);

k = sqrt(1.83e6);

kt  = k * sqrt(dt);
 
A0 = eye(2,2);
B0 = eye(2,2);
C0 = zeros(2,2);

m0 = [1;0];
n0 = [0;0];


y0 = [m0;
      n0];
  

g0 = [A0  C0; 
      C0' B0];
 
S = eye(dim,dim) + kt * x1*p2' + kt * x2*p1'
   
m = m0;
A = A0;
y = y0;
g = g0;

ys = zeros([size(y0), nt]);
gs = zeros([size(g0), nt]);

pi_vec = diag([1; 0]);

for i = 1:numel(ts)
   
    ys(:,1,i) = y;
    gs(:,:,i) = g;

    y = [m; n0];
    
    g = [A,   C0; 
         C0', B0];
     
    y = S*y;
    g = S*g*S';
    
    m = y(1:2,:);
    n = y(3:4,:);
    
    A = g(1:2,1:2);
    B = g(3:4,3:4);
    C = g(1:2,3:4);
    
    xph_measurement = n(1) + sqrt(0.5)*randn();
    
    mpinv = pinv(pi_vec*B*pi_vec);
    A = A - C*mpinv*C';
    m = m + C*mpinv*[xph_measurement - n(1) ; 0];
    

end


tiledlayout flow 

nexttile
plot(ts, squeeze(ys(1,1,:)), 'displayname', '$x$')
hold on
plot(ts, squeeze(ys(2,1,:)), 'displayname', '$p$')

nexttile
hold on
%plot(ts, squeeze(gs(1,1,:)), 'displayname', '$\gamma_{11}$')
%plot(ts, squeeze(gs(2,1,:)), 'displayname', '$\gamma_{21}$')
%plot(ts, squeeze(gs(1,2,:)), 'displayname', '$\gamma_{12}$')

plot(ts, sqrt(squeeze(gs(1,1,:))), 'displayname', '$\gamma_{11}$')
plot(ts, sqrt(squeeze(gs(2,2,:))), 'displayname', '$\gamma_{22}$')
%set(gca,'yscale','log')
leg = legend;
leg.Location = 'bestoutside';