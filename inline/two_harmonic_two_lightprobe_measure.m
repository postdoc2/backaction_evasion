clc
clear all 
close all

%% constants
s  = 1;
ms = 1e-3;
mus = 1e-6;
ns = 1e-9;
mm = 1e-3;
GHz = 1e9;
nm = 1e-9;
pT = 1e-12;
h    = 6.626070040e-34; % [J*s]
hbar = h/(2*pi);

%%
%%%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%%%%%

dim  = 8;
ndim = 4;
mdim = dim - ndim;

xA1 = unitvec(dim, 1);
pA1 = unitvec(dim, 2);
xA2 = unitvec(dim, 3);
pA2 = unitvec(dim, 4);
xL1 = unitvec(dim, 5);
pL1 = unitvec(dim, 6);
xL2 = unitvec(dim, 7);
pL2 = unitvec(dim, 8);

%% physical parameters


%%%%%%% TIME %%%%%%%%

%T = 5*ms;
%T = 5*mus;
T  = 1*ns;
dt = 1e-14;

ts = 0:dt:T;
nt = numel(ts);

fprintf('T = %.2f ms\ndt = %.2e ms\nnt = %i\n',T/ms, dt/ms, nt)

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1; % p laser
L2_master = 1; % x laser

k = 1 * sqrt(1.83e6);
%k = 1;
kt  = k * sqrt(dt);

k11t = + 1 * kt * L1_master * A1_master; % * pA1 * pL1
k21t = + 1 * kt * L1_master * A2_master; % * pA2 * pL1
k12t = + 1 * kt * L2_master * A1_master; % * xA1 * xL2
k22t = - 1 * kt * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

w_1ns = (2*pi/(1*ns));
w_1mus = (2*pi/(1*mus));
w_1ms = (2*pi/(1*ms));
w_5ms = 5 * w_1ms;

%wt = 0.006 * (dt/1e-7);
%wt = kt;
%wt = 5*(2*pi/T * dt);
%wt = 0.002 * sqrt(dt/1e-7);

w = 1*w_1ns;
%w = 1*w_1mus;
%w = 5*w_1ms;
%w  = 5*(2*pi/T);

%wt  = w * dt;
wt  = 1.42*kt; % about 1.42 changes the squeezing @ T = 1*ns, dt = 1e-14, k = 1 * sqrt(1.83e6);

w1t = + 1*wt;
w2t = - 1*wt;

fprintf('kt = %.2e \nwt = %.2e\n', kt, wt)

phase_90deg_time = (pi/2)/w;
nt_phase_90deg_time = phase_90deg_time/dt;

%%%%%%% INITIAL STATE  %%%%%%%%

initial_conditions = 3;

if initial_conditions == 0
    
    m0 = zeros(4,1);
    
elseif initial_conditions == 1
    
    m0 = rand(4,1);
    
else
    
    m0 = [1; 0; -1; 0];
    
end


%%%%%%% MEASUREMENT RNG %%%%%%%%

seed_num = 2; % 0: random 



%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

A0 = eye(dim-ndim, dim-ndim);
rL1 = 1;
rL2 = 1;
B0 = diag([1/rL1, rL1, 1/rL2, rL2]);
C0 = zeros(dim-ndim, ndim);

n0 = [0; 0; 0; 0];

y0 = [m0;
      n0];
  

g0 = [A0  C0; 
      C0' B0];

Scst =  ... 
    + xA1 * ( xA1  +  w1t * pA1  +  k11t * pL1 )'   ... % XA1 
    + pA1 * ( pA1  -  w1t * xA1  -  k12t * xL2 )'   ... % PA1
    + xA2 * ( xA2  +  w2t * pA2  +  k21t * pL1 )'   ... % XA2
    + pA2 * ( pA2  -  w2t * xA2  -  k22t * xL2 )'   ... % PA2
    + xL1 * ( xL1  +  k11t * pA1 +  k21t * pA2 )'   ... % XL1
    + pL1 * ( pL1 )'                                ... % PL1     
    + xL2 * ( xL2 )'                                ... % XL2
    + pL2 * ( pL2  -  k12t * xA1 -  k22t * xA2  )'  ... % PL2

S = Scst;

St = @(k11t, k12t, k21t, k22t)  ... 
    + xA1 * ( xA1  +  w1t * pA1  +  k11t * pL1 )'   ... % XA1 
    + pA1 * ( pA1  -  w1t * xA1  -  k12t * xL2 )'   ... % PA1
    + xA2 * ( xA2  +  w2t * pA2  +  k21t * pL1 )'   ... % XA2
    + pA2 * ( pA2  -  w2t * xA2  -  k22t * xL2 )'   ... % PA2
    + xL1 * ( xL1  +  k11t * pA1 +  k21t * pA2 )'   ... % XL1
    + pL1 * ( pL1 )'                                ... % PL1     
    + xL2 * ( xL2 )'                                ... % XL2
    + pL2 * ( pL2  -  k12t * xA1 -  k22t * xA2  )'  ... % PL2


m = m0;
A = A0;
y = y0;
g = g0;


ys = zeros([size(y0), nt]);
gs = zeros([size(g0), nt]);

pi_vec = diag([1; 0; 0; 1]);

ys(:,1,1) = y;
gs(:,:,1) = g;

if seed_num ~= 0
    rng(seed_num)
end

xL1_measurements = randn(1,nt);
pL2_measurements = randn(1,nt);

for i = 1:nt-1
    
    if mod(i/100000, 1) == 0
        fprintf('done: %i pct.\n', round(i/nt*100))
    end
        
    try        
%        
%          if i < nt * 0.25
%              S = Scst;
%          elseif i < nt * 0.5
%              S = St(0,0,0,0);
%          elseif i < nt * 0.75
%              S = Scst;
%          else
%              S = St(0,0,0,0);
%          end
        
        y = S*y;
        g = S*g*S';

        m = y(1:mdim,     :);
        n = y(mdim+1:end, :);

        A = g(1:mdim, 1:mdim);
        B = g(mdim+1:end, mdim+1:end);
        C = g(1:mdim, mdim+1:end);

        xL1_measurement = n(1) + sqrt(0.5)*xL1_measurements(i);
        pL2_measurement = n(4) + sqrt(0.5)*pL2_measurements(i);

        mpinv = pinv(pi_vec*B*pi_vec);
        A = A - C*mpinv*C';
        m = m + C*mpinv*[xL1_measurement - n(1) ; 0; 0; pL2_measurement - n(4)];

        y = [m; n0];

        g = [A,   C0; 
             C0', B0];

        ys(:,1,i+1) = y;
        gs(:,:,i+1) = g;
    
    catch
        
        disp('error at')
        i
        ts(i)
        break
    end
    
end

%%%%%%%%% PLOT %%%%%%%%%%%%

figure

normalize = 1;
logx = 1;

if ~normalize
    normalization = 1; 
else
    normalization = T; 
end
ts_norm = ts/normalization;


tiledlayout flow 

nexttile
hold on

m1 = squeeze(ys(1,1,:));
m2 = squeeze(ys(2,1,:));
m3 = squeeze(ys(3,1,:));
m4 = squeeze(ys(4,1,:));

plot(ts_norm, m1, 'k-' , 'displayname', '$\langle x_1 \rangle$')
plot(ts_norm, m2, 'k--', 'displayname', '$\langle p_1 \rangle$')
plot(ts_norm, m3, 'r-' , 'displayname', '$\langle x_2 \rangle$')
plot(ts_norm, m4, 'r--', 'displayname', '$\langle p_2 \rangle$')

if logx
    set(gca,'xscale','log')
end

leg = legend;
leg.Location = 'bestoutside';

nexttile
hold on
%plot(ts, squeeze(gs(1,1,:)), 'displayname', '$\gamma_{11}$')
%plot(ts, squeeze(gs(2,1,:)), 'displayname', '$\gamma_{21}$')
%plot(ts, squeeze(gs(1,2,:)), 'displayname', '$\gamma_{12}$')

varxA1 = 0.5*squeeze(gs(1,1,:));
varpA1 = 0.5*squeeze(gs(2,2,:));
varxA2 = 0.5*squeeze(gs(3,3,:));
varpA2 = 0.5*squeeze(gs(4,4,:));


plot(ts_norm, sqrt(varxA1), 'r-', 'displayname', '$\Delta x_{1}$')
plot(ts_norm, sqrt(varpA1), 'r--', 'displayname', '$\Delta p_{1}$')
plot(ts_norm, sqrt(varxA2), 'k-' , 'displayname', '$\Delta x_{2}$')
plot(ts_norm, sqrt(varpA2), 'k--', 'displayname', '$\Delta p_{2}$')

set(gca,'yscale','log')
leg = legend;
leg.Location = 'bestoutside';
xlabel('t (s)')

nexttile
hold on
plot(ts_norm, m1 - m3, 'g-' , 'displayname', '$\langle x_1-x_2 \rangle$')
plot(ts_norm, m2 + m4, 'b--', 'displayname', '$\langle p_1+p_2 \rangle$')
try
    plot(ts_norm, circshift(m2 + m4, nt_phase_90deg_time), 'y--', 'displayname', '$\langle p_1+p_2 \rangle$ shifted $\pi$/2')
catch
end
leg = legend;
leg.Location = 'bestoutside';


nexttile
hold on

varxA1xA2 = 0.5*squeeze(gs(1,3,:));
varxA2xA1 = 0.5*squeeze(gs(3,1,:));
varpA1pA2 = 0.5*squeeze(gs(2,4,:));
varpA2pA1 = 0.5*squeeze(gs(4,2,:));


relXVar = varxA1 + varxA2 - (varxA1xA2 + varxA2xA1 );
relPVar = varpA1 + varpA1 + (varpA1pA2 + varpA2pA1 );

plot(ts_norm, sqrt(relXVar), 'g-',  'displayname', '$\Delta (x_{A_1}-x_{A_2})$')
plot(ts_norm, sqrt(relPVar), 'b--', 'displayname', '$\Delta (p_{A_1}+p_{A_2})$')

set(gca,'yscale','log')
leg = legend;
leg.Location = 'bestoutside';

sgtitle( [
    sprintf('T = %.2e s, dt = %.2e s, nt = %i\n',T, dt, nt) ...
    sprintf('$\\omega_{1t}$ = %.2e, $\\omega_{2t}$ = %.2e\n', w1t, w2t) ...
    sprintf('$p_{A_i} p_{L_1}$: $\\kappa_{11t}$ = %.2e, $\\kappa_{21t}$ = %.2e\n', k11t, k21t)  ...
    sprintf('$x_{A_i} x_{L_2}$: $\\kappa_{12t}$ = %.2e, $\\kappa_{22t}$ = %.2e', k12t, k22t)  ...
    %$x_{A_i} x_{L_2}$: $\\kappa_{12t}$ = %.2f, $\\kappa_{22t}$ = %.2f', k11t, k21t, k12t, k22t)
    %sprintf('$p_{A_i} p_{L_1}$: $\\kappa_{11t}$ = %.2f, $\\kappa_{21t}$ = %.2f | $x_{A_i} x_{L_2}$: $\\kappa_{12t}$ = %.2f, $\\kappa_{22t}$ = %.2f', k11t, k21t, k12t, k22t)
],'interpreter','latex','fontsize',18)

pause(0.001)
autoArrangeFigures(2,2,3)

