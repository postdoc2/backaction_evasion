clc
clear all 
close all


%%%%%%
% Atom + light variables

% H = hbar w / 2(xA^2 + pA^2) + k*xAxL 
dim = 4;
x1 = unitvec(dim, 1);
p1 = unitvec(dim, 2);
x2 = unitvec(dim, 3);
p2 = unitvec(dim, 4);



%%%%%%

T = pi;
dt = 1e-4;
ts = 0:dt:T;
nt = numel(ts);

omega = 2;
m = 1;
k  = 1;
 

A0 = eye(2,2);
B0 = eye(2,2);
C0 = zeros(2,2);

m0 = [1;0];
n0 = [0;0];


y0 = [m0;
      n0];
  

gamma0 = [A0  C0; 
          C0' B0];


S = [   1,               omega*dt,     0,                 0;
       -omega*dt,        1       ,    -k*dt/(m*omega),    0;
        0,               0       ,     1,                 0;
       -k*dt/(m*omega),  0       ,     0,                 1]
 
S2 = eye(dim,dim) ... 
    + omega*dt       * x1*p1' ...
    - omega*dt       * p1*x1' ...
    - k*dt/(m*omega) * p1*x2' ...
    - k*dt/(m*omega) * p2*x1' 
   
   
y     = y0;
gamma = gamma0;

ys = zeros([size(y0), nt]);
gammas = zeros([size(gamma), nt]);

for i = 1:numel(ts)
   
   % y(3:4) = n0;
   % gamma(3:4,3:4) = B0;
   % gamma(1:2,3:4) = C0';
   % gamma(3:4,1:2) = C0;
    
    
    
    y = S*y;
    gamma = S*gamma*S';
    
    ys(:,1,i) = y;
    gammas(:,:,i) = gamma;
    
end


tiledlayout flow 

nexttile
plot(ts, squeeze(ys(1,1,:)), 'displayname', '$x$')
hold on
plot(ts, squeeze(ys(2,1,:)), 'displayname', '$p$')

nexttile
hold on
plot(ts, squeeze(gammas(1,1,:)), 'displayname', '$\sigma_{11}$')
plot(ts, squeeze(gammas(2,1,:)), 'displayname', '$\sigma_{21}$')
plot(ts, squeeze(gammas(1,2,:)), 'displayname', '$\sigma_{12}$')
plot(ts, squeeze(gammas(2,2,:)), 'displayname', '$\sigma_{22}$')