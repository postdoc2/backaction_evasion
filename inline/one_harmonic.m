clc
clear all 
close all

% Single oscillator [x,p] = i

T = pi;
dt = 1e-4;
ts = 0:dt:T;

nt = numel(ts);

omega = 2;
m = 1;

y0 = [1;
      0];
  
gamma0 = eye(2,2);


S = [1,         omega*dt; 
     -omega*dt, 1       ];
 

y     = y0;
gamma = gamma0;

ys = zeros([size(y0), nt]);
gammas = zeros([size(gamma), nt]);

for i = 1:numel(ts)
   
    y = S*y;
    gamma = S*gamma*S';
    
    ys(:,1,i) = y;
    gammas(:,:,i) = gamma;
    
end


tiledlayout flow 

nexttile
plot(ts, squeeze(ys(1,1,:)), 'displayname', '$x$')
hold on
plot(ts, squeeze(ys(2,1,:)), 'displayname', '$p$')

nexttile
hold on
plot(ts, squeeze(gammas(1,1,:)), 'displayname', '$\sigma_{11}$')
plot(ts, squeeze(gammas(2,1,:)), 'displayname', '$\sigma_{21}$')
plot(ts, squeeze(gammas(1,2,:)), 'displayname', '$\sigma_{12}$')
plot(ts, squeeze(gammas(2,2,:)), 'displayname', '$\sigma_{22}$')