function plotting(r, normalize, logx, varargin)
% show options:
% {'mean', 'meanrel', 'meaninc', 'var', 'varrel', 'varreldiff', 'fpmean', 'fpvar'}

figure

if ~normalize
    normalization = 1; 
else
    normalization = r.T; 
end
ts_norm = r.ts/normalization;

tiledlayout flow 




if numel(varargin) > 0
    show = @(var) any(strcmp(varargin{:}, var));
else
    show = @(var) false;
    warning('No plots chosen in varargin {}')
end

%% MEANS 

if show('mean')
    nexttile
    hold on

    plot(ts_norm, r.m1, 'k-' , 'displayname', '$\langle x_1 \rangle$')
    plot(ts_norm, r.m2, 'k--', 'displayname', '$\langle p_1 \rangle$')
    plot(ts_norm, r.m3, 'r-' , 'displayname', '$\langle x_2 \rangle$')
    plot(ts_norm, r.m4, 'r--', 'displayname', '$\langle p_2 \rangle$')

    if (r.k11 == r.k12 && r.k12 == r.k21 && r.k21 == - r.k22) && (r.w1t == - r.w2t) && (r.varxA1(1) == r.varpA1(1) && r.varpA1(1) == r.varxA2(1) && r.varxA2(1) == r.varpA2(1))
        plot(ts_norm, r.m1_ana, 'k:' , 'displayname', '$\langle x_1 \rangle_{ana}$')
        plot(ts_norm, r.m2_ana, 'k:' , 'displayname', '$\langle p_1 \rangle_{ana}$')
        plot(ts_norm, r.m3_ana, 'k:' , 'displayname', '$\langle x_2 \rangle_{ana}$')
        plot(ts_norm, r.m4_ana, 'k:' , 'displayname', '$\langle p_2 \rangle_{ana}$')

        %title('err')
        err_m1 = sum(abs(r.m1 - r.m1_ana))*r.dt
        err_m2 = sum(abs(r.m2 - r.m2_ana))*r.dt
        err_m3 = sum(abs(r.m3 - r.m3_ana))*r.dt
        err_m4 = sum(abs(r.m4 - r.m4_ana))*r.dt
    end

    if logx
        set(gca,'xscale','log')
    end

    leg = legend;
    leg.Location = 'bestoutside';
end

%% RELATIVE MEANS %%
if show('meanrel')
    nexttile
    hold on
    plot(ts_norm, r.m1 - r.m3, 'g-' , 'displayname', '$\langle x_1-x_2 \rangle$')
    plot(ts_norm, r.m2 + r.m4, 'b--', 'displayname', '$\langle p_1+p_2 \rangle$')

    
    if r.k11 == r.k12 && r.k21 == -r.k22 && r.w1 == -r.w2 && (r.varxA1(1) == r.varpA1(1) && r.varpA1(1) == r.varxA2(1) && r.varxA2(1) == r.varpA2(1))
        plot(ts_norm, r.mxm, 'k:' , 'displayname', '$\langle x_1-x_2 \rangle_{ana}$')
        plot(ts_norm, r.mpp, 'k:' , 'displayname', '$\langle p_1+p_2 \rangle_{ana}$')
    end
    
    yyaxis right
    plot(ts_norm, r.m1 + r.m3, 'c-' , 'displayname', '$\langle x_1+x_2 \rangle$')
    plot(ts_norm, r.m2 - r.m4, 'y--', 'displayname', '$\langle p_1-p_2 \rangle$')

    if logx
        set(gca,'xscale','log')
    end

    leg = legend;
    leg.Location = 'bestoutside';
end

%% MEAN INCREMENT SCALING %%

if show('meaninc')
    nexttile
    hold on

    dmxm_num = ((diff(r.m1-r.m3)));
    dmpp_num = ((diff(r.m2+r.m4)));

    plot(ts_norm, [0 dmxm_num],'displayname','$\delta m_{x_-}$ num')
    plot(ts_norm, [0 dmpp_num],'displayname','$\delta m_{p_+}$ num')
    

    if r.k11 == r.k12 && r.k21 == -r.k22 && r.w1 == -r.w2 && (r.varxA1(1) == r.varpA1(1) && r.varpA1(1) == r.varxA2(1) && r.varxA2(1) == r.varpA2(1))
        plot(ts_norm, [0 r.dmxm_ana],':','displayname','$\delta m_{x_-}$ ana')
        plot(ts_norm, [0 r.dmpp_ana],':','displayname','$\delta m_{p_+}$ ana')
    end
    
    if r.k11 == r.k12 && r.k21 == -r.k22 && r.k11 == r.k21
        plot(ts_norm, sqrt(r.dt)/r.k*1./(r.ts),'displayname','$\frac{\sqrt{\delta t}}{k} \frac{1}{t}$')
        title(['errs - $\delta m_{x_-}:$ ' num2str(sum(abs(dmxm_num - r.dmxm_ana))*r.dt,2) ', $\delta m_{p_+}:$ ' num2str(sum(abs(dmpp_num - r.dmpp_ana))*r.dt,2)])
    end
    
    set(gca,'yscale','log')
    set(gca,'xscale','log')

    leg = legend;
    leg.Location = 'bestoutside';
end

%% X,P VARIANCES %%%%%%%%
if show('var')
    nexttile
    hold on

    plot(ts_norm, sqrt(r.varxA1), 'k-', 'displayname', '$\Delta x_{1}$')
    plot(ts_norm, sqrt(r.varpA1), 'k--', 'displayname', '$\Delta p_{1}$')
    plot(ts_norm, sqrt(r.varxA2), 'r-' , 'displayname', '$\Delta x_{2}$')
    plot(ts_norm, sqrt(r.varpA2), 'r--', 'displayname', '$\Delta p_{2}$')

    set(gca,'yscale','log')
    set(gca,'xscale','log')
    leg = legend;
    leg.Location = 'bestoutside';
    xlabel('$t/T$')
end


%% RELATIVE VARIANCES %%
if show('varrel')
    nexttile
    hold on

    plot(ts_norm, sqrt(r.relXVar), 'g-',  'displayname', '$\Delta (x_{A_1}-x_{A_2})$')
    plot(ts_norm, sqrt(r.sumPVar), 'b--', 'displayname', '$\Delta (p_{A_1}+p_{A_2})$')
    
    if r.w1 == -r.w2 && r.k11 == r.k21 && r.k12 == -r.k22 && r.gammabp == 0 && r.sigmabp == 0 && r.cp == 0
        plot(ts_norm, sqrt(r.relXVar_ana), 'g:',  'displayname', '$\Delta (x_{A_1}-x_{A_2})_\mathrm{ana}$')
        plot(ts_norm, sqrt(r.sumPVar_ana), 'b:',  'displayname', '$\Delta (p_{A_1}+p_{A_2})_\mathrm{ana}$')
    end
    set(gca,'yscale','log')
    set(gca,'xscale','log')

    try
    %    xline(pi/(2*r.w))
    catch

    end

    yyaxis right
    %plot(ts_norm, sqrt(r.sumXVar), 'c-',  'displayname', '$\Delta (x_{A_1}+x_{A_2})$')
    %plot(ts_norm, sqrt(r.relPVar), 'y--', 'displayname', '$\Delta (p_{A_1}-p_{A_2})$')
    %plot(ts_norm, sqrt(r.sumXVar_ana), 'c:',  'displayname', '$\Delta (x_{A_1}+x_{A_2})_\mathrm{ana}$')
    %plot(ts_norm, sqrt(r.relPVar_ana), 'y:',  'displayname', '$\Delta (p_{A_1}-p_{A_2})_\mathrm{ana}$')
    set(gca,'yscale','log')
    set(gca,'xscale','log')

    leg = legend;
    leg.Location = 'bestoutside';
end
%% RELATIVE VARIANCES DIFF %%
if show('varreldiff')
    nexttile
    hold on
    %err_fun = @(ana,num) abs(1-ana./num) 
    err_fun = @(ana,num) abs(num-ana);

    if r.w1 == -r.w2 && r.k11 == r.k21 && r.k12 == -r.k22 && r.gammabp == 0 && r.sigmabp == 0 && r.cp == 0
        plot(ts_norm, err_fun(r.relXVar, r.relXVar_ana), 'g-',  'displayname', '$\Delta (x_{A_1}-x_{A_2})^2$')
        plot(ts_norm, err_fun(r.sumPVar, r.sumPVar_ana), 'b--',  'displayname', '$\Delta (p_{A_1}+p_{A_2})^2$')
    end
    yyaxis right
%    plot(ts_norm, err_fun(r.sumXVar, r.sumXVar_ana), 'c:',  'displayname', '$\Delta (x_{A_1}+x_{A_2})^2$')
%    plot(ts_norm, err_fun(r.relPVar, r.relPVar_ana), 'y:',  'displayname', '$\Delta (p_{A_1}-p_{A_2})^2$')
    title('num-ana')
    leg = legend;
    leg.Location = 'bestoutside';
end

%% FORCE PLOTS IF EXIST
A0_string = sprintf('diag A(0) = (%.2f, %.2f, %.2f, %.2f)\n',r.gs(1,1), r.gs(2,2), r.gs(3,3), r.gs(4,4));

if any(strcmp(fieldnames(r),'m6'))

    A0_string = sprintf('diag A(0) = (%.2f, %.2f, %.2f, %.2f, %.2f, %.2f)\n',r.gs(1,1), r.gs(2,2), r.gs(3,3), r.gs(4,4), r.gs(5,5), r.gs(6,6));
    
    if show('fxmean')
        nexttile
        hold on 
        plot(r.ts, r.fxs,'displayname', '$f_x$')
        xlabel('$t$')
        plot(r.ts, r.m5,'displayname', '$m_{fx}$')

        if any(strcmp(fieldnames(r), 'mFx_PQS'))
            plot(r.ts, r.mFx_PQS, 'displayname', '$m_{fx}$ PQS')
       %     plot(r.ts, r.mEs(5,:),'displayname', '$m_E$ PQS')
        end
        
        title('$x_{A_1} \rightarrow  x_{A_1} + (c_x\delta t) f_x$')
        leg = legend;
        leg.Location = 'bestoutside';
    end
    
    if show('fpmean')
        nexttile
        hold on 
        plot(r.ts, r.fps,'displayname', '$f_p$')
        xlabel('$t$')
        plot(r.ts, r.m6,'displayname', '$m_{fp}$')

        if any(strcmp(fieldnames(r), 'mFp_PQS'))
            plot(r.ts, r.mFp_PQS, 'displayname', '$m_{fp}$ PQS')
       %     plot(r.ts, r.mEs(5,:),'displayname', '$m_E$ PQS')
        end
        
        title('$p_{A_1} \rightarrow p_{A_1} + (c_p\delta t) f_p$')
        leg = legend;
        leg.Location = 'bestoutside';
    end
    
    
    factor_inv_ts = 1e-2;
    factor_inv_ts_cubed = 1e-6;
    
    if show('fxvar')
        nexttile


        
        loglog(r.ts, sqrt(r.varFx), 'displayname', '$\Delta f_x(t)$'); hold on;

        if any(strcmp(fieldnames(r), 'varFx_PQS'))
            loglog(r.ts, sqrt(r.varFx_PQS), 'displayname', '$\Delta f_x(t)$ PQS')
        end
       
        
        loglog(r.ts, sqrt(1./r.ts) * factor_inv_ts,'--', 'linewidth', 1.5, 'displayname','$\propto\frac{1}{t}$'); hold on
        loglog(r.ts, sqrt(1./r.ts.^3) * factor_inv_ts_cubed,'--', 'linewidth', 1.5,'displayname','$\propto\frac{1}{t^3}$'); hold on
        
        xlabel('$t$')
        leg = legend;
        leg.Location = 'bestoutside';

        ylim([1e-3, 1])
    end
    
    
    if show('fpvar')
        nexttile


        loglog(r.ts, sqrt(r.varFp), 'displayname', '$\Delta f_p(t)$'); hold on

        if any(strcmp(fieldnames(r), 'varFp_PQS'))
            loglog(r.ts, sqrt(r.varFp_PQS), 'displayname', '$\Delta f_p(t)$ PQS')
        end
        
        loglog(r.ts, sqrt(1./r.ts) * factor_inv_ts,'--', 'linewidth', 1.5, 'displayname','$\propto\frac{1}{t}$'); hold on
        loglog(r.ts, sqrt(1./r.ts.^3) * factor_inv_ts_cubed,'--', 'linewidth', 1.5,'displayname','$\propto\frac{1}{t^3}$'); hold on
        
        if r.sigmabp == 0 && r.w1 == 0 && r.w2 == 0 && r.k11 == r.k21 && r.k12 == -r.k22
            
            plot(r.ts, sqrt(r.varFp_ana),'--', 'displayname', '$\Delta f_p(t)_\mathrm{ana}$')
            
            if r.gammabp == 0
                plot(r.ts, sqrt(6./(r.cp^2*r.k1^2*r.ts.^3)),'displayname', '$\left(\frac{6}{(c^2 k_1^2)}\right) \frac{1}{t^3}$','linewidth',1);
            else
                plot(r.ts, sqrt((1/(1/r.varFp(1) + r.cp^2/(2*r.gammabp^3) * (r.gammabp + 2*r.k1^2)))*exp(-2*r.gammabp*r.ts)),'displayname', '$\left(\frac{1}{\frac{1}{\Delta f^2(0)} + \frac{c^2}{2\gamma_b^3}(\gamma_b + 2k_1^2)}\right)\exp(-2\gamma_b t)$','linewidth',1); 
            end
        end
        
        xlabel('$t$')
        leg = legend;
        leg.Location = 'bestoutside';

        ylim([1e-3, 1])

        %plot(r.ts, sqrt((r.w/(r.c*r.k))^2 * 1./r.ts),'displayname','$\left(\frac{\omega}{c k}\right)^2 \frac{1}{t}$')

        %plot(r.ts, sqrt(1./r.ts.^3),'displayname','$\frac{1}{t^3}$')

        %limsy=get(gca,'YLim');
        %set(gca,'Ylim',[limsy(1) 2]);
    end
end


%% TITLE
the_title = [
    sprintf('T = %.2e s, dt = %.2e s, nt = %i\n',r.T, r.dt, r.nt) ...
    A0_string ...
    sprintf('$x_{A_i}^2 + p_{A_i}^2$: $~ \\omega_{1}$ = %.2e,  $\\omega_{2}$ = %.2e\n', r.w1, r.w2) ...
    sprintf('$p_{A_i} p_{L_1} ~ ~ $ :  $\\kappa_{11}$ = %.2e, $\\kappa_{21}$ = %.2e\n', r.k11, r.k21)  ...
    sprintf('$x_{A_i} x_{L_2} ~ ~ ~ $ :  $\\kappa_{12}$ = %.2e, $\\kappa_{22}$ = %.2e', r.k12, r.k22)  ...
];


if any(strcmp(fieldnames(r),'m6'))
    the_title = [the_title ...
        sprintf('\n$c_x$ = %.2e, $\\gamma_x$ = %.2e, $\\sigma_x$ = %.2e', r.cx, r.gammabx, r.sigmabx) ...
        sprintf('\n$c_p$ = %.2e, $\\gamma_p$ = %.2e, $\\sigma_p$ = %.2e', r.cp, r.gammabp, r.sigmabp) ];
end

sgtitle(the_title ,'interpreter','latex','fontsize',18)

end



