function r=analytical_forms(r)

warning('MOMENTS IN THESE EQUATIONS DO NOT YET ACCOUNT FOR 0.5 CONVENTION')

A11 = squeeze(r.gs(1,1,:))';
A22 = squeeze(r.gs(2,2,:))';
A33 = squeeze(r.gs(3,3,:))';
A44 = squeeze(r.gs(4,4,:))';
A55 = squeeze(r.gs(5,5,:))';

A13 = squeeze(r.gs(1,3,:))';
A31 = squeeze(r.gs(3,1,:))';
A24 = squeeze(r.gs(2,4,:))';
A42 = squeeze(r.gs(4,2,:))';

    if 0 % currently not used
        % w=0, k11 = k12 = k21 = -k22 = k
        r.relXVar_ana = 1./(2*r.k2^2*r.ts + 1/(r.varxA1(1) + r.varxA2(1)));
        %r.relPvar_ana = (r.varxA1 + r.varxA2 + 2*r.k^2*r.ts )
        r.sumPVar_ana = 1./(2*r.k1^2*r.ts + 1/(r.varpA1(1) + r.varpA2(1))); % THIS ONE IS THE OLD RESULT WITH SAME K AND ZERO W

        %n1 = (1 + A11(1)*A33(1))*r.k^2*r.ts + (A11(1) + A33(1))*r.k^4*r.ts.^2;
        %d1 = 1 + (A11(1) + A33(1))*r.k^2*r.ts;
        %r.sumXVar_ana = (r.varxA1 + r.varxA2)./d1 + 2*n1./d1; % OK
        %r.sumXVar_ana = r.relXVar_ana + 2*A11(1)*A33(1)*r.k^2*r.ts/(1+(A11(1) + A33(1))*r.k^2*r.ts) + 2*r.k^2*r.ts; % OK
        r.sumXVar_ana = r.relXVar_ana + 4*r.varxA1(1)*r.varxA2(1)./(1./(2*r.k2^2*r.ts) + r.varxA1(1) + r.varxA2(1)) + 2*r.k1^2*r.ts; % OK

        %n2 = (1 + A22(1)*A44(1))*r.k^2*r.ts + (A22(1) + A44(1))*r.k^4*r.ts.^2;
        %d2 = 1 + (A22(1) + A44(1))*r.k^2*r.ts;
        %r.relPVar_ana = (r.varpA1 + r.varpA2)./d2 + 2*n2./d2; % OK
        r.relPVar_ana = r.sumPVar_ana + 4*r.varpA1(1)*r.varpA2(1)./(1./(2*r.k1^2*r.ts) + r.varpA1(1) + r.varpA2(1)) + 2*r.k2^2*r.ts;  % OK
    end 

    %% Analytical forms for w≠0, k11=k12=k1, k21=-k22=k2, and no force
    
    if r.w1 == -r.w2 && r.k11 == r.k21 && r.k12 == -r.k22 && r.gammabp == 0 && r.sigmabp == 0 && r.cp == 0 && r.gammabx == 0 && r.sigmabx == 0 && r.cx == 0

        if r.w == 0
            sinwt_w   = r.ts;
            sin2wt_2w = r.ts;
        else
            sinwt_w = sin(r.w*r.ts)/r.w;
            sin2wt_2w = sin(2*r.w*r.ts)/(2*r.w);
        end
        %nx = (A11(1) + A33(1)) + (A22(1) + A44(1) - A11(1) - A33(1))*sin(r.w*r.ts).^2 + 0.5*(A11(1) + A33(1))*(A22(1) + A44(1))*( (r.k1^2 + r.k2^2)*r.ts + cos(r.w*r.ts).*sinwt_w*(r.k1^2-r.k2^2)); % WORKS
        %np = (A22(1) + A44(1)) + (A11(1) + A33(1) - A22(1) - A44(1))*sin(r.w*r.ts).^2 + 0.5*(A11(1) + A33(1))*(A22(1) + A44(1))*( (r.k1^2 + r.k2^2)*r.ts + cos(r.w*r.ts).*sinwt_w*(r.k2^2-r.k1^2)); % WORKS
        %d = 2*( (1 + 0.5*(A11(1)+A33(1))*(r.k2^2 + r.k1^2)*r.ts ).*(1 + 0.5*(A22(1)+A44(1))*(r.k2^2 + r.k1^2)*r.ts )  + 0.5*(A11(1)+A33(1))*0.5*(-A22(1)-A44(1)).*sinwt_w.^2 * (r.k2^2 - r.k1^2)^2 + cos(r.w*r.ts).*sinwt_w.*(0.5*(A11(1)+A33(1)) - 0.5*(A22(1)+A44(1)))*(r.k2^2 - r.k1^2) ); % WORKS

        Var_mu1 = 0.5*(A11(1) + A33(1));
        Var_mu2 = 0.5*(A22(1) + A44(1));
        Kp = r.k2^2 + r.k1^2;
        Km = r.k2^2 - r.k1^2;
        %nx = Var_mu1 + (Var_mu2 - Var_mu1)*sin(r.w*r.ts).^2 + Var_mu1 * Var_mu2*( Kp*r.ts - cos(r.w*r.ts).*sinwt_w*Km );
        %np = Var_mu2 + (Var_mu1 - Var_mu2)*sin(r.w*r.ts).^2 + Var_mu1 * Var_mu2*( Kp*r.ts + cos(r.w*r.ts).*sinwt_w*Km );
        nx = Var_mu1 + (Var_mu2 - Var_mu1)*sin(r.w*r.ts).^2 + Var_mu1 * Var_mu2*( Kp*r.ts - sin2wt_2w*Km );
        np = Var_mu2 + (Var_mu1 - Var_mu2)*sin(r.w*r.ts).^2 + Var_mu1 * Var_mu2*( Kp*r.ts + sin2wt_2w*Km );
        %d =  (1 + Var_mu1*Kp*r.ts ).*(1 + Var_mu2*Kp*r.ts ) + sinwt_w * Km .* (cos(r.w*r.ts) * (Var_mu1 - Var_mu2) - sinwt_w * Km * Var_mu1 * Var_mu2);
        %d =  (1 + Var_mu1*Kp*r.ts ).*(1 + Var_mu2*Kp*r.ts ) + cos(r.w*r.ts).*sinwt_w * Km * (Var_mu1 - Var_mu2) - sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2;
        d =  (1 + Var_mu1*Kp*r.ts ).*(1 + Var_mu2*Kp*r.ts ) + sin2wt_2w * Km * (Var_mu1 - Var_mu2) - sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2;
        % Works, but some inaccuracy wrt. numerics because of finite dt

        r.relXVar_ana = nx./d;
        r.sumPVar_ana = np./d;
        %%%%%%%%%%%%%%%%%%

        %n1 = (1 + A11(1)*A33(1))*r.k^2*r.ts + (A11(1) + A33(1))*r.k^4*r.ts.^2;
        %n2 = (1 + A22(1)*A44(1))*r.k^2*r.ts + (A22(1) + A44(1))*r.k^4*r.ts.^2;
        %d1 = (1 + (A11(1) + A33(1))*r.k^2*r.ts);
        %d2 = (1 + (A22(1) + A44(1))*r.k^2*r.ts);

        n1 = (r.k1^2 + A11(1)*A33(1)*r.k2^2)*r.ts + (A11(1) + A33(1))*r.k1^2*r.k2^2*r.ts.^2;
        n2 = (r.k2^2 + A22(1)*A44(1)*r.k1^2)*r.ts + (A22(1) + A44(1))*r.k1^2*r.k2^2*r.ts.^2;
        d1 = (1 + (A11(1) + A33(1))*r.k2^2*r.ts);
        d2 = (1 + (A22(1) + A44(1))*r.k1^2*r.ts);


        varxA1_ana = 0.5 * (A11(1) + n1)./d1;
        varpA1_ana = 0.5 * (A22(1) + n2)./d2;
        varxA2_ana = 0.5 * (A33(1) + n1)./d1;
        varpA2_ana = 0.5 * (A44(1) + n2)./d2;

        varxA1xA2_ana = +0.5*n1./d1;
        varxA2xA1_ana = varxA1xA2_ana;

        varpA1pA2_ana = -0.5*n2./d2;
        varpA2pA1_ana = varpA1pA2_ana;

            
        % calculate means only if same initial covar 
        if (r.varxA1(1) == r.varpA1(1) && r.varpA1(1) == r.varxA2(1) && r.varxA2(1) == r.varpA2(1))
            [m1_ana, m2_ana, m3_ana, m4_ana] = deal(zeros(size(r.m1)));
            m1_ana(1) = r.m1(1);
            m2_ana(1) = r.m2(1);
            m3_ana(1) = r.m3(1);
            m4_ana(1) = r.m4(1);
            % for i = 1:r.nt-1
            %     m1_ana(i+1) = m1_ana(i) -2*r.k12t*r.pL2_measurements(i)*(varxA1_ana(i) - varxA1xA2_ana(i))/(1+2*r.k12t^2*r.relXVar_ana(i));
            %     m3_ana(i+1) = m3_ana(i) -2*r.k12t*r.pL2_measurements(i)*(varxA2xA1_ana(i) - varxA2_ana(i))/(1+2*r.k12t^2*r.relXVar_ana(i));
            %     m2_ana(i+1) = m2_ana(i) +2*r.k11t*r.xL1_measurements(i)*(varpA1_ana(i) + varpA2pA1_ana(i))/(1+2*r.k11t^2*r.sumPVar_ana(i));
            %     m4_ana(i+1) = m4_ana(i) +2*r.k11t*r.xL1_measurements(i)*(varpA2_ana(i) + varpA1pA2_ana(i))/(1+2*r.k11t^2*r.sumPVar_ana(i));
            % end

            for i = 1:r.nt-1
                 m1_ana(i+1) = m1_ana(i)*cos(r.wt) + m2_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*cos(r.wt)*(r.varxA1(i) - r.varxA2xA1(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*sin(r.wt)*(r.varpA1(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
                 m2_ana(i+1) = m2_ana(i)*cos(r.wt) - m1_ana(i)*sin(r.wt) + 2*r.k2t*r.pL2_measurements(i)*sin(r.wt)*(r.varxA1(i) - r.varxA2xA1(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*cos(r.wt)*(r.varpA1(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
                 m3_ana(i+1) = m3_ana(i)*cos(r.wt) - m4_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*cos(r.wt)*(r.varxA2xA1(i) - r.varxA2(i))/(1+2*r.k2t^2*r.relXVar(i)) - 2*r.k1t*r.xL1_measurements(i)*sin(r.wt)*(r.varpA2(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
                 m4_ana(i+1) = m4_ana(i)*cos(r.wt) + m3_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*sin(r.wt)*(r.varxA2xA1(i) - r.varxA2(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*cos(r.wt)*(r.varpA2(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
            end


            r.m1_ana = m1_ana;
            r.m2_ana = m2_ana;
            r.m3_ana = m3_ana;
            r.m4_ana = m4_ana;


            %%%%%%%%%%%%%%%%%%
            idxs = 1:numel(r.ts)-1;

            %if r.wt == 0
            %    r.dmxm_ana = -2*r.pL2_measurements*r.k12t.*(1./(2*r.k12t^2 + 1./r.relXVar_ana(idxs))); 
            %    r.dmpp_ana = +2*r.xL1_measurements*r.k11t.*(1./(2*r.k11t^2 + 1./r.sumPVar_ana(idxs)));
            %else
                if r.trig_harmonic == 0
                    r.dmxm_ana = -2*r.pL2_measurements*r.k12t.*(1./(2*r.k12t^2 + 1./r.relXVar_ana(idxs))) + r.wt*(+2*r.xL1_measurements*r.k11t.*(1./(2*r.k11t^2 + 1./r.sumPVar_ana(idxs)))) + ( r.m2(idxs) + r.m4(idxs))*r.wt;
                    r.dmpp_ana = +2*r.xL1_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(idxs))) + r.wt*(+2*r.pL2_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.relXVar_ana(idxs)))) + (-r.m1(idxs) + r.m3(idxs))*r.wt;
                else
                    r.dmxm_ana = (cos(r.wt) - 1)*(r.m1(idxs)-r.m3(idxs)) + (r.m2(idxs)+r.m4(idxs))*sin(r.wt) - 2*(r.k2t*cos(r.wt)*r.pL2_measurements./(2*r.k2t^2 + 1./(r.relXVar(idxs))) - r.k1t*sin(r.wt)*r.xL1_measurements./(2*r.k1t^2 + 1./(r.sumPVar(idxs))));
                    r.dmpp_ana = (cos(r.wt) - 1)*(r.m2(idxs)+r.m4(idxs)) - (r.m1(idxs)-r.m3(idxs))*sin(r.wt) + 2*(r.k2t*sin(r.wt)*r.pL2_measurements./(2*r.k2t^2 + 1./(r.relXVar(idxs))) + r.k1t*cos(r.wt)*r.xL1_measurements./(2*r.k1t^2 + 1./(r.sumPVar(idxs))));
                end
            %end

            %%%%%%%%%%%%%%%%%%
            %mxm = (r.m1(1) - r.m3(1)) + [0, cumsum(-2*r.pL2_measurements*r.kt.*(r.relXVar_ana(2:end)./(1+2*r.kt^2*r.relXVar_ana(2:end))))];
            [mxm, mpp] = deal(zeros(size(r.m1)));
            mxm(1) = r.m1(1) - r.m3(1);
            mpp(1) = r.m2(1) + r.m4(1);
            for i = 1:r.nt-1
                %mxm(i+1) = mxm(i) -2*r.pL2_measurements(i)*r.kt.*(r.relXVar_ana(i)./(1+2*r.kt^2*r.relXVar_ana(i)));
                %mpp(i+1) = mpp(i) +2*r.xL1_measurements(i)*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(i)));

                mxm(i+1) = mxm(i) + r.dmxm_ana(i);
                mpp(i+1) = mpp(i) + r.dmpp_ana(i);
            end

            %mxm = (r.m1(1) - r.m3(1)) + [0, cumsum(-2*r.pL2_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.relXVar_ana(2:end))))];
            %mpp = (r.m2(1) + r.m4(1)) + [0, cumsum(+2*r.xL1_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(2:end))))];

            r.mxm = mxm;
            r.mpp = mpp;
        end

    end
    
    
    %% WITH FORCES

    if r.sigmabp == 0 && r.k11 == r.k21 && r.k12 == -r.k22 && r.w == 0    
        if r.gammabp == 0 % if constant f(t) = f0
            r.varFp_ana = r.varFp(1)./(1 + 2/3*r.cp^2 * r.varFp(1)*r.k1^2*r.ts.^3 .* (1 + 0.5 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts)./(1 + 2 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts));
        else % if OU f(t) with zero diffusion
            r.varFp_ana = exp(-2*r.gammabp*r.ts)./(1/r.varFp(1) + r.cp^2*r.k1^2./(r.gammabp^4*(2*r.k1^2 * r.ts + 1)).* ...
                ( ...
                   -(4*r.k1^2 + r.gammabp*(1 + 2*r.k1^2*r.ts)).*exp(-2*r.gammabp*r.ts)  ...
                   + 4*(2*r.k1^2 + r.gammabp).*exp(-r.gammabp*r.ts) ...
                   + (2*r.gammabp*r.ts*(r.gammabp + r.k1^2) - (4*r.k1^2 + 3*r.gammabp))...
                )...
               );
        end

    end
end