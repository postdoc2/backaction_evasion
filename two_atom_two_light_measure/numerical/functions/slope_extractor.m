function [ts_windows_begin, slopes] = slope_extractor(r, variable, threshold, n_segments, do_plot)


ts_full  = r.ts; 
var_full = r.(variable);



idxs = var_full < threshold;

ts  = ts_full(idxs);
var = var_full(idxs);

if do_plot
    figure;
    loglog(ts_full, var_full)
    hold on
    loglog(ts, var)
end



segment_points =  logspace(log10(ts(1)), log10(ts(end)), n_segments+1);

segment_points_idx = 0*segment_points;
for i = 1:n_segments + 1
    [~, idx] = min(abs(ts - segment_points(i)));
    segment_points_idx(i) = idx;
end

if length(segment_points_idx) ~= numel(unique(segment_points_idx))
    error('too many segments, non-unique indices')
end

slopes = zeros(1,n_segments);
%ts_windows = cell(1, n_segments);
ts_windows_begin = ts(segment_points_idx(1:end-1));
for i = 1:n_segments
    
    min_idx = segment_points_idx(i);
    max_idx = segment_points_idx(i+1);
    
    window = [min_idx:max_idx];
    
    %ts_windows{i} = ts(window);
    
    coeffs = polyfit(log10(ts(window)), log10(var(window)), 1); % f(x) = b*x^m ---> log10(f(x)) = log10(x^m) + log10(b) = m * log10(x) + log10(b)
    slopes(i) = coeffs(1);
    
    if do_plot
        loglog(ts(window), var(window));
    end
    
end

if do_plot
    yyaxis right
    plot(ts(segment_points_idx(1:end-1)), slopes ,'r.')
    set(gca,'yscale','linear')
end


end