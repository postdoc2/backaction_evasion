function plotting_rotation(r)

%%%
% p = inputParser;
% chkBool = @(x) isnumeric(x) && isscalar(x) && (x == 0 || x == 1);
% p.addParameter('plot_A1',  0, chkBool)
% p.addParameter('plot_A2',  0, chkBool)
% p.addParameter('plot_rel', 1, chkBool)
% parse(p, varargin{:})
% n_subplots = p.Results.plot_A1 + p.Results.plot_A2 + p.Results.plot_rel;
%%%

the_fig = gcf;

n_subplots = 3;

angles = linspace(0,2*pi,100);

for k = 1:n_subplots
    subplot(1, 3, k)

    hold on
    ellipse_v1{k} = plot(nan);
    ellipse_v2{k} = plot(nan);
    ellipse{k}    = plot(nan,'linewidth',3);
    mean_dot{k}   = plot(nan,'k.','markersize',20);
    dot_trace{k}  = plot(nan,'k','linewidth',1);
    ax_walk{k} = gca;
    axis equal
    
    if k == 1
        xlabel('$x_1$')
        ylabel('$p_1$')
        t_string{1} = '$\Delta x_1 \Delta p_1 = $';
        mn{1} = [r.m1; r.m2];
    elseif k == 2
        xlabel('$x_2$')
        ylabel('$p_2$')
        mn{2} = [r.m3; r.m4];
        t_string{2} = '$\Delta x_2 \Delta p_2 = $';
    else 
        xlabel('$x_1-x_2$')
        ylabel('$p_1+p_2$')
    
        t_string{3} = '$\Delta (x_1-x_2) \Delta (p_1+p_2) = $';

        mn{3} = [r.m1 - r.m3; r.m2 + r.m4];
    end
    
    the_title{k} = title(ax_walk{k}, [t_string{k}]);
    
        
    %xlim(ax_walk{k}, [-5,5])
    %ylim(ax_walk{k}, [-5,5])
end


lastwarn('','')
for idx = 1:1:r.nt
    
    if ~ishghandle(the_fig) || ~isempty(lastwarn)
        disp('breaking anim')
        break
    end
  %  try 
    lastwarn('','')
        for k = 1:n_subplots

            if k == 1
                covar = 0.5*r.gs([1,2],[1,2], idx);
            elseif k == 2
                covar = 0.5*r.gs([3,4],[3,4], idx);
            else
                covar = zeros(2,2);

                covar(1,1) = r.relXVar(idx);
                covar(2,2) = r.sumPVar(idx);
                covar(1,2) = r.relXsumPcovar(idx);
            end

            covar = triu(covar) + triu(covar)' - diag(diag(covar));

            [vecs, vals] = eig(covar);

            if ~isempty(find(vals < 0))
                %disp(['negative eigs for ' num2str(k)])
                the_title{k}.String = [t_string{k} num2str(sqrt(covar(1,1)*covar(2,2)),3) ' - negative eigs'];
                continue;            
            end

            v1 = sqrt(vals(1,1))*vecs(:,1);
            v2 = sqrt(vals(2,2))*vecs(:,2);

            set(ellipse_v1{k}, 'xdata', [0, v1(1)] + mn{k}(1,idx) ,'ydata',[0, v1(2)] + mn{k}(2,idx));
            set(ellipse_v2{k}, 'xdata', [0, v2(1)] + mn{k}(1,idx) ,'ydata',[0, v2(2)] + mn{k}(2,idx));
            %plot([0, v1(1)], [0 v1(2)])
            %hold on
            %plot([0, v2(1)], [0 v2(2)])
            %plot()
            

            p_vec = mn{k}(:,idx) + v1*cos(angles) + v2*sin(angles);

            set(ellipse{k}, 'xdata', p_vec(1,:), 'ydata', p_vec(2,:));
            set(mean_dot{k},'xdata',  mn{k}(1,idx), 'ydata', mn{k}(2,idx));
            set(dot_trace{k},'xdata', mn{k}(1,1:idx), 'ydata', mn{k}(2,1:idx));

            %plot(p(1,:), p(2,:))

            %title(ax_walk, [t_string num2str(sqrt(covar(1,1)*covar(2,2)),3)])
            the_title{k}.String = [t_string{k} num2str(sqrt(covar(1,1)*covar(2,2)),3)];

        end

        if idx == 1
            pause()
        end
       % pause()
        pause(0.01)
  %  catch

end

%r.varxA1xA2(end)
%r.gs(:,:, idx)
end
