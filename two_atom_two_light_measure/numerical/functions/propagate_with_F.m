function r = propagate_with_F(A0, m0, k11, k12, k21, k22, w1, w2, cx, OUx, cp, OUp, measurement_seed_num, varargin)

constants
trig_harmonic = 1; % hardcoded for now

var_is_class(OUx, 'OrnsteinUhlenbeckProcess');
var_is_class(OUp, 'OrnsteinUhlenbeckProcess');
if isempty(OUx.X); error('OUx must have a process already!'); end
if isempty(OUp.X); error('OUp must have a process already!'); end

if OUx.time.ts ~= OUp.time.ts; error('OU processes does not have same Time!'); end

if ~isequal(A0', A0)
   disp('A0 was not symmetric - symmetrizing with upper triangle...')  
   A0 = triu(A0) + triu(A0)' - diag(diag(A0))
end
r.m0   = m0;
r.A0   = A0;

r.k11 = k11;
r.k12 = k12;
r.k21 = k21;
r.k22 = k22;

r.w1 = w1;
r.w2 = w2;
if w1 == w2
    r.w = w1;
else
    r.w = nan;
end

r.measurement_seed_num = measurement_seed_num;
r.trig_harmonic = trig_harmonic;

r.cx  = cx;
r.cp  = cp;

r.OUx = OUx;
r.OUp = OUp;

r.time = OUp.time;
r.T    = OUp.time.T;
r.dt   = OUp.time.dt;
r.nt   = OUp.time.nt;
r.ts   = OUp.time.ts;


% transformed params
r.k11t = k11 * sqrt(r.dt);
r.k21t = k21 * sqrt(r.dt);
r.k12t = k12 * sqrt(r.dt);
r.k22t = k22 * sqrt(r.dt);

if k11 == k21 % If both atoms couples to laser 1 with same strength
    r.k1 = k11;
    r.k1t = r.k1 * sqrt(r.dt);
end

if k12 == -k22 % If both atoms couples to laser 2 with same strength
    r.k2 = k12;
    r.k2t = r.k2 * sqrt(r.dt);
end
    
if k11 == k12 &&  k21 == -k22 && k11 == k21
    r.k = r.k11;
    r.kt = r.k * sqrt(r.dt);
end

r.w1t = w1 * r.dt;
r.w2t = w2 * r.dt;
if w1 == -w2 % only if same freq.
    r.w = w1; 
    r.wt = r.w * r.dt;
end 
    
r.cxt = cx * r.dt;
r.fxs = OUx.X;
r.gammabx = OUx.drift_scaling;     r.gammabxt = OUx.drift_scaling * r.dt;
r.sigmabx = OUx.diffusion_scaling; r.sigmabxt = OUx.diffusion_scaling * r.dt;

r.cpt = cp * r.dt;
r.fps = OUp.X;
r.gammabp = OUp.drift_scaling;     r.gammabpt = OUp.drift_scaling * r.dt;
r.sigmabp = OUp.diffusion_scaling; r.sigmabpt = OUp.diffusion_scaling * r.dt;


%% dims

mdim = 6;
ndim = 4;
dim = mdim + ndim;

xA1 = unitvec(dim, 1);
pA1 = unitvec(dim, 2);
xA2 = unitvec(dim, 3);
pA2 = unitvec(dim, 4);
fxv = unitvec(dim, 5);
fpv = unitvec(dim, 6);
xL1 = unitvec(dim, 7);
pL1 = unitvec(dim, 8);
xL2 = unitvec(dim, 9);
pL2 = unitvec(dim, 10);

%% 10x10

rL1 = 1;
rL2 = 1;
B0 = diag([1/rL1, rL1, 1/rL2, rL2]);
C0 = zeros(mdim, ndim);

n0 = [0; 0; 0; 0];

y0 = [r.m0;
        n0];
  
g0 = [A0  C0; 
      C0' B0];
    
if trig_harmonic  
    Scst =  ... 
        + xA1 * ( cos(r.w1t)*xA1  +  sin(r.w1t) * pA1  +  r.k11t * pL1 + r.cxt * fxv)'   ... % XA1 
        + pA1 * ( cos(r.w1t)*pA1  -  sin(r.w1t) * xA1  -  r.k12t * xL2 + r.cpt * fpv )'   ... % PA1
        + xA2 * ( cos(r.w2t)*xA2  +  sin(r.w2t) * pA2  +  r.k21t * pL1 )'   ... % XA2
        + pA2 * ( cos(r.w2t)*pA2  -  sin(r.w2t) * xA2  -  r.k22t * xL2 )'   ... % PA2
        + fxv * ( (1-r.gammabxt) * fxv )' ... 
        + fpv * ( (1-r.gammabpt) * fpv )' ... 
        + xL1 * ( xL1  +  r.k11t * pA1 +  r.k21t * pA2 )'   ... % XL1
        + pL1 * ( pL1 )'                                    ... % PL1     
        + xL2 * ( xL2 )'                                    ... % XL2
        + pL2 * ( pL2  -  r.k12t * xA1 -  r.k22t * xA2  )'; ... % PL2  
else
    error('trig_harmonic = 0 not implemented with forces yet');
end

L = diag([0, 0, 0, 0, 2*r.sigmabxt, 2*r.sigmabpt, 0, 0, 0, 0]);
S = Scst;
y =   y0;
g =   g0;

ys = zeros([size(y0), r.nt]);
gs = zeros([size(g0), r.nt]);

pi_mat = diag([1; 0; 0; 1]);

ys(:,1,1) = y;
gs(:,:,1) = g;

%xL1_gauss = -0.1 + 0.0*xL1_gauss;
%pL2_gauss = -0.1 + 0.0*pL2_gauss;


%% Measurement detection record

p = inputParser;
p.addParameter('forwardresults',[])
p.addParameter('approx_mpinv', 'off') % approximate moore penrose inverse (pi*B*pi)^- ? if yes, (pi*B*pi)^- ≈ diag(1, 0, 0, 1)
parse(p, varargin{:});

if isempty(p.Results.forwardresults) % if not PQS backward
    %% 8x8

    if measurement_seed_num ~= 0
        rng(r.measurement_seed_num)
    end
    xL1_gauss = sqrt(0.5) * randn(1,r.nt-1);
    pL2_gauss = sqrt(0.5) * randn(1,r.nt-1);

    [xL1_measurements, pL2_measurements] = deal(zeros(size(xL1_gauss)));

    exp_idxs = [1:4, dim-ndim+1:dim]; % excluding f
    C0_exp  = zeros((dim-2)-ndim, ndim);
    S_exp   = S(exp_idxs, exp_idxs);
    y_exp   = y(exp_idxs);
    g_exp   = g(exp_idxs, exp_idxs);

    for i = 1:r.nt-1
            y_exp = S_exp * y_exp + [r.cxt*r.fxs(i); r.cpt*r.fps(i); 0; 0; 0; 0; 0; 0];
            g_exp = S_exp * g_exp * S_exp';

            m_exp = y_exp(1:mdim-2,   :);
            n_exp = y_exp(mdim+1-2:end, :);

            A_exp = g_exp(1:mdim-2, 1:mdim-2);
            B_exp = g_exp(mdim+1-2:end, mdim+1-2:end);
            C_exp = g_exp(1:mdim-2, mdim+1-2:end);

            xL1_measurements(i) = n_exp(1) + xL1_gauss(i);
            pL2_measurements(i) = n_exp(4) + pL2_gauss(i);

            if strcmp(p.Results.approx_mpinv, 'on')
                mpinv_exp = diag([1 0 0 1]);
            else
                mpinv_exp = pinv(pi_mat*B_exp*pi_mat);
            end
            A_exp = A_exp - C_exp * mpinv_exp * C_exp';
            m_exp = m_exp + C_exp * mpinv_exp * [xL1_measurements(i) - n_exp(1); 0; 0; pL2_measurements(i) - n_exp(4)];

            y_exp = [m_exp; n0];
            g_exp = [A_exp, C0_exp; C0_exp', B0];
    end
    
else
    xL1_measurements = - fliplr(p.Results.forwardresults.xL1_measurements);
    pL2_measurements = - fliplr(p.Results.forwardresults.pL2_measurements);
end
%% 10x10

[DeltaL1, DeltaL2] = deal(zeros(1, r.nt - 1));
for i = 1:r.nt-1
    
    
    
    if mod(i/1e4, 1) == 0;  fprintf('done: %i pct.\n', round(i/r.nt*100));     end
    
    y = S*y;
    g = S*g*S' + L;

    m = y(1:mdim,     :);
    n = y(mdim+1:end, :);

    A = g(1:mdim, 1:mdim);
    B = g(mdim+1:end, mdim+1:end);

    C = g(1:mdim, mdim+1:end);

    %xL1_measurements(i) = sqrt(0.5*B(1,1))*xL1_gauss(i);
    %pL2_measurements(i) = sqrt(0.5*B(4,4))*pL2_gauss(i);

    %xL1_measurement = n(1) + xL1_measurements(i);
    %pL2_measurement = n(4) + pL2_measurements(i);

    %if mod(i/1e3, 1) == 0; tic; end
    if strcmp(p.Results.approx_mpinv, 'on')
        mpinv = diag([1 0 0 1]);
    else
        mpinv = pinv(pi_mat*B*pi_mat);
    end
    %if mod(i/1e3, 1) == 0; toc; end
    
    A = A - C * mpinv * C';
    m = m + C * mpinv * [xL1_measurements(i) - n(1) ; 0; 0; pL2_measurements(i) - n(4)];

    DeltaL1(i) = xL1_measurements(i) - n(1);
    DeltaL2(i) = pL2_measurements(i) - n(4);
    
    y = [m; n0];

    g = [A,   C0; 
         C0', B0];

    ys(:,1,i+1) = y;
    gs(:,:,i+1) = g;
    
end

if ~isempty(p.Results.forwardresults)
    gs = flip(gs, 3);
    ys = flip(ys, 3);
end

%% STORAGE

r.S = Scst;
r.ys = ys;
r.gs = gs;

if ~isempty(p.Results.forwardresults); return; end % if backward PQS, return

r.xL1_measurements = xL1_measurements;
r.pL2_measurements = pL2_measurements;
r.DeltaL1 = DeltaL1;
r.DeltaL2 = DeltaL2;

% moments are constructed from calling moments_from_results(r)

end
