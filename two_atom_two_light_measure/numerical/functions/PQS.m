function r = PQS(r, varargin)

    mdim = numel(r.m0);
    % backward
    m0_E = zeros(size(r.m0));

    p = inputParser;
    p.addParameter('E_covar_initial_val', 10000);
    p.addParameter('approx_mpinv', 'off') % approximate moore penrose inverse (pi*B*pi)^- ? if yes, (pi*B*pi)^- ≈ diag(1, 0, 0, 1)
    parse(p, varargin{:})
   
    
    A0_E = p.Results.E_covar_initial_val * eye(size(r.A0));
    
    OUx_backward = r.OUx;
    OUx_backward.drift_scaling = -OUx_backward.drift_scaling;
    
    OUp_backward = r.OUp;
    OUp_backward.drift_scaling = -OUp_backward.drift_scaling;
    
    r_backward = propagate_with_F(A0_E, m0_E, -r.k11, -r.k12, -r.k21, -r.k22, -r.w1, -r.w2, -r.cx, OUx_backward, -r.cp, OUp_backward, r.measurement_seed_num, 'forwardresults', r,'approx_mpinv', p.Results.approx_mpinv); 
    
    % PQS calculations    
    nt = r.nt;
    
    Ars = r.gs(1:mdim, 1:mdim, :);
    mrs = r.ys(1:mdim, 1, :);
    
    AEs = r_backward.gs(1:mdim, 1:mdim, :);
    mEs = r_backward.ys(1:mdim, 1, :);
    
    r.mEs = squeeze(mEs);
    
    ArEs = zeros(size(AEs));
    mrEs = zeros(size(mEs));
    
    for i = 1:nt
       
        lastwarn('');
        
        Ar = Ars(:,:,i);
        AE = AEs(:,:,i);
        mr = mrs(:,1,i);
        mE = mEs(:,1,i);
        
        ArE_inv = inv(Ar) + inv(AE);
        ArE = inv(ArE_inv);
        %mrE = ArE*(inv(Ar)*mr + inv(AE)*mE);
        mrE = ArE_inv \ (Ar\ mr + AE \ mE);
        
        if ~isempty(lastwarn)
            error('Choose a lower dt!')
            %rs.time.ts(i)
         %   continue;
        end
        
        ArEs(:,:,i) = ArE;
        mrEs(:,1,i) = mrE;
    end
    
    r.m_PQS = mrEs;
    r.mFx_PQS = squeeze(mrEs(5,1,:))';
    r.mFp_PQS = squeeze(mrEs(6,1,:))';
    
    r.ArEs   = ArEs;
    r.varFx_PQS   = 0.5*(squeeze(ArEs(5,5,:)))';
    r.varFp_PQS   = 0.5*(squeeze(ArEs(6,6,:)))';

    
    
end