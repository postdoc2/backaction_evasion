function r = moments_from_results(r)

ys = r.ys;
gs = r.gs;


%% means 

r.m1 = squeeze(ys(1,1,:))';
r.m2 = squeeze(ys(2,1,:))';
r.m3 = squeeze(ys(3,1,:))';
r.m4 = squeeze(ys(4,1,:))';
r.m5 = squeeze(ys(5,1,:))';
r.m6 = squeeze(ys(6,1,:))';

% EPR variables 
r.relXMean = 1/sqrt(2) * (r.m1 - r.m3);
r.sumPMean = 1/sqrt(2) * (r.m2 + r.m4);
r.sumXMean = 1/sqrt(2) * (r.m1 + r.m3);
r.relPMean = 1/sqrt(2) * (r.m2 - r.m4);

%% covariances 
A11 = squeeze(gs(1,1,:))';
A22 = squeeze(gs(2,2,:))';
A33 = squeeze(gs(3,3,:))';
A44 = squeeze(gs(4,4,:))';
A55 = squeeze(gs(5,5,:))';
A66 = squeeze(gs(6,6,:))';

A12 = squeeze(gs(1,2,:))';
A13 = squeeze(gs(1,3,:))';
A14 = squeeze(gs(1,4,:))';
A31 = squeeze(gs(3,1,:))';
A24 = squeeze(gs(2,4,:))';
A42 = squeeze(gs(4,2,:))';
A23 = squeeze(gs(2,3,:))';
A34 = squeeze(gs(3,4,:))';

r.varxA1 = 0.5*A11;
r.varpA1 = 0.5*A22;
r.varxA2 = 0.5*A33;
r.varpA2 = 0.5*A44;
r.varFx  = 0.5*A55;
r.varFp  = 0.5*A66;

% EPR variables
r.varxA1xA2 = 0.5*A13;
r.varxA2xA1 = 0.5*A31;
r.varpA1pA2 = 0.5*A24;
r.varpA2pA1 = 0.5*A42;
r.varxA1pA1 = 0.5*A12;
r.varxA2pA2 = 0.5*A34;
r.varxA1pA2 = 0.5*A14;
r.varpA1xA2 = 0.5*A23;


r.relXVar = 0.5*(r.varxA1 + r.varxA2 - (r.varxA1xA2 + r.varxA2xA1 ));
r.sumXVar = 0.5*(r.varxA1 + r.varxA2 + (r.varxA1xA2 + r.varxA2xA1 ));

r.relPVar = 0.5*(r.varpA1 + r.varpA2 - (r.varpA1pA2 + r.varpA2pA1 ));
r.sumPVar = 0.5*(r.varpA1 + r.varpA2 + (r.varpA1pA2 + r.varpA2pA1 ));

r.relXsumPcovar = 0.5*(r.varxA1pA1 - r.varxA2pA2 + r.varxA1pA2 - r.varpA1xA2);
r.relXrelPcovar = 0.5*(r.varpA1xA2 - r.varxA1pA1 + r.varxA1pA2 - r.varxA2pA2);

%r.relXsumPcovar = 0.5*(r.gs(1,2, :) + r.gs(1,4, :) - r.gs(2,3, :) - r.gs(3,4, :));
%r.relXrelPcovar = 0.5*(r.gs(3,2, :) + r.gs(1,4, :) - r.gs(1,2, :) - r.gs(3,4, :));

end