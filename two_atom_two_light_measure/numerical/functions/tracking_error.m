function res = tracking_error(true_field, estimated_field, varargin)
% 'True field' is an OU process clas, estimated field is a vector 

p = inputParser;
p.addParameter('integration_idxs', ones(1, length(estimated_field)))
%p.addParameter('integration_idxs', 1:length(estimated_field));
parse(p, varargin{:})

%include_indices = p.Results.include_indices;

integration_idxs = p.Results.integration_idxs;

idxs = 1:length(estimated_field);
idxs = idxs(integration_idxs == 1);

dt = true_field.time.dt;
%T  = true_field.time.ts(end);

%t1 = true_field.time.ts(idxs(1));
%t2 = true_field.time.ts(idxs(end));



n = numel(idxs);

res = 1/(n*dt) * sum((true_field.X(1,idxs) - estimated_field(1,idxs)).^2) * dt;

end