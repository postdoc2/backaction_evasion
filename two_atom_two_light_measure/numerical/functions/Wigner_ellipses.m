classdef Wigner_ellipses < handle
    
    properties
        major_axis_vectors
        minor_axis_vectors
        
        scale 
        ellipse_angles
        
        centers
        major_axis_tilt_angles 
        
        ellipses
    end
    
    methods
        function obj = Wigner_ellipses(r, subsystem_id, ellipse_contour_value_normalized, n_ellipse_pts)
            
            
            obj.ellipse_angles = [linspace(0, 2*pi, n_ellipse_pts)];
            
            
            [obj.major_axis_vectors, obj.minor_axis_vectors] = deal(zeros(2, r.nt));
            obj.major_axis_tilt_angles = zeros(1, r.nt);
            
            
            % https://cookierobotics.com/007/
            obj.set_scale(ellipse_contour_value_normalized);
            
            if strcmp(subsystem_id, 'A1')
                obj.centers        = r.ys([1,2], 1, :);
            elseif strcmp(subsystem_id, 'A2')
                obj.centers        = r.ys([3,4], 1, :);
            elseif strcmp(subsystem_id, 'x1p2')
                obj.centers        = r.ys([1,4], 1, :);
            elseif strcmp(subsystem_id, 'forces')
                obj.centers        = r.ys([5,6], 1, :);
            elseif strcmp(subsystem_id, 'forces_pqs')
                obj.centers        = r.m_PQS([5,6], :);
            elseif strcmp(subsystem_id, 'L1')
                obj.centers        = r.ys([7,8], 1, :);
            elseif strcmp(subsystem_id, 'L2')
                obj.centers        = r.ys([9,10], 1, :);
            elseif strcmp(subsystem_id, 'rel') || strcmp(subsystem_id, 'rel_sum')
                obj.centers        = [r.relXMean; r.sumPMean];
            elseif strcmp(subsystem_id, 'rel_rel')
                obj.centers        = [r.relXMean; r.relPMean];
            else
                error('no valid subsystem id!')
            end
            
            for i = 1:r.nt
               
                if strcmp(subsystem_id, 'A1')
                    covar = 0.5*r.gs([1,2],[1,2], i);
                elseif strcmp(subsystem_id, 'A2')
                    covar = 0.5*r.gs([3,4],[3,4], i);
                elseif strcmp(subsystem_id, 'x1p2')
                    covar = 0.5*r.gs([1,4],[1,4], i);
                elseif strcmp(subsystem_id, 'forces')
                    covar = 0.5*r.gs([5,6],[5,6], i);
                elseif strcmp(subsystem_id, 'forces_pqs')
                    covar = 0.5*r.ArEs([5,6],[5,6], i);
                elseif strcmp(subsystem_id, 'L1')
                    covar = 0.5*r.gs([7,8],[7,8], i);
                elseif strcmp(subsystem_id, 'L2')
                    covar = 0.5*r.gs([9, 10],[9,10], i);
                elseif strcmp(subsystem_id, 'rel') || strcmp(subsystem_id, 'rel_sum') % x- and p+
                    covar = zeros(2,2);
                    
                    covar(1,1) = r.relXVar(i);
                    covar(2,2) = r.sumPVar(i);
                    covar(1,2) = r.relXsumPcovar(i);
                elseif strcmp(subsystem_id, 'rel_rel') % x- and p-
                    covar = zeros(2,2);
                    
                    covar(1,1) = r.relXVar(i);
                    covar(2,2) = r.relPVar(i);
                    covar(1,2) = r.relXrelPcovar(i);
                else
                    error('no valid subsystem id!')
                end
                
                
                covar = triu(covar) + triu(covar)' - diag(diag(covar)); % ensure symmetric (small numerical diff. errors are detrimental)
                [vecs, vals] = eig(covar); % eig is in ascending order

                lambda1 = vals(2,2); v1 = vecs(:,2); % largest
                lambda2 = vals(1,1); v2 = vecs(:,1); % smallest
                
                obj.major_axis_vectors(:,i) = sqrt(lambda1)*v1;
                obj.minor_axis_vectors(:,i) = sqrt(lambda2)*v2;

                a = covar(1,1);
                b = covar(2,1);
                c = covar(2,2);
                if b == 0
                    if a >= c
                        obj.major_axis_tilt_angles(i) = 0;
                    else
                        obj.major_axis_tilt_angles(i) = pi/2;
                    end
                else
                    obj.major_axis_tilt_angles(i) = atan2(lambda1-a, b);
                end
                
            end 
            
            
            obj.ellipses = obj.all();
        end
        
        function parametric_ellipses_vectors = at(obj, time_idx)
            
            
            v1 = sqrt(obj.scale) * obj.major_axis_vectors(:,time_idx);
            v2 = sqrt(obj.scale) * obj.minor_axis_vectors(:,time_idx);
            
            m = obj.centers(:, time_idx);
            vectors = m + v1*cos(obj.ellipse_angles) + v2*sin(obj.ellipse_angles);
            x = vectors(1,:);
            y = vectors(2,:);
            
            parametric_ellipses_vectors = {x, y}; % plot with parametric_ellipses_vectors{:}
            
        end
        
        function parametric_ellipses_vectors = all(obj)
            nt = size(obj.major_axis_vectors, 2);
            %parametric_ellipses_vectors = zeros(2, numel(obj.ellipse_angles), nt);
            parametric_ellipses_vectors = cell(1, nt);
            
            for i = 1:nt
                %parametric_ellipses_vectors(:,:, i) = obj.at(i);
                parametric_ellipses_vectors{i} = obj.at(i); % plot with parametric_ellipses_vectors{i}{:}
            end
        end
        
        function obj = set_scale(obj, ellipse_contour_value_normalized)
           % obj.scale = 2 * fminsearch(@(x) abs(chi2cdf(x,2) - (1 - ellipse_contour_value_normalized)), sqrt(2)); % scales ellipse so it coincides with the half-max value of gaussian
            obj.scale = fminsearch(@(x) abs(chi2cdf(x,2) - (1 - ellipse_contour_value_normalized)), sqrt(2)); % scales ellipse so it coincides with the half-max value of gaussian
        end
    end
end

