function r = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic)
%function r = propagate(r)

r.T    = T;
r.dt   = dt;
r.m0   = m0;
r.w    = w;
r.k    = k;
r.wt   = wt;
r.kt   = kt;
r.w1t  = w1t;
r.w2t  = w2t;
r.k11t = k11t; r.k11 = k11t/sqrt(dt); r.k1 = r.k11; r.k1t = r.k1*sqrt(dt);
r.k21t = k21t; r.k21 = k21t/sqrt(dt); 
r.k12t = k12t; r.k12 = k12t/sqrt(dt); r.k2 = r.k12; r.k2t = r.k2*sqrt(dt);
r.k22t = k22t; r.k22 = k22t/sqrt(dt);
r.seed_num = seed_num;

constants

ts = 0:r.dt:r.T;
nt = numel(ts);

r.ts = ts;
r.nt = nt;

r.phase_90deg_time = (pi/2)/w;
r.nt_phase_90deg_time = round(r.phase_90deg_time/dt);

%fprintf('T = %.2f ms\ndt = %.2e ms\nnt = %i\n',r.T/ms, r.dt/ms, nt)
%fprintf('kt = %.2e \nwt = %.2e\n', r.kt, r.wt)

dim  = 8;
ndim = 4;
mdim = dim - ndim;

xA1 = unitvec(dim, 1);
pA1 = unitvec(dim, 2);
xA2 = unitvec(dim, 3);
pA2 = unitvec(dim, 4);
xL1 = unitvec(dim, 5);
pL1 = unitvec(dim, 6);
xL2 = unitvec(dim, 7);
pL2 = unitvec(dim, 8);

%%

if ~isequal(A0', A0)
   disp('A0 was not symmetric - symmetrizing with upper triangle...')  
   A0 = triu(A0) + triu(A0)' - diag(diag(A0))
end

%A0 = eye(dim-ndim, dim-ndim);
%A0(1,1) = 1;
%A0 = A0 + diag(rand(dim-ndim,1));
%A0 = diag([1 1 1 1]);
%A0 = diag([5 1 1 1]);
%A0 = diag([4 16 2 4]);
rL1 = 1;
rL2 = 1;
B0 = diag([1/rL1, rL1, 1/rL2, rL2]);
C0 = zeros(dim-ndim, ndim);

n0 = [0; 0; 0; 0];

y0 = [r.m0;
        n0];
  

g0 = [A0  C0; 
      C0' B0];

    
if trig_harmonic  
    Scst =  ... 
        + xA1 * ( cos(r.w1t)*xA1  +  sin(r.w1t) * pA1  +  r.k11t * pL1 )'   ... % XA1 
        + pA1 * ( cos(r.w1t)*pA1  -  sin(r.w1t) * xA1  -  r.k12t * xL2 )'   ... % PA1
        + xA2 * ( cos(r.w2t)*xA2  +  sin(r.w2t) * pA2  +  r.k21t * pL1 )'   ... % XA2
        + pA2 * ( cos(r.w2t)*pA2  -  sin(r.w2t) * xA2  -  r.k22t * xL2 )'   ... % PA2
        + xL1 * ( xL1  +  r.k11t * pA1 +  r.k21t * pA2 )'   ... % XL1
        + pL1 * ( pL1 )'                                    ... % PL1     
        + xL2 * ( xL2 )'                                    ... % XL2
        + pL2 * ( pL2  -  r.k12t * xA1 -  r.k22t * xA2  )'; ... % PL2  
else
    Scst =  ... 
     + xA1 * ( xA1  +  r.w1t * pA1  +  r.k11t * pL1 )'   ... % XA1 
     + pA1 * ( pA1  -  r.w1t * xA1  -  r.k12t * xL2 )'   ... % PA1
     + xA2 * ( xA2  +  r.w2t * pA2  +  r.k21t * pL1 )'   ... % XA2
     + pA2 * ( pA2  -  r.w2t * xA2  -  r.k22t * xL2 )'   ... % PA2
     + xL1 * ( xL1  +  r.k11t * pA1 +  r.k21t * pA2 )'   ... % XL1
     + pL1 * ( pL1 )'                                    ... % PL1     
     + xL2 * ( xL2 )'                                    ... % XL2
     + pL2 * ( pL2  -  r.k12t * xA1 -  r.k22t * xA2  )'; ... % PL2
end





S = Scst;

% St = @(k11t, k12t, k21t, k22t)  ... 
%     + xA1 * ( xA1  +  w1t * pA1  +  k11t * pL1 )'   ... % XA1 
%     + pA1 * ( pA1  -  w1t * xA1  -  k12t * xL2 )'   ... % PA1
%     + xA2 * ( xA2  +  w2t * pA2  +  k21t * pL1 )'   ... % XA2
%     + pA2 * ( pA2  -  w2t * xA2  -  k22t * xL2 )'   ... % PA2
%     + xL1 * ( xL1  +  k11t * pA1 +  k21t * pA2 )'   ... % XL1
%     + pL1 * ( pL1 )'                                ... % PL1     
%     + xL2 * ( xL2 )'                                ... % XL2
%     + pL2 * ( pL2  -  k12t * xA1 -  k22t * xA2  )'  ... % PL2

y =   y0;
g =   g0;


ys = zeros([size(y0), nt]);
gs = zeros([size(g0), nt]);

pi_vec = diag([1; 0; 0; 1]);

ys(:,1,1) = y;
gs(:,:,1) = g;

if r.seed_num ~= 0
    rng(r.seed_num)
end

xL1_gauss = randn(1,nt-1);
pL2_gauss = randn(1,nt-1);

[xL1_measurements, pL2_measurements] = deal(zeros(size(xL1_gauss)));

%xL1_gauss = -0.1 + 0.0*xL1_gauss;
%pL2_gauss = -0.1 + 0.0*pL2_gauss;

for i = 1:nt-1
    
    if mod(i/100000, 1) == 0
        fprintf('done: %i pct.\n', round(i/nt*100))
    end
        
    try        
%        
%          if i < nt * 0.25
%              S = Scst;
%          elseif i < nt * 0.5
%              S = St(0,0,0,0);
%          elseif i < nt * 0.75
%              S = Scst;
%          else
%              S = St(0,0,0,0);
%          end
        
        y = S*y;
        g = S*g*S';

        m = y(1:mdim,     :);
        n = y(mdim+1:end, :);

        A = g(1:mdim, 1:mdim);
        B = g(mdim+1:end, mdim+1:end);
        
        C = g(1:mdim, mdim+1:end);

        %xL1_measurements(i) = sqrt(0.5*B(1,1))*xL1_gauss(i);
        %pL2_measurements(i) = sqrt(0.5*B(4,4))*pL2_gauss(i);
        
        xL1_measurements(i) = sqrt(0.5)*xL1_gauss(i);
        pL2_measurements(i) = sqrt(0.5)*pL2_gauss(i);
        
        xL1_measurement = n(1) + xL1_measurements(i);
        pL2_measurement = n(4) + pL2_measurements(i);
        
        
        
%         xL1_measurement = n(1) + sqrt(0.5)*xL1_measurements(i);
%         pL2_measurement = n(4) + sqrt(0.5)*pL2_measurements(i);

        mpinv = pinv(pi_vec*B*pi_vec);
        A = A - C*mpinv*C';
        m = m + C*mpinv*[xL1_measurement - n(1) ; 0; 0; pL2_measurement - n(4)];

        y = [m; n0];

        g = [A,   C0; 
             C0', B0];

        ys(:,1,i+1) = y;
        gs(:,:,i+1) = g;
    
    catch
        warning(sprintf('error at idx: %i - ts(i) = %.2e, ts(i)/T = %2f', i, ts(i), ts(i)/T))
        break
    end
    
end

%% STORAGE

r.S = Scst;
r.ys = ys;
r.gs = gs;

r.xL1_measurements = xL1_measurements;
r.pL2_measurements = pL2_measurements;

r.m1 = squeeze(ys(1,1,:))';
r.m2 = squeeze(ys(2,1,:))';
r.m3 = squeeze(ys(3,1,:))';
r.m4 = squeeze(ys(4,1,:))';

A11 = squeeze(gs(1,1,:))';
A22 = squeeze(gs(2,2,:))';
A33 = squeeze(gs(3,3,:))';
A44 = squeeze(gs(4,4,:))';

A13 = squeeze(gs(1,3,:))';
A31 = squeeze(gs(3,1,:))';
A24 = squeeze(gs(2,4,:))';
A42 = squeeze(gs(4,2,:))';

r.varxA1 = 0.5*A11;
r.varpA1 = 0.5*A22;
r.varxA2 = 0.5*A33;
r.varpA2 = 0.5*A44;

r.varxA1xA2 = 0.5*A13;
r.varxA2xA1 = 0.5*A31;
r.varpA1pA2 = 0.5*A24;
r.varpA2pA1 = 0.5*A42;

r.relXVar = r.varxA1 + r.varxA2 - (r.varxA1xA2 + r.varxA2xA1 );
r.sumXVar = r.varxA1 + r.varxA2 + (r.varxA1xA2 + r.varxA2xA1 );
r.relPVar = r.varpA1 + r.varpA2 - (r.varpA1pA2 + r.varpA2pA1 );
r.sumPVar = r.varpA1 + r.varpA2 + (r.varpA1pA2 + r.varpA2pA1 );
r.relXsumPcovar = 0.5*(r.gs(1,2, :) + r.gs(1,4, :) - r.gs(2,3, :) - r.gs(3,4, :));


%% Analytical forms for w=0, k11=k12=k21=k, k22=-k

%%%%%%%%%%%%%%%%%%
r.relXVar_ana = 1./(2*r.k2^2*r.ts + 1/(r.varxA1(1) + r.varxA2(1)));
%r.relPvar_ana = (r.varxA1 + r.varxA2 + 2*r.k^2*r.ts )
r.sumPVar_ana = 1./(2*r.k1^2*r.ts + 1/(r.varpA1(1) + r.varpA2(1)));

%n1 = (1 + A11(1)*A33(1))*r.k^2*r.ts + (A11(1) + A33(1))*r.k^4*r.ts.^2;
%d1 = 1 + (A11(1) + A33(1))*r.k^2*r.ts;
%r.sumXVar_ana = (r.varxA1 + r.varxA2)./d1 + 2*n1./d1; % OK
%r.sumXVar_ana = r.relXVar_ana + 2*A11(1)*A33(1)*r.k^2*r.ts/(1+(A11(1) + A33(1))*r.k^2*r.ts) + 2*r.k^2*r.ts; % OK
r.sumXVar_ana = r.relXVar_ana + 4*r.varxA1(1)*r.varxA2(1)./(1./(2*r.k2^2*r.ts) + r.varxA1(1) + r.varxA2(1)) + 2*r.k1^2*r.ts; % OK

%n2 = (1 + A22(1)*A44(1))*r.k^2*r.ts + (A22(1) + A44(1))*r.k^4*r.ts.^2;
%d2 = 1 + (A22(1) + A44(1))*r.k^2*r.ts;
%r.relPVar_ana = (r.varpA1 + r.varpA2)./d2 + 2*n2./d2; % OK
r.relPVar_ana = r.sumPVar_ana + 4*r.varpA1(1)*r.varpA2(1)./(1./(2*r.k1^2*r.ts) + r.varpA1(1) + r.varpA2(1)) + 2*r.k2^2*r.ts;  % OK

%%%%%%%%%%%%%%%%%%

%n1 = (1 + A11(1)*A33(1))*r.k^2*r.ts + (A11(1) + A33(1))*r.k^4*r.ts.^2;
%n2 = (1 + A22(1)*A44(1))*r.k^2*r.ts + (A22(1) + A44(1))*r.k^4*r.ts.^2;
%d1 = (1 + (A11(1) + A33(1))*r.k^2*r.ts);
%d2 = (1 + (A22(1) + A44(1))*r.k^2*r.ts);

n1 = (r.k1^2 + A11(1)*A33(1)*r.k2^2)*r.ts + (A11(1) + A33(1))*r.k1^2*r.k2^2*r.ts.^2;
n2 = (r.k2^2 + A22(1)*A44(1)*r.k1^2)*r.ts + (A22(1) + A44(1))*r.k1^2*r.k2^2*r.ts.^2;
d1 = (1 + (A11(1) + A33(1))*r.k2^2*r.ts);
d2 = (1 + (A22(1) + A44(1))*r.k1^2*r.ts);


varxA1_ana = 0.5 * (A11(1) + n1)./d1;
varpA1_ana = 0.5 * (A22(1) + n2)./d2;
varxA2_ana = 0.5 * (A33(1) + n1)./d1;
varpA2_ana = 0.5 * (A44(1) + n2)./d2;

varxA1xA2_ana = +0.5*n1./d1;
varxA2xA1_ana = varxA1xA2_ana;

varpA1pA2_ana = -0.5*n2./d2;
varpA2pA1_ana = varpA1pA2_ana;

[m1_ana, m2_ana, m3_ana, m4_ana] = deal(zeros(size(r.m1)));
m1_ana(1) = r.m1(1);
m2_ana(1) = r.m2(1);
m3_ana(1) = r.m3(1);
m4_ana(1) = r.m4(1);
% for i = 1:r.nt-1
%     m1_ana(i+1) = m1_ana(i) -2*r.k12t*r.pL2_measurements(i)*(varxA1_ana(i) - varxA1xA2_ana(i))/(1+2*r.k12t^2*r.relXVar_ana(i));
%     m3_ana(i+1) = m3_ana(i) -2*r.k12t*r.pL2_measurements(i)*(varxA2xA1_ana(i) - varxA2_ana(i))/(1+2*r.k12t^2*r.relXVar_ana(i));
%     m2_ana(i+1) = m2_ana(i) +2*r.k11t*r.xL1_measurements(i)*(varpA1_ana(i) + varpA2pA1_ana(i))/(1+2*r.k11t^2*r.sumPVar_ana(i));
%     m4_ana(i+1) = m4_ana(i) +2*r.k11t*r.xL1_measurements(i)*(varpA2_ana(i) + varpA1pA2_ana(i))/(1+2*r.k11t^2*r.sumPVar_ana(i));
% end

for i = 1:r.nt-1
     m1_ana(i+1) = m1_ana(i)*cos(r.wt) + m2_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*cos(r.wt)*(r.varxA1(i) - r.varxA2xA1(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*sin(r.wt)*(r.varpA1(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
     m2_ana(i+1) = m2_ana(i)*cos(r.wt) - m1_ana(i)*sin(r.wt) + 2*r.k2t*r.pL2_measurements(i)*sin(r.wt)*(r.varxA1(i) - r.varxA2xA1(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*cos(r.wt)*(r.varpA1(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
     m3_ana(i+1) = m3_ana(i)*cos(r.wt) - m4_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*cos(r.wt)*(r.varxA2xA1(i) - r.varxA2(i))/(1+2*r.k2t^2*r.relXVar(i)) - 2*r.k1t*r.xL1_measurements(i)*sin(r.wt)*(r.varpA2(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
     m4_ana(i+1) = m4_ana(i)*cos(r.wt) + m3_ana(i)*sin(r.wt) - 2*r.k2t*r.pL2_measurements(i)*sin(r.wt)*(r.varxA2xA1(i) - r.varxA2(i))/(1+2*r.k2t^2*r.relXVar(i)) + 2*r.k1t*r.xL1_measurements(i)*cos(r.wt)*(r.varpA2(i) + r.varpA2pA1(i))/(1+2*r.k1t^2*r.sumPVar(i));
end




r.m1_ana = m1_ana;
r.m2_ana = m2_ana;
r.m3_ana = m3_ana;
r.m4_ana = m4_ana;


%%%%%%%%%%%%%%%%%%
idxs = 1:numel(r.ts)-1;

%if r.wt == 0
%    r.dmxm_ana = -2*r.pL2_measurements*r.k12t.*(1./(2*r.k12t^2 + 1./r.relXVar_ana(idxs))); 
%    r.dmpp_ana = +2*r.xL1_measurements*r.k11t.*(1./(2*r.k11t^2 + 1./r.sumPVar_ana(idxs)));
%else
    if trig_harmonic == 0
        r.dmxm_ana = -2*r.pL2_measurements*r.k12t.*(1./(2*r.k12t^2 + 1./r.relXVar_ana(idxs))) + r.wt*(+2*r.xL1_measurements*r.k11t.*(1./(2*r.k11t^2 + 1./r.sumPVar_ana(idxs)))) + ( r.m2(idxs) + r.m4(idxs))*r.wt;
        r.dmpp_ana = +2*r.xL1_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(idxs))) + r.wt*(+2*r.pL2_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.relXVar_ana(idxs)))) + (-r.m1(idxs) + r.m3(idxs))*r.wt;
    else
        r.dmxm_ana = (cos(r.wt) - 1)*(r.m1(idxs)-r.m3(idxs)) + (r.m2(idxs)+r.m4(idxs))*sin(r.wt) - 2*(r.k2t*cos(r.wt)*r.pL2_measurements./(2*r.k2t^2 + 1./(r.relXVar(idxs))) - r.k1t*sin(r.wt)*r.xL1_measurements./(2*r.k1t^2 + 1./(r.sumPVar(idxs))));
        r.dmpp_ana = (cos(r.wt) - 1)*(r.m2(idxs)+r.m4(idxs)) - (r.m1(idxs)-r.m3(idxs))*sin(r.wt) + 2*(r.k2t*sin(r.wt)*r.pL2_measurements./(2*r.k2t^2 + 1./(r.relXVar(idxs))) + r.k1t*cos(r.wt)*r.xL1_measurements./(2*r.k1t^2 + 1./(r.sumPVar(idxs))));
    end
%end

%%%%%%%%%%%%%%%%%%
%mxm = (r.m1(1) - r.m3(1)) + [0, cumsum(-2*r.pL2_measurements*r.kt.*(r.relXVar_ana(2:end)./(1+2*r.kt^2*r.relXVar_ana(2:end))))];
[mxm, mpp] = deal(zeros(size(r.m1)));
mxm(1) = r.m1(1) - r.m3(1);
mpp(1) = r.m2(1) + r.m4(1);
for i = 1:r.nt-1
    %mxm(i+1) = mxm(i) -2*r.pL2_measurements(i)*r.kt.*(r.relXVar_ana(i)./(1+2*r.kt^2*r.relXVar_ana(i)));
    %mpp(i+1) = mpp(i) +2*r.xL1_measurements(i)*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(i)));
    
    mxm(i+1) = mxm(i) + r.dmxm_ana(i);
    mpp(i+1) = mpp(i) + r.dmpp_ana(i);
end

%mxm = (r.m1(1) - r.m3(1)) + [0, cumsum(-2*r.pL2_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.relXVar_ana(2:end))))];
%mpp = (r.m2(1) + r.m4(1)) + [0, cumsum(+2*r.xL1_measurements*r.kt.*(1./(2*r.k^2*r.dt + 1./r.sumPVar_ana(2:end))))];

r.mxm = mxm;
r.mpp = mpp;

