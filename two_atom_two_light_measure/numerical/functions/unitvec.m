function e = unitvec(dim, unit_entry)

e = zeros(dim,1);
e(unit_entry) = 1;

if unit_entry < 1 || unit_entry > dim 
   error('unitentry wrong');
end

end