function r = reduce_time_resolution(r, ts_target)

closest_idxs = zeros(size(ts_target));

ts = r.ts;
ts_idx_pointer = 1;

for i = 1:numel(ts_target)

    t_tgt = ts_target(i);
    
    for j = ts_idx_pointer : numel(ts)
        
        if t_tgt == ts(j)
           closest_idxs(i) = j; 
           ts_idx_pointer  = j;
           break;
           
        elseif t_tgt < ts(j)
            
            if j ~= 1
                if abs(t_tgt - ts(j)) < abs(t_tgt - ts(j-1))
                    closest_idxs(i) = j;
                    ts_idx_pointer  = j;
                else
                    closest_idxs(i) = j-1;
                    ts_idx_pointer  = j-1;
                end
            else
                closest_idxs(i) = 1;
                ts_idx_pointer  = 1;
            end
            
            break;
        end
        
        
    end 
end


ts_target_idxs = closest_idxs ;


nt = numel(r.ts); 
fn = fieldnames(r);
for k=1:numel(fn)
    field = r.(fn{k});
    if isnumeric(field)
        field_dimensions = size(field, 2);

        if field_dimensions == nt % is this time dependent?
            r.(fn{k}) = field(ts_target_idxs);
        end 
    end
end

r.OUx.X = r.OUx.X(ts_target_idxs);
r.OUp.X = r.OUp.X(ts_target_idxs);

r.nt = numel(r.ts);

end