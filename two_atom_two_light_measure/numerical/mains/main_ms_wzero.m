clc
clear all 
close all

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

T = 250*ms;
dt = 1.00e-6;

%%%%%%% OSCILLATORS  %%%%%%%%

w = 0 * w_1ms;
wt  = w * dt;

w1t = + 1*wt;
w2t = + 1*wt;


%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1; % p laser
L2_master = 1; % x laser

k_cst = sqrt(1.83e6);

%k = 0.01 * k_cst; 
k = 1 * k_cst; 
%k = 1/10 * wt / sqrt(dt);

kt  = k * sqrt(dt);

k11t = + 1 * kt * L1_master * A1_master; % * pA1 * pL1
k21t = + 1 * kt * L1_master * A2_master; % * pA2 * pL1
k12t = + 1 * kt * L2_master * A1_master; % * xA1 * xL2
k22t = + 1 * kt * L2_master * A2_master; % * xA2 * xL2


%%%%%%% INITIAL STATE  %%%%%%%%


%%%%%%% MEASUREMENT RNG %%%%%%%%

seed_num = 0; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


trig_harmonic = 1;


%m0 = 

rs(1)     = propagate(T, dt, randn(4,1), w, wt, k, kt, w1t, w2t, k11t,   k12t, k21t,  -k22t, seed_num, trig_harmonic);

k_factor = 10;
rs(end+1) = propagate(T, dt, randn(4,1), w, wt, k_factor*k, k_factor*kt, w1t, w2t, k_factor*k11t,   k_factor*k12t, k_factor*k21t,  -k_factor*k22t, seed_num, trig_harmonic);

k_factor = 100;
rs(end+1) = propagate(T, dt, randn(4,1), w, wt, k_factor*k, k_factor*kt, w1t, w2t, k_factor*k11t,   k_factor*k12t, k_factor*k21t,  -k_factor*k22t, seed_num, trig_harmonic);

k_factor = 300;
rs(end+1) = propagate(T, dt, randn(4,1), w, wt, k_factor*k, k_factor*kt, w1t, w2t, k_factor*k11t,   k_factor*k12t, k_factor*k21t,  -k_factor*k22t, seed_num, trig_harmonic);
%rs(end+1) = propagate(T, dt, m0, w, wt, k, kt, w1t, w2t, k11t, 0*k12t, k21t, 0*k22t, seed_num, trig_harmonic);


r = rs(1);
nruns = numel(rs);

%% 
close all

normalize = 1;  logx = 1;
for i=1:nruns
    plotting(rs(i), normalize, logx) 
end

pause(0.001)
autoArrangeFigures(3,3,2)


%%
% figure
% 
% Fs = dt;
% T  = 1/Fs;
% L  = r.nt;
% t  = r.ts;
% 
% Y = fft(r.m1-r.m3);
% 
% P2 = abs(Y/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = Fs*(0:(L/2))/L;
% plot(f,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
% 
% [val idx] = max(P1)
% f(idx) 
% f(idx) /(dt)