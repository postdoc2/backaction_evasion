%clc
clear all 
%close all

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw1/2(XA1^2 + PA1^2) + hw2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

%T = 1000*ms;
T = 80*ms;
dt = 1.00e-5;

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1.; % p laser, squeezes p
L2_master = 1;  % x laser, squeezes x

k = 1*sqrt(1.83e6); kt  = k * sqrt(dt);
k11t = + 1 * kt * L1_master * A1_master; % * pA1 * pL1
k21t = + 1 * kt * L1_master * A2_master; % * pA2 * pL1
k12t = + 1 * kt * L2_master * A1_master; % * xA1 * xL2
k22t = - 1 * kt * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

w =  1 * w_1ms;
wt  = w * dt;

w1t = + 1*wt;
w2t = - 1*wt;

%%%%%%% FORCE %%%%%%%%

% F = c * f

%c  = 500;
c  = 15000;
ct = c*dt;
f0  = 1; 

gammab = 1*1e2*s^(-1);
sigmab = 100*1*s^(-1);

%%%%%%% INITIAL STATE  %%%%%%%%


%%%%%%% MEASUREMENT RNG %%%%%%%%

measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


%nruns = 3;

m0 = [0; 0; 0; 0; 0];

A0 = diag([1 1 1 1 1]);
%A0 = A0+ 10*diag(diag(rand(5)))
%A0 = diag([5 1 5 1 1]);
%A0 = A0 + (rand(1);
%A0 = diag((1+rand(1))*[1 1 1 1])

trig_harmonic = 1;


rs = {}; 
factor = 0.1;
rs{end+1} = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, factor*k, factor*kt, 0*w1t, 0*w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$0.1 k_{11}$';

factor = 1;
rs{end+1} = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, factor*k, factor*kt, 0*w1t, 0*w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$1 k_{11}$';

factor = 10;
rs{end+1} = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, factor*k, factor*kt, 0*w1t, 0*w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$10 k_{11}$';


factor = 0.1;
rs{end+1} = propagate_with_F(T, dt, A0, m0, w, wt, factor*k, factor*kt, w1t, w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$0.1 k_{11}, \omega$';

factor = 1;
rs{end+1} = propagate_with_F(T, dt, A0, m0, w, wt, factor*k, factor*kt, w1t, w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$1 k_{11}, \omega$';

factor = 10;
rs{end+1} = propagate_with_F(T, dt, A0, m0, w, wt, factor*k, factor*kt, w1t, w2t, factor*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); 
rs{end}.label = '$10 k_{11}, \omega$';

%rs(2) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 2*k, 2*kt, 0*w1t, 0*w2t, 2*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
r = rs(1);

%for i = 1:nruns
    %m0 = 0*rand(4,1);
    %r = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);
    %rs(i) = r;
%end

nruns = numel(rs);



%% 
%close all

figure



the_title = [
    sprintf('T = %.2e s, dt = %.2e s\n', T, dt) ...
    sprintf('diag A(0) = (%.2f, %.2f, %.2f, %.2f, %.2f)\n',A0(1,1), A0(2,2), A0(3,3), A0(4,4), A0(5,5)) ...
    sprintf('$k = %.1f, ~ \\omega = %.1f $\n', k, w) ...
    sprintf('$c = %.1f, ~ \\gamma_b = %.1f,~ \\sigma_b = %.1f $', c, gammab, sigmab)
]

sgtitle(the_title);
%sgtitle(['$' 'T = ' num2str(T) ', dt = ' num2str(dt) ', k = ' num2str(k), ', \omega = ' num2str(w), ', c = ' num2str(c) ', \gamma_b = ' num2str(gammab) ', \sigma_b = ' num2str(sigmab) '$'])

sub_ax1 = subplot(2,2,1);
hold(sub_ax1);

sub_ax2 = subplot(2,2,2);
hold(sub_ax2);

sub_ax3 = subplot(2,2,3);
hold(sub_ax3);

sub_ax4 = subplot(2,2,4);
hold(sub_ax4);

for i=1:nruns
    r = rs{i};
   
    if r.w == 0
        lt = '-';
    else
        lt = '--';
    end
    
    if r.gammabp == 0 
        ax1 = sub_ax1;
        ax2 = sub_ax3;
    else
        ax1 = sub_ax2;
        ax2 = sub_ax4;
    end
    
    plot(ax1, r.ts, r.varFp, lt, 'displayname', r.label)
    plot(ax2, r.ts, r.m5, lt, 'displayname', r.label)
end

plot(sub_ax3, r.ts, rs{1}.fs, 'k--')
plot(sub_ax4, r.ts, rs{end}.fs, 'k--')

set(sub_ax1, 'xscale', 'log', 'yscale', 'log')
set(sub_ax2, 'xscale', 'log', 'yscale', 'log')

set(sub_ax3, 'xscale', 'log')
set(sub_ax4, 'xscale', 'log')

title(sub_ax1, 'var(F)')
title(sub_ax2, 'var(F)')
title(sub_ax3, '$m_F$')
title(sub_ax4, '$m_F$')


leg1 = legend(sub_ax1);
leg1.Location = 'southwest';

leg2 = legend(sub_ax2);
leg2.Location = 'southwest';
pause(0.001)
autoArrangeFigures(2,2,2)

return

%% 
close all

% figure
% tiledlayout flow 
% nexttile
% hold on 
% plot(r.ts, r.fs,'displayname', '$f$')
% xlabel('$t$')
% plot(r.ts, r.m5,'displayname', '$m_5$')
% title('$p_{A_1} \rightarrow p_{A_1} + F\delta t = p_{A_1} + (c\delta t) f$')
% leg = legend;
% 
% nexttile
% loglog(r.ts, sqrt(r.varF), 'displayname', '$\Delta f(t)$')
% varF_ana = r.varF(1)./(1 + 2/3*r.c^2 * r.varF(1)*r.k1^2*r.ts.^3 .* (1 + 0.5 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts)./(1 + 2 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts));
% hold on
% plot(r.ts, sqrt(varF_ana),'--', 'displayname', '$\Delta f(t)_\mathrm{ana}$')
% xlabel('$t$')
% leg = legend;


normalize = 1;  logx = 1;
for i=1:nruns
    plotting(rs(i), normalize, logx) 
end

pause(0.001)
autoArrangeFigures(2,1,1)

return
%%
r = rs{5};
%r = rs{7};
%r = rs{8};
try 
    close(f_walk)
catch
    
end
f_walk = figure;
pause(0.001)
autoArrangeFigures(2,1,2)
plotting_rotation(r)
return




%%
% figure
% 
% Fs = dt;
% T  = 1/Fs;
% L  = r.nt;
% t  = r.ts;
% 
% Y = fft(r.m1-r.m3);
% 
% P2 = abs(Y/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = Fs*(0:(L/2))/L;
% plot(f,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
% 
% [val idx] = max(P1)
% f(idx) 
% f(idx) /(dt)