clc
clear all 
close all

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

T = 5*ms;
w = 0 * w_1ms;
m0 = [1; 0; 2; 0];
seed_num = 0; % 0: random 

ks  = [0, 0.1, 1] * sqrt(1.83e6); 
dts = [1e-6, 1e-7, 1e-8];

n_dts = numel(dts);
n_ks  = numel(ks);

l = 1;

for i = 1:n_dts
    dt = dts(i);
    for j = 1:n_ks
        k = ks(j);

        kt  = k * sqrt(dt);
        k11t =  kt;
        k21t =  kt;
        k12t =  kt;
        k22t = -kt;


        wt  = w * dt;
        w1t =   wt;
        w2t = - wt;

        r = propagate(T, dt, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num);
        rs(l) = r;
        l = l+1;
    end
end

%% 
close all
normalize = 1;  logx = 1;
for i=1:numel(rs)
    plotting(rs(i), normalize, logx) 
end

pause(0.001)
autoArrangeFigures(3,3,2)


%%
% figure
% 
% Fs = dt;
% T  = 1/Fs;
% L  = r.nt;
% t  = r.ts;
% 
% Y = fft(r.m1-r.m3);
% 
% P2 = abs(Y/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = Fs*(0:(L/2))/L;
% plot(f,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
% 
% [val idx] = max(P1)
% f(idx) 
% f(idx) /(dt)