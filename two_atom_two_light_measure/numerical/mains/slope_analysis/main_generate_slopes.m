clear all 
%close all

%rng('default');

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)
% Forces: acting on indep. on XA1 and PA1
% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H/hbar = +w1/2(XA1^2 + PA1^2) + w2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

%T = 1000*ms;
T = 100*ms;
dt = 1.0e-5;
time = Time('ts', 0:dt:T);

nks = 15;
nws = 15;
ncs = 15;

normalize_ws_with_k = 1;
normalize_cs_with_k = 0;


k_option = 1;

if k_option == 1
    
    ks = logspace(-3, 0.5, nks) * sqrt(1.83e6);

elseif k_option == 2

    ks = logspace(0.5, 1, 4) * sqrt(1.83e6);

else
    error('no k option');
end



if normalize_ws_with_k
    ws = logspace(-2, 2, nws); % scaled with k in the loop
else
    ws = logspace(-4, 1, nws) * w_1ms;
end



if normalize_cs_with_k
    
else
    cs = logspace(+0, 5,  ncs);
end

%%%%%%% FORCE %%%%%%%%

f = 1;
gamma = 0;
sigma = 0;

fx0  = f;
gammabx = gamma;
sigmabx = sigma;
fp0  = f;
gammabp = gamma;
sigmabp = sigma;

m0 = [0; 0; 0; 0; 0; 0];

%A0 = diag([1 1 1 1 1 1]);
A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%outroot = ['/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/slope_analysis/output/' DataHash(sum(ws) + sum(cs) + sum(ks))]
outroot = ['/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/slope_analysis/output/' char(datetime)]


E_covar_initial_val = 10000;
OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);
with_PQS = 0;

for q = 1:numel(ks)
    k = ks(q)
    rs = {};

    % ws = logspace(-2, 2, nws) * k;
  %  cs = logspace(-2, 2, ncs) * k;
    
    for i = 1:numel(ws)
      
        if normalize_ws_with_k
            w = ws(i) * k;
        else
            w = ws(i);
        end
            
        parfor j = 1:numel(cs)

            if normalize_cs_with_k
                c = cs(j) * k;
            else
                c = cs(j);
            end
            

            p = struct('w1',   w, 'w2', -  w, 'k11', k, 'k21', k, 'k12',   k, 'k22', -k, 'cx', c, 'cp', c);
            r = analytical_forms( propagate_with_F(A0, m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, p.cx, OUx, p.cp, OUp, i*measurement_seed_num)); 

            if with_PQS
                r = PQS(r, 'E_covar_initial_val', E_covar_initial_val);
            end

            r_trunc = struct;
            r_trunc.ts = r.ts;
            r_trunc.k = k;
            r_trunc.w = w;
            r_trunc.c = c;
            r_trunc.varFp = r.varFp;
            r_trunc.varFx = r.varFx;
            r_trunc.relXVar = r.relXVar;
            r_trunc.sumPVar = r.sumPVar;

            r_trunc.normalize_ws_with_k = normalize_ws_with_k;
            r_trunc.normalize_cs_with_k = normalize_cs_with_k;

            rs{i,j} = r_trunc;
        end
    end

    if ~exist(outroot,'dir'); mkdir(outroot); end
    save([outroot '/' 'rs_k=' num2str(k,5) '.mat'], 'rs', 'k', 'ks','ws','cs')

end
%%
slope_mins = zeros(numel(ws), numel(cs));

variable = 'varFp';
%variable = 'relXVar';
for i = 1:numel(ws)-1
    for j=1:numel(cs)
        r = rs{i,j};
        try
            [ts_windows_begin, slopes] = slope_extractor(r, variable, 0.499, 30, 0);
        
            slopes = smooth(slopes,5);
            %figure
          %  semilogx(ts_windows_begin, smooth(slopes,5),'.')

            [val, idx] = min(slopes);

            slope_mins(i,j) = val;
        catch
            slope_mins(i,j) = nan;
        end
    end
end

figure
normalize_with_k = 1;

if normalize_with_k
    im = imagesc(log10(cs/k), log10(ws/k), slope_mins);
    xlabel('$\log_{10} (c_x/k)$')
    ylabel('$\log_{10} (\omega/k)$')
else
    im = imagesc(log10(cs), log10(ws), slope_mins);
    xlabel('$\log_{10} c_x$')
    ylabel('$\log_{10} \omega$')
end
set(gca,'ydir','normal')
colorbar

title(['min. slope of ' variable ', k=' num2str(k,3)])
set(im, 'Alphadata', ~isnan(slope_mins))


%r = rs{1};
return




















%%
figure
%subplot(numel(cs), numel(ws),1)
[ha, pos] = tight_subplot(numel(ws), 1, 0, 0.01, 0.01);

for i = 1:numel(ws)
    axes(ha(end-i+1))
    for j=1:numel(cs)
        %subplot(numel(cs), numel(ws), i + (j-1)*numel(ws))
        r = rs{i,j};
        loglog(r.ts, r.('relXVar'))
        hold on
    end
    loglog(r.ts, 1e-3./r.ts,'--')
    loglog(r.ts, 1e-10./r.ts.^3,'--')
        
end

title(ha(1), 'relXvar')

return
%% plotting





%%
do_plot = 0;

values = zeros(numel(rs), 3);
for i = 1:numel(rs)

    r = rs{i};
    
    [ts_windows_begin, slopes] = slope_extractor(r, 'varFp', 0.499, 30, do_plot);

  

    slopes = smooth(slopes,5);
    %figure
  %  semilogx(ts_windows_begin, smooth(slopes,5),'.')

    [val, idx] = min(slopes);
    
    values(i,:) = [r.w, r.cx,  val];
end
figure
%scatter3(log10(values(:,1)), log10(values(:,2)), values(:,3))
surf(log10(values(:,1)), log10(values(:,2)), values(:,3))
xlabel('$\log_{10}\omega$')
ylabel('$\log_{10} c_x$')

return
%%

close all

normalize = 0;
logx = 1;

%opts = {'fxmean','fxvar','fpvar', 'fpmean','varrel','var'}
opts = {'fpvar','varrel','fxmean'};
for i = 1:numel(rs)
    continue
    plotting(rs{i}, normalize, logx, opts);
    nexttile(1)
    factor_inv_ts = 1e-2;
    factor_inv_ts_cubed = 1e-6;
    loglog(r.ts, sqrt(1./r.ts) * factor_inv_ts,'--', 'linewidth', 1.5, 'displayname','$\propto\frac{1}{t}$'); hold on
    loglog(r.ts, sqrt(1./r.ts.^3) * factor_inv_ts_cubed,'--', 'linewidth', 1.5,'displayname','$\propto\frac{1}{t^3}$'); hold on
    
    %set(nexttile(3), 'xscale','log')
end


pause(0.001)
autoArrangeFigures(2,4,2)

%%
%close all
r = rs{1};



figure


ts_full = r.ts; 
var_full = r.varFp;

loglog(ts_full, var_full)
hold on


thresh = 0.49;
idxs = var_full < thresh;
ts = ts_full(idxs);
var = var_full(idxs);

loglog(ts, var)

n_segments = 40;

%log_ts = log

segment_points =  logspace(log10(ts(1)), log10(ts(end)), n_segments+1);
%loglog(segment_points, segment_points*0 + 1e-1, '.')

segment_points_idx = 0*segment_points;
for i = 1:n_segments + 1
    [~, idx] = min(abs(ts - segment_points(i)));
    segment_points_idx(i) = idx;
end

if length(segment_points_idx) ~= numel(unique(segment_points_idx))
    error('too many segments, non-unique indices')
end

slope = zeros(1,n_segments);
for i = 1:n_segments
    
    min_idx = segment_points_idx(i);
    max_idx = segment_points_idx(i+1);
    
    window = [min_idx:max_idx];
    
    coeffs = polyfit(log10(ts(window)), log10(var(window)), 1); % f(x) = b*x^m ---> log10(f(x)) = log10(x^m) + log10(b) = m * log10(x) + log10(b)
    slope(i) = coeffs(1);
    
    loglog(ts(window), var(window))
    
end

yyaxis right
plot(ts(segment_points_idx(1:end-1)), slope ,'r.')
set(gca,'yscale','linear')
return





ts_window = ts(window);
var_window = varFx(window);
loglog(ts_window, var_window)


coeffs = polyfit(log10(ts_window), log10(var_window), 1)

pause(0.01)
autoArrangeFigures(2,4,2)

loglog(ts, (1./ts.^3) * 1e-9)

