% datapath = 'fixed_ks_ws_cs';

%datapath = 'fixed_ks_ws_cs';
%datapath = 'fixed_ks_cs_scaled_ws';
%datapath = 'fixed_ks_cs_scaled_ws'; 

datapath = 'fixed_ks_cs_scaled_ws_with_epr';


% get and sort files according to k value 
datapath = ['/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/slope_analysis/output/' datapath];
files = dir(fullfile(datapath, '*.mat'))
for i = 1:numel(files)
    file = files(i); 
    the_k = load([file.folder '/' file.name], 'k');
    ks(i) = the_k.k;
end
[~, sorted_idxs] = sort(ks);
files = files(sorted_idxs);

% for each k, do analysis

normalize_cs_with_k = 0;
normalize_ws_with_k = 1;

plot_durations_instead_of_slope_mins = 0;

close all
figure
tiledlayout flow
for q = 1:numel(files)

    file = files(q); 
    load([file.folder '/' file.name])
    
    slope_mins = zeros(numel(ws), numel(cs));
    cubed_slope_duration = zeros(numel(ws), numel(cs));

    variable = 'varFp';
    %variable = 'relXVar';
    for i = 1:numel(ws)
        for j=1:numel(cs)
            r = rs{i,j};
            try
                %close all
                [ts_windows_begin, slopes] = slope_extractor(r, variable, 0.499, 30, 0);

                slopes = smooth(slopes,5);
                %figure
              %  semilogx(ts_windows_begin, smooth(slopes,5),'.')

                [val, idx] = min(slopes);

                slope_mins(i,j) = val;
                
                 if -2.8 >= val
                     
                     ts_windows_begin = ts_windows_begin(idx:end);
                     slopes = slopes(idx:end);
                     
                     t1 = ts_windows_begin(1);
                     above_threshold = find(slopes > -1.2, 1);
                     if ~isempty(above_threshold)
                         t2 = ts_windows_begin(above_threshold);
                     %    dummy = 1;
                     else
                         t2 = ts_windows_begin(end);
                     end
                     
                     cubed_slope_duration(i,j) = t2-t1;
                     
                     
                 else
                     cubed_slope_duration(i,j) = nan;
                 end
                
                 
                
            catch
                slope_mins(i,j) = nan;
                cubed_slope_duration(i,j) = nan;
            end
        end
    end

%    figure
    nexttile
    x_values = cs;
    y_values = ws;
    if normalize_cs_with_k;     x_values = x_values / k;    end
    if normalize_ws_with_k;     y_values = y_values / k;    end
    
    
    if ~plot_durations_instead_of_slope_mins
        zdata = slope_mins;
    else
        zdata = cubed_slope_duration;
    end
    
    im = imagesc(log10(x_values), log10(y_values), zdata);
    
    if normalize_cs_with_k;     xlabel('$\log_{10} (c_x/k)$');       else;     xlabel('$\log_{10} c_x$'); end
    if normalize_ws_with_k;     ylabel('$\log_{10} (\omega/k)$');    else;     ylabel('$\log_{10} \omega$'); end
    
%     if normalize_with_k
%         im = imagesc(log10(cs/k), log10(ws/k), slope_mins);
%         xlabel('$\log_{10} (c_x/k)$')
%         ylabel('$\log_{10} (\omega/k)$')
%     else
%         im = imagesc(log10(cs), log10(ws), slope_mins);
%         xlabel('$\log_{10} c_x$')
%         ylabel('$\log_{10} \omega$')
%     end
    set(gca,'ydir','normal')
    colorbar

    %title(['min. slope of ' variable ', k=' num2str(k,5)])
    title(['k=' num2str(k,5)],'fontsize',13)
    set(im, 'Alphadata', ~isnan(zdata))
    
    
end

pause(0.001);
%autoArrangeFigures(4,6,2)
autoArrangeFigures(1,1,2)
sgtitle(['min. slope of ' variable])


savename = 'plots';

if normalize_cs_with_k;     savename = [savename '_csNorm']; end
if normalize_ws_with_k;     savename = [savename '_wsNorm']; end

%saveas(gcf,[datapath '/' savename '.png'])

%r = rs{1};
return

%% look at epr variables 

k_idx = 12;

file = files(k_idx); 
load([file.folder '/' file.name])

figure
%tiledlayout flow
tiledlayout(numel(cs), numel(ws),'Padding', 'none','TileSpacing','none')
%ha = tight_subplot(numel(cs), numel(ws), 0, 0.05, 0.05)

for i = 1:numel(ws)
    for j=1:numel(cs)
        
        r = rs{i,j};
        
        nexttile
        %axes(ha(sub2ind(size(rs), j, i)));
        
        loglog(r.ts, (r.relXVar));
        hold on
        loglog(r.ts, (r.sumPVar),'--');
        
        
        loglog(r.ts, 1e-6 *1./r.ts.^1,'k:')
        %loglog(r.ts, 1e-8 *1./r.ts.^2,'k:')
        %loglog(r.ts, 1e-10 *1./r.ts.^3,'k:')
        
      %  title(['w/k: ' num2str(r.w/k,2) '  c: ' num2str(r.c,2)])
        
        ylim([1e-5, 1e0])
    end
end

%%
figure

w_idx = 13;
c_idx = 13;

r = rs{w_idx,c_idx};        

loglog(r.ts, r.relXVar);
hold on 
loglog(r.ts, r.sumPVar,'--');
        
        
loglog(r.ts, 1e-6 *1./r.ts.^1,'k:')
loglog(r.ts, 1e-8 *1./r.ts.^2,'k:')

title(['w/k: ' num2str(r.w/k) '  c: ' num2str(r.c)])

loglog(r.ts, r.varFp)


return

%%

figure
im = imagesc(log10(x_values), log10(y_values), cubed_slope_duration,'AlphaData', ~isnan(cubed_slope_duration));
set(gca,'ydir','normal')
colorbar

return
%%
close all
i = 2;
file = files(end); 
load([file.folder '/' file.name])
variable = 'varFp';
%variable = 'relXVar';

for i = 1:15
    for j = 1:15
        r = rs{i,j};
        [ts_windows_begin, slopes] = slope_extractor(r, variable, 0.499, 30, 1);
        pause(0.001)
        autoArrangeFigures(1,1,2)
        close
    end
end
   

%%
close all
i = 2;
file = files(end); 
load([file.folder '/' file.name])
variable = 'varFp';
%variable = 'relXVar';

r = rs{6,10};
[ts_windows_begin, slopes] = slope_extractor(r, variable, 0.499, 30, 1);
