clear all 
%close all

%rng('default');

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H/hbar = +w1/2(XA1^2 + PA1^2) + w2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

%%%%%%% TIME %%%%%%%%

%T = 1000*ms;
T = 20*ms;
dt = 1.0e-6;
time = Time('ts', 0:dt:T);

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 0;
L1_master = 1; % p laser, squeezes p
L2_master = 0; % x laser, squeezes x

kfactor = 1;
k = kfactor * 0.1*sqrt(1.83e6); 
k11 = + 1 * k * L1_master * A1_master; % * pA1 * pL1
k21 = + 1 * k * L1_master * A2_master; % * pA2 * pL1
k12 = + 1 * k * L2_master * A1_master; % * xA1 * xL2
k22 = - 1 * k * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

w =  0.1 * w_1ms;

w1 = + 1*w * A1_master;
w2 = - 1*w * A2_master;

%%%%%%% FORCE %%%%%%%%

% F = c * f

fx0  = 4;
cx  = 15000;
gammabx = 1e2*s^(-1);
sigmabx = 1*1e1*s^(-1);

fp0  = 4;
cp  = 15000;
gammabp = 1e2*s^(-1);
sigmabp = 1*1e1*s^(-1);

%%%%%%% INITIAL STATE  %%%%%%%%

m0 = [0; 0; 0; 0; 0; 0];

%A0 = diag([1 1 1 1 1 1]);
A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

%A0 = A0+ 10*diag(diag(rand(5)))
%A0 = diag([5 1 5 1 1]);
%A0 = A0 + (rand(1);
%A0 = diag((1+rand(1))*[1 1 1 1])


%%%%%%% RNG %%%%%%%%

OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

E_covar_initial_val = 10000;
OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

nruns = 1;
rs = {};
for i = 1:nruns
    rs{end+1} = analytical_forms( PQS ( propagate_with_F(A0, m0, k11, k12, k21, k22, w1, w2, cx, OUx, cp, OUp, i*measurement_seed_num), 'E_covar_initial_val', E_covar_initial_val) ); 
end
r = rs{1};

%% plotting

%close all

normalize = 0;
logx = 1;

plotting(r, normalize, logx, {'fxmean','fxvar','fpvar', 'fpmean','varrel','var'});
pause(0.001)
%autoArrangeFigures(2,1,2)
return

ROI_offset = 2*ms;
time_limits = [ROI_offset, OUp.time.ts(end) - ROI_offset];
slice_idxs = find(OUp.time.ts >= time_limits(1), 1) : find(OUp.time.ts >= time_limits(2), 1);

[MSEp_forward, MSEp_PQS, mean_varP_forward, mean_varP_PQS, mean_fp_forward, mean_fp_PQS] = deal(zeros(1,OUp.time.nt));
[MSEx_forward, MSEx_PQS, mean_varX_forward, mean_varX_PQS, mean_fx_forward, mean_fx_PQS] = deal(zeros(1,OUx.time.nt));

for i = 1:nruns
    mean_fx_forward = mean_fx_forward + rs{i}.m5;
    mean_fx_PQS = mean_fx_PQS + rs{i}.mFx_PQS;
    
    mean_fp_forward = mean_fp_forward + rs{i}.m6;
    mean_fp_PQS = mean_fp_PQS + rs{i}.mFp_PQS;
end

mean_fx_forward = mean_fx_forward / nruns;
mean_fx_PQS = mean_fx_PQS / nruns;
mean_fp_forward = mean_fp_forward / nruns;
mean_fp_PQS = mean_fp_PQS / nruns;


for i = 1:nruns
   % mean_var_forward = mean_var_forward + rs{i}.varF;
   % mean_var_PQS = mean_var_PQS + rs{i}.varF_PQS;
    
    mean_varX_forward = mean_varX_forward + (rs{i}.m5 - mean_fx_forward).^2;
    mean_varX_PQS = mean_varX_PQS + (rs{i}.mFx_PQS - mean_fx_PQS).^2;
   
    mean_varP_forward = mean_varP_forward + (rs{i}.m6 - mean_fp_forward).^2;
    mean_varP_PQS = mean_varP_PQS + (rs{i}.mFp_PQS - mean_fp_PQS).^2;
    
    MSEx_forward = MSEx_forward   + (rs{i}.m5 - OUx.X).^2;
    MSEx_PQS     = MSEx_PQS       + (rs{i}.mFx_PQS - OUx.X).^2;
    
    MSEp_forward = MSEp_forward   + (rs{i}.m6 - OUp.X).^2;
    MSEp_PQS     = MSEp_PQS       + (rs{i}.mFp_PQS - OUp.X).^2;
end

MSEx_forward = MSEx_forward / nruns;
MSEx_PQS     = MSEx_PQS / nruns;
mean_varX_forward = mean_varX_forward / nruns;
mean_varX_PQS = mean_varX_PQS / nruns;


MSEp_forward = MSEp_forward / nruns;
MSEp_PQS     = MSEp_PQS / nruns;
mean_varP_forward = mean_varP_forward / nruns;
mean_varP_PQS = mean_varP_PQS / nruns;


if 0
    nexttile
    hold on 
    plot(OUp.time.ts(slice_idxs), OUp.X(slice_idxs),'displayname','$f(t)$')
    plot(OUp.time.ts(slice_idxs), mean_fp_forward(slice_idxs),'displayname','$\langle f_p^{f}(t)\rangle$')
    plot(OUp.time.ts(slice_idxs), mean_fp_PQS(slice_idxs),'displayname','$\langle f_p^{PQS}(t)\rangle$')
    leg = legend;
end

nexttile

plot(nan, 'w','displayname', '')
hold on
plot(OUp.time.ts(slice_idxs), MSEx_forward(slice_idxs), 'displayname', sprintf('Forward - err: %.1e', 1/r.T * sum(MSEx_forward(slice_idxs))*OUx.time.dt))
plot(OUp.time.ts(slice_idxs), MSEx_PQS(slice_idxs), 'displayname', sprintf('PQS - err: %.1e', 1/r.T * sum(MSEx_PQS(slice_idxs))*OUx.time.dt))
plot(OUp.time.ts(slice_idxs), mean_varX_forward(slice_idxs),'--', 'displayname', sprintf('Forward var'))
plot(OUp.time.ts(slice_idxs), mean_varX_PQS(slice_idxs),'--', 'displayname', sprintf('PQS var'))

leg = legend;
leg.Location = 'bestoutside';
title('$\mathrm{MSE}_x(t) = \frac{1}{M} \sum_i (f_\mathrm{x,est, i}(t) - f_x(t) )^2$')

nexttile

plot(nan, 'w','displayname', '')
hold on
plot(OUp.time.ts(slice_idxs), MSEp_forward(slice_idxs), 'displayname', sprintf('Forward - err: %.1e', 1/r.T * sum(MSEp_forward(slice_idxs))*OUp.time.dt))
plot(OUp.time.ts(slice_idxs), MSEp_PQS(slice_idxs), 'displayname', sprintf('PQS - err: %.1e', sum(1/r.T * MSEp_PQS(slice_idxs))*OUp.time.dt))
plot(OUp.time.ts(slice_idxs), mean_varP_forward(slice_idxs),'--', 'displayname', sprintf('Forward var'))
plot(OUp.time.ts(slice_idxs), mean_varP_PQS(slice_idxs),'--', 'displayname', sprintf('PQS var'))

leg = legend;
leg.Location = 'bestoutside';
title('$\mathrm{MSE}_p(t) = \frac{1}{M} \sum_i (f_\mathrm{p,est, i}(t) - f_p(t) )^2$')

autoArrangeFigures(1,3,2)
