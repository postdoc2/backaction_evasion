clc
clear all 
close all

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

%T = 5*ms;
%T = 5*mus;
T  = 1*ns;
dt = 1e-14;

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1; % p laser
L2_master = 1; % x laser

k = 1 * sqrt(1.83e6); kt  = k * sqrt(dt);
k11t = + 1 * kt * L1_master * A1_master; % * pA1 * pL1
k21t = + 1 * kt * L1_master * A2_master; % * pA2 * pL1
k12t = + 1 * kt * L2_master * A1_master; % * xA1 * xL2
k22t = - 1 * kt * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

%w  = 5*(2*pi/T);

%wt  = w * dt;
wt  = 1.42*kt; w = wt/dt; % about 1.42 changes the squeezing @ T = 1*ns, dt = 1e-14, k = 1 * sqrt(1.83e6);

w1t = + 1*wt;
w2t = - 1*wt;

%%%%%%% INITIAL STATE  %%%%%%%%

m0 = [1; 0; -1; 0];

%%%%%%% MEASUREMENT RNG %%%%%%%%

seed_num = 2; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


normalize = 1;  logx = 1;


r = propagate(T, dt, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num);
plotting(r, normalize, logx)
pause(0.001)
autoArrangeFigures(2,2,3)

r = propagate(T, dt, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num);
plotting(r, normalize, logx)
pause(0.001)
autoArrangeFigures(2,2,3)