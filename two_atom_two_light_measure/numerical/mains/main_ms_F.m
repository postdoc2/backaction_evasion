%clc
clear all 
%close all

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

T = 150*ms;
dt = 1.00e-5;
time = Time('ts', 0:dt:T);

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1.2; % p laser, squeezes p
L2_master = 1; % x laser, squeezes x

k = 0.016*sqrt(1.83e6); 
k11 = + 1 * k * L1_master * A1_master; % * pA1 * pL1
k21 = + 1 * k * L1_master * A2_master; % * pA2 * pL1
k12 = + 1 * k * L2_master * A1_master; % * xA1 * xL2
k22 = - 1 * k * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

w =  0.0 * w_1ms;
w1 = + 1*w;
w2 = - 1*w;

%%%%%%% FORCE %%%%%%%%

% F = c * f

%c  = 500;
c  = 15000;
ct = c*dt;
f0  = 1; 

gammab = 3*1e2*s^(-1);
sigmab = 0.1*1*s^(-1);

%%%%%%% INITIAL STATE  %%%%%%%%


%%%%%%% MEASUREMENT RNG %%%%%%%%

measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


%nruns = 3;

m0 = [1; 0; 1; 0; 0];

A0 = diag([1 1 1 1 1]);
%A0 = A0+ 10*diag(diag(rand(5)))
%A0 = diag([5 1 5 1 1]);
%A0 = A0 + (rand(1);
%A0 = diag((1+rand(1))*[1 1 1 1])

make_OU = @(gammab, sigmab) OrnsteinUhlenbeckProcess(time, gammab, 0, sigmab).Ito_SDE(f0);

%OU = OrnsteinUhlenbeckProcess(time, gammab, 0, sigmab).Ito_SDE(f0);
%rs(1) = propagate_with_F(A0, m0, k, k11, k12, k21, k22, w, w1, w2, 0*c, make_OU(0*gammab, 0*sigmab), measurement_seed_num);
rs(1) = propagate_with_F(A0, m0, k11, k12, k21, k22, w1, w2, c, make_OU(0*gammab, 0*sigmab), measurement_seed_num);

%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, 0*w2t, k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(3) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, 0*w2t, k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);

%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, 0*w2t, k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);

%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, k21t, k22t, c, ct, f0, gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, k21t, k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 10*k, 10*kt, 0*w1t, 0*w2t, 10*k11t, 10*k12t, 10*k21t, 10*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 2*k, 2*kt, 0*w1t, 0*w2t, 0*k11t, 2*k12t, 0*k21t, 0*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 2*k, 2*kt, 0*w1t, 0*w2t, 2*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);

%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 0*k, 0*kt, 0*w1t, 0*w2t, 0*k11t, 0*k12t, 0*k21t, 0*k22t, 100*c, 100*ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, c, ct, f0, gammab, 0*sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, 10*k, 10*kt, w1t, w2t, 10*k11t, 10*k12t, 10*k21t, 10*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, 2*k, 2*kt, w1t, w2t, 0*k11t, 2*k12t, 0*k21t, 2*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);

%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, k21t, k22t, c, ct, f0, gammab, sigmab, seed_num, trig_harmonic);
%rs(1) = propagate_with_F(T, dt, A0, m0, w, wt, 2*k, 2*kt, w1t, w2t, 0*k11t, 2*k12t, 0*k21t, 2*k22t, c, ct, f0, gammab, sigmab, seed_num, trig_harmonic);


% KLAUS
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); % god, 1/t^3
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, 0*k11t, k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); % dårlig
%rs(1) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, 0*k21t, 0*k22t, c, ct, f0, 0*gammab, 0*sigmab, measurement_seed_num, trig_harmonic); % ødelægger squeezing, klaus forventer 1/t


%rs(2) = propagate_with_F(T, dt, A0, m0, 0*w, 0*wt, 2*k, 2*kt, 0*w1t, 0*w2t, 2*k11t, 0*k12t, 0*k21t, 0*k22t, c, ct, f0, gammab, sigmab, measurement_seed_num, trig_harmonic);
r = rs(1);

%for i = 1:nruns
    %m0 = 0*rand(4,1);
    %r = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);
    %rs(i) = r;
%end

nruns = numel(rs);
%% 
close all

% figure
% tiledlayout flow 
% nexttile
% hold on 
% plot(r.ts, r.fs,'displayname', '$f$')
% xlabel('$t$')
% plot(r.ts, r.m5,'displayname', '$m_5$')
% title('$p_{A_1} \rightarrow p_{A_1} + F\delta t = p_{A_1} + (c\delta t) f$')
% leg = legend;
% 
% nexttile
% loglog(r.ts, sqrt(r.varF), 'displayname', '$\Delta f(t)$')
% varF_ana = r.varF(1)./(1 + 2/3*r.c^2 * r.varF(1)*r.k1^2*r.ts.^3 .* (1 + 0.5 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts)./(1 + 2 * (r.varpA1(1) + r.varpA2(1)) * r.k1^2*r.ts));
% hold on
% plot(r.ts, sqrt(varF_ana),'--', 'displayname', '$\Delta f(t)_\mathrm{ana}$')
% xlabel('$t$')
% leg = legend;


normalize = 1;  logx = 1;
show = {'mean', 'meanrel', 'meaninc', 'var', 'varrel', 'varreldiff', 'fmean', 'fvar'};
for i=1:nruns
    plotting(analytical_forms(rs(i)), normalize, logx, show) 
end

pause(0.001)
autoArrangeFigures(1,1,2)

return
%%
%r = rs(2);
try 
    close(f_walk)
catch
    
end
f_walk = figure;
pause(0.001)
autoArrangeFigures(2,1,1)
plotting_rotation(r)
return




%%
% figure
% 
% Fs = dt;
% T  = 1/Fs;
% L  = r.nt;
% t  = r.ts;
% 
% Y = fft(r.m1-r.m3);
% 
% P2 = abs(Y/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = Fs*(0:(L/2))/L;
% plot(f,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
% 
% [val idx] = max(P1)
% f(idx) 
% f(idx) /(dt)