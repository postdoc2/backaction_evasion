clc
clear all 
%close all

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

T = 5*ms;
dt = 1.00e-7;

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1; % p laser
L2_master = 1; % x laser

k = 0.1 * sqrt(1.83e6); kt  = k * sqrt(dt);
k11t = + 1 * kt * L1_master * A1_master; % * pA1 * pL1
k21t = + 1 * kt * L1_master * A2_master; % * pA2 * pL1
k12t = + 1 * kt * L2_master * A1_master; % * xA1 * xL2
k22t = - 1 * kt * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

w =  1*50 * w_1ms;
wt  = w * dt;

w1t = + 1*wt;
w2t = - 1*wt;

trig_harmonic = 1;
%%%%%%% INITIAL STATE  %%%%%%%%


%%%%%%% MEASUREMENT RNG %%%%%%%%

seed_num = 0; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


%nruns = 3;

m0 = 1*[-1; 0; 1; 0];

A0 = diag([2 1 1 2]); %+ rand(4,4);
%A0(1,2) = 1;
%A0(1,3) = -5; 
%A0(2,4) = 1; 
%A0 = A0+diag(diag(rand(4)))
%A0 = A0 + (rand(1);
%A0 = diag((1+rand(1))*[1 1 1 1])


tic

% ROTATION ONLY
%rs(1) = propagate(T, dt, A0, m0, w, wt, 0*k, 0*kt, w1t, w2t, 0*k11t, 0*k12t, 0*k21t, 0*k22t, seed_num, trig_harmonic); 

% SINGLE SQUEEZING ON ATOM 1 BY X LASER
%rs(1) = propagate(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, 0*k11t, k12t, 0*k21t, 0*k22t, seed_num, trig_harmonic);

% INDIVIDUAL SQUEEZING
rs(1) = propagate(T, dt, diag([1,1,1,1]), m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, 0*k12t, 0*k21t, k22t, seed_num, trig_harmonic);

% BOTH LASERS, SAME SIGNS
%rs(1) = propagate(T, dt, A0, m0, 0*w, 0*wt, 0*k, 0*kt, w1t, w2t, k11t, k12t, k21t, -k22t, seed_num, trig_harmonic);

% BOTH LASERS 
%rs(1) = propagate(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);

% ROTATION AND BOTH LASERS
%rs(1) = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);

% ROTATION AND SINGLE LASER
%rs(1) = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, 0*w2t, 0*k11t, k12t, 0*k21t, 0*k22t, seed_num, trig_harmonic);


%rs(1) = propagate(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, 0*k11t, 0*k12t, k21t, 0*k22t, seed_num, trig_harmonic);
%rs(2) = propagate(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);
%rs(3) = propagate(T, dt, A0, m0, 0*w, 0*wt, k, kt, 0*w1t, 0*w2t, 0.9*k11t, k12t, 0.9*k21t, k22t, seed_num, trig_harmonic);
%rs(4) = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);

toc
r = rs(1);

%for i = 1:nruns
    %m0 = 0*rand(4,1);
    %r = propagate(T, dt, A0, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num, trig_harmonic);
    %rs(i) = r;
%end

nruns = numel(rs);
%% 
close all
normalize = 1;  logx = 1;
for i=1:nruns
    plotting(rs(i), normalize, logx) 
end

pause(0.001)
autoArrangeFigures(2,2,1)


%

try 
    close(f_walk)
catch
    
end
f_walk = figure;

pause(0.001)
autoArrangeFigures(2,1,1)

plotting_rotation(r)

%%
% figure
% 
% Fs = dt;
% T  = 1/Fs;
% L  = r.nt;
% t  = r.ts;
% 
% Y = fft(r.m1-r.m3);
% 
% P2 = abs(Y/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = Fs*(0:(L/2))/L;
% plot(f,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
% 
% [val idx] = max(P1)
% f(idx) 
% f(idx) /(dt)