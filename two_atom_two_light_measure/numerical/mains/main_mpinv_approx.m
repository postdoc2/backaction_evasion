clear all 
%close all

%rng('default');

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)
% Forces: acting on indep. on XA1 and PA1
% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H/hbar = +w1/2(XA1^2 + PA1^2) + w2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

%%%%%%% TIME %%%%%%%%

%T = 1000*ms;
T = 50*ms;
dt = 1.0e-6;
time = Time('ts', 0:dt:T);


k =  0.1*sqrt(1.83e6); 
w =  0.1 * w_1ms;

params = {};


params{end+1} = struct( 'w1', 1*w, 'w2', 1*w, 'k11', 1*k, 'k21', 1*k, 'k12', 1*k, 'k22', -1*k, 'approx_mpinv', 'on');
params{end+1} = struct( 'w1', 1*w, 'w2', 1*w, 'k11', 1*k, 'k21', 1*k, 'k12', 1*k, 'k22', -1*k, 'approx_mpinv', 'off');



%%%%%%% FORCE %%%%%%%%

% F = c * f

fx0  = 4;
cx  = 15000;
gammabx = 0e2*s^(-1);
sigmabx = 0*1e1*s^(-1);

fp0  = 4;
cp  = 15000;
gammabp = 0e2*s^(-1);
sigmabp = 0*1e1*s^(-1);

%%%%%%% INITIAL STATE  %%%%%%%%

m0 = [0; 0; 0; 0; 0; 0];

%A0 = diag([1 1 1 1 1 1]);
A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

%A0 = A0+ 10*diag(diag(rand(5)))
%A0 = diag([5 1 5 1 1]);
%A0 = A0 + (rand(1);
%A0 = diag((1+rand(1))*[1 1 1 1])


%%%%%%% RNG %%%%%%%%

OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

E_covar_initial_val = 10000;
OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

nruns = 1;
rs = {};

with_PQS = 0;

normalize = 0;
logx = 1;

tic
for i = 1:nruns
    for j = 1:numel(params)
         
        p = params{j};
        
        r = analytical_forms( moments_from_results(propagate_with_F(A0, m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, cx, OUx, cp, OUp, i*measurement_seed_num, 'approx_mpinv', p.approx_mpinv))); 
        
        if with_PQS
            r = PQS(r, 'E_covar_initial_val', E_covar_initial_val);
        end
        
        rs{j} = r;
    end
end
toc
r = rs{1};

%%
figure; plot(rs{1}.ts, rs{1}.m6); hold on ; plot(rs{2}.ts, rs{2}.m6,'--');
figure; loglog(rs{1}.ts, rs{1}.varFp); hold on ; loglog(rs{2}.ts, rs{2}.varFp,'--');

%figure; plot(rs{1}.ts, rs{1}.m6); hold on ; plot(rs{2}.ts, rs{2}.m6);
%figure; plot(rs{1}.ts, rs{1}.varFp); hold on ; plot(rs{2}.ts, rs{2}.varFp);
return

%% plotting

close all

%opts = {'fxmean','fxvar','fpvar', 'fpmean','varrel','var'}
opts = {'fxmean','fpmean','fxvar','fpvar'};
for i = 1:numel(rs)
    plotting(rs{i}, normalize, logx, opts);
end


pause(0.001)
autoArrangeFigures(2,4,1)

