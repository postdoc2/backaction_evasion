clc
clear all 
close all

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)

% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H = +hw/2(XA1^2 + PA1^2) - hw/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters


%%%%%%% TIME %%%%%%%%

T = 100*ms;
dt = 1e-5; 
%T = 5*mus;
%T  = 1*ns;
%dt = 1e-14;
time = Time('ts', 0:dt:T);

%%%%%%% LASER COUPLING %%%%%%%%

A1_master = 1;
A2_master = 1;
L1_master = 1; % p laser
L2_master = 1; % x laser

k = 1 * sqrt(1.83e6); 
k11 = + 1 * k * L1_master * A1_master; % * pA1 * pL1
k21 = + 1 * k * L1_master * A2_master; % * pA2 * pL1
k12 = + 1 * k * L2_master * A1_master; % * xA1 * xL2
k22 = - 1 * k * L2_master * A2_master; % * xA2 * xL2

%%%%%%% OSCILLATORS  %%%%%%%%

%w  = 5*(2*pi/T);

%wt  = w * dt;
w  = 1.42*k; % about 1.42 changes the squeezing @ T = 1*ns, dt = 1e-14, k = 1 * sqrt(1.83e6);

w1 =  w;
w2 = -w;

%%%%%%

fx0 = 0;
fp0 = 0;
gammabx = 0;
gammabp = 0;
sigmabx = 0;
sigmabp = 0;

cx = 0;
cp = 0;

%%%%%%% INITIAL STATE  %%%%%%%%

initial_conditions = 3;

A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

if initial_conditions == 0
    
    m0 = zeros(6,1);
    
elseif initial_conditions == 1
    
    m0 = rand(6,1);
    
else
    
    m0 = [1; 0; -1; 0; 0; 0];
    
end


%%%%%%% MEASUREMENT RNG %%%%%%%%

OU_seed = 1;
measurement_seed = 2; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);


normalize = 1;  logx = 1;


r = analytical_forms(propagate_with_F(A0, m0, k11, k12, k21, k22, w1, w2, cx, OUx, cp, OUp, measurement_seed));
%r = propagate(T, dt, m0, w, wt, k, kt, w1t, w2t, k11t, k12t, k21t, k22t, seed_num);
plotting(r, normalize, logx,{'var'})
pause(0.001)
autoArrangeFigures(2,2,2)


