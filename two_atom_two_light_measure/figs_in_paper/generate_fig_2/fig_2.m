clear all
close all
clc

constants
stochastic_project_defaults


ellipse_contour_value_normalized = 0.5; 

%idx = 1;
idx = 50;

save_fig = 1;

with_marginal = 0;

%% generate wigners

T = 0.2*ms;
dt = 1.0e-6;
time = Time('ts', 0:dt:T);

k =  0.*sqrt(1.83e6); 
% w =  1 * w_1ms;  <--- currently in paper
w =  1.7 * w_1ms;


fx0  = 4;
cx  = 0;
gammabx = 0e2*s^(-1);
sigmabx = 0*1e1*s^(-1);

fp0  = 4;
cp  = 0;
gammabp = 0e2*s^(-1);
sigmabp = 0*1e1*s^(-1);

m0 = [0; -1.9; 0; 0; 0; 0];


A0 = diag([2, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 

E_covar_initial_val = 10000;
OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);


p = struct('w1',   w, 'w2', -  0*w, 'k11', k, 'k21', k, 'k12',   k, 'k22', -k);

r = analytical_forms( propagate_with_F(A0, m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, cx, OUx, cp, OUp, 1*measurement_seed_num)); 
W_ellipses = Wigner_ellipses(r, 'A1', ellipse_contour_value_normalized, 100);




Gamma = r.gs([1,2], [1,2], idx);
covar = 0.5 * Gamma;

m = r.ys([1,2], idx);
xs = linspace(-5,5,500);
ys = xs;

[X1,X2] = meshgrid(xs,ys);
X = [X1(:) X2(:)];
matlab_normal_pdf = mvnpdf(X, m', covar);
y_pdf = reshape(matlab_normal_pdf,length(ys),length(xs));

dx = xs(2)-xs(1);
dy = ys(2)-ys(1);

%y_norm = y / max(max(y));

W = zeros(numel(xs), numel(ys));


inv_Gamma = inv(Gamma) ;
for i = 1:numel(xs)
    for j=1:numel(ys)
        pt = [xs(i); ys(j)];       
        W(j,i) = 1 /(pi * sqrt(det(Gamma))) * exp(-(pt - m)' * inv_Gamma * (pt-m));
    end
end
max_W = max(max(W));


%figure
%surf(y)
%shading interp
%figure
%surf(y_pdf)
%shading interp

sum(sum(W)) * dx * dy
sum(sum(y_pdf)) * dx * dy

%y = 1 /(pi * det(covar) * exp(-())
%% plot 

fontsize = 15; 
markersize = 8;


figure
%imagesc(x1,x2, y_norm);
surf(xs,ys, W);
colormap(brewermap([],'Blues'))

shading interp
zticks([0, 0.5, 1])

cb = colorbar
title(cb, '$\mathcal{W}_{\hat \rho}^{\mathbf m, \mathbf \Gamma}$','interpreter','latex','fontsize',15)
set(cb,'TickLabelInterpreter','latex')
%cb.Location = 'northoutside';

cb.Location = 'south';
cb.Position = [0.43 0.19, 0.1, 0.03];


cb.Ticks = round([0, max_W],2);
cb.Limits = [0, round(max_W,2)];

hold on

set(gca,'ydir','normal')



z_ellipse_contour_value_normalized = max_W * ellipse_contour_value_normalized * ones(size(W_ellipses.ellipse_angles));
plot3(W_ellipses.ellipses{idx}{:}, z_ellipse_contour_value_normalized ,'displayname', ['Wigner ellipse'])

xlim([-5,5])
ylim([-5,5])


%plot(sqrt(W.scale) * [0, W.major_axis_vectors(1, idx)] + W.centers(1,idx), sqrt(W.scale) * [0, W.major_axis_vectors(2, idx)] + W.centers(2,idx),'displayname','major axis')
%plot(sqrt(W.scale) * [0, W.minor_axis_vectors(1, idx)] + W.centers(1,idx), sqrt(W.scale) * [0, W.minor_axis_vectors(2, idx)] + W.centers(2,idx),'displayname','minor axis')



the_xlim = xlim;
the_ylim = ylim;

Px = 1 /(sqrt(2*pi * covar(1,1))) * exp(-(xs - r.ys(1,idx)).^2 / (2*covar(1,1)));
Py = 1 /(sqrt(2*pi * covar(2,2))) * exp(-(ys - r.ys(2,idx)).^2 / (2*covar(2,2)));

max_Px = (max(Px));
max_Py = (max(Py));

sum(Px)*dx
sum(Py)*dy


plot3(xs, the_ylim(2)*ones(size(ys)), Px, 'color', colors(2,:))
plot3(m(1),  the_ylim(2), 0.01, 'k.','markersize',markersize)
%plot3( 2*sqrt(2*log(2))* [-sqrt(covar(1,1))/2 +sqrt(covar(1,1))/2] + m(1),  [the_ylim(2)  the_ylim(2)], max_Px * [0.5 0.5], 'k-')
plot3( 2*sqrt(log(2) * Gamma(1,1))* [-0.5, 0.5] + m(1),  [the_ylim(2)  the_ylim(2)], max_Px * [0.5 0.5], 'k-')
text(m(1),the_ylim(2),0.05, '$m_{i}$', 'horizontalalignment','center', 'fontsize',fontsize)
text(m(1),the_ylim(2), max_Px * 0.62, '$\Gamma_{i,i}$', 'horizontalalignment','center', 'fontsize',fontsize)
text(m(1),the_ylim(2), max_Px + 0.05, '$P(y_i)$', 'horizontalalignment','center','fontsize',fontsize)


plot3(the_ylim(2)*ones(size(ys)), ys, Py, 'color', colors(2,:))
plot3(the_xlim(2), m(2), 0.01, 'k.','markersize',10)
%plot3([the_xlim(2)  the_xlim(2)],  2*sqrt(2*log(2))* [-sqrt(covar(2,2))/2 +sqrt(covar(2,2))/2] + m(2), max_Py * [0.5 0.5], 'k-')
plot3([the_xlim(2)  the_xlim(2)],  2*sqrt(log(2)) * sqrt(Gamma(2,2)) * [-0.5 +0.5] + m(2), max_Py * [0.5 0.5], 'k-')

text(the_xlim(2), m(2),0.05, '$m_{j}$', 'horizontalalignment','center', 'fontsize',fontsize)
text(the_xlim(2)-0.05, m(2), max_Py * 0.61, '$\Gamma_{j,j}$', 'horizontalalignment','center', 'fontsize',fontsize)
text(the_xlim(2), m(2), max_Py + 0.05, '$P(y_j)$', 'horizontalalignment','center', 'fontsize',fontsize)

%xlabel('$x$')
%ylabel('$p$')

xticks([-5,0 5])
yticks([-5,0 5])

xticklabels({'-5', '$y_i$', '5'})
yticklabels({'-5', '$y_j$', '5'})

%text(mean(the_xlim), 1.25*the_ylim(1), 0,'$x$', 'horizontalalignment','center')
%text(1.25*the_xlim(1), mean(the_ylim), 0,'$p$', 'horizontalalignment','center')

%plot3(the_xlim(2)*ones(size(xs)), ys, exp(-(xs - r.ys(2,idx)).^2 / (r.gs(2,2,idx))), 'color', colors(2,:))

%plot3(the_xlim(2), ys, 0.5)

zlim([0 0.7])

spacing = 30;  
for i = 1 : spacing : length(X1(:,1))
    pp = plot3(X1(i,:), X2(i,:), W(i,:),'color',[0, 0, 0, 0.1],'linewidth', 1);
    plot3(X1(:,i), X2(:,i), W(:,i),'color',[0, 0, 0, 0.1],'linewidth', 1);
end


% Create textarrow
annotation(gcf,'textarrow',[0.535 0.535],...
    [0.40 0.56],'HeadStyle','vback1','HeadLength',7);


if with_marginal
    
    measurement_color = "#cb4154";
    
    x_marginal = 1;
    y_marginal = 0;
    
    
    if y_marginal
        y_val_marginal = -3.3;
        [~, min_idx] = min(abs(ys - y_val_marginal));

        plot3(xs, y_val_marginal * ones(size(ys)), W(min_idx,:)+0.01,'color',measurement_color)
        plot3(xs, the_ylim(2)*ones(size(ys)), W(min_idx,:)  , 'color', measurement_color)
    end
    
    if x_marginal
        x_val_marginal = -2.4;
        [~, min_idx] = min(abs(xs - x_val_marginal));

        plot3(x_val_marginal * ones(size(ys)), ys,  W(:,min_idx)+0.0,'color',measurement_color)
        p_tmp = plot3(the_xlim(2)*ones(size(ys)), ys, W(:,min_idx)-0.0001  , 'color', [measurement_color])
        p_tmp.Color = [p_tmp.Color 0.4];
        
        [~, max_idx] = max(W(:,min_idx));
        
        plot3(the_xlim(2), ys(max_idx), 0.01, '.', 'markersize',markersize, 'color', measurement_color)
        
        
    end
end

ax = gca;
ax.FontSize = 15;


%% inset

ax2 = axes('Position',[0.42 0.6 0.23 0.23]);
%imagesc(xs, ys, W)
xlim([-2, 2])
%ylim([-4, 0])
ylim([-3.5, 0.5])



axis equal
hold on
p1 = plot(W_ellipses.ellipses{idx}{:}, 'color', colors(1,:));
p2 = plot(W_ellipses.centers(1, idx), W_ellipses.centers(2, idx),'k.','handlevisibility','off','markersize',10);


the_xlim2 = xlim;
the_ylim2 = ylim;

text(m(1), the_ylim2(1) + 0.4, '$y_i$','horizontalalignment','center', 'fontsize',fontsize)
text(the_xlim2(1) + 0.5, m(2), '$y_j$','horizontalalignment','center', 'fontsize',fontsize)

text(m(1)+0.17, m(2), '$\mathbf m$','horizontalalignment','left', 'fontsize',fontsize)
text(m(1)+0.17, m(2)+1.2, '$\mathbf \Gamma$','horizontalalignment','left', 'fontsize',fontsize)

%xticklabels({})
%yticklabels({})



%%

%pos = [1641 1581 1.5*col_width 250];
set(gcf, 'position', [3841 1581 483 390])

prefix = [fig_root '/' 'fig'];
suffix = ['.pdf'];
savename = @(str) [prefix str suffix];
    
    
if save_fig 
    exportgraphics(gcf,savename('2'),'BackgroundColor','none','Resolution', 750)
end