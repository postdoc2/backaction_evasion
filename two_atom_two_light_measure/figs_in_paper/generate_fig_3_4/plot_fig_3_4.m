
clear all
%close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
%datafolder_root = '/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/paper_figs/fig_2_3/data';


%data = 'rs_03-Nov-2021 14:21:45'; 

if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end


load([datafolder_root '/' data '.mat'])
stochastic_project_defaults


include = {};
include{end+1} = 'a';
include{end+1} = 'b';
include{end+1} = 'c';
include{end+1} = 'd'; 
include{end+1} = 'e';
include{end+1} = 'f';



% param_plot_opt = {};
% 
% param_plot_opt{end+1} = struct(...
%     'label', 'a', ...
%     'ts_snapshot', linspace(0, 0.5*pi/r.w, 4) ... 
%     );
% 
% param_plot_opt{end+1} = struct(...
%     'label', 'b', ...
%     'ts_snapshot', linspace(0, 10/r.k^2, 4) ... 
%     );


% cellfun(@(x) strcmp('b', x.label), param_plot_opt)


save_plots = 1;

%figure
%set(gcf, 'position', [1641 1581 244 478])
%tiledlayout(2,1)

%    ha = tight_subplot(2, 1, 0.1, 0.1, 0.1);

%axes(ha(2));



for k = 1:numel(rs)


    r = moments_from_results(rs{k});

    if ~any(cellfun(@(x) strcmp(r.p.label, x), include)) % check if label is included?
        continue;
    end

    opt = opts(r);
    ts_snapshot = opt.ts_snapshot;
    snapshot_idx = opt.snapshot_idx;

    alphaVals = [linspace(0.2, 0.5, numel(ts_snapshot)-1), 1];
    linewidthVals = linspace(0.5, 2, numel(ts_snapshot));


    %%%%%%%%%% VARIANCE %%%%%%%%%%%%
    f1=figure;
    %axes(ha(1))
    %subplot(2,1,1)

    if strcmp(opt.wigner_plot_var, 'A1')
        plot(r.ts, r.varxA1, 'color', colors(2,:), 'displayname', '$\Delta \hat x_{}^2$')
        hold on 
        plot(r.ts, r.varpA1, 'color', colors(3,:), 'displayname', '$\Delta \hat p_{}^2$', 'linewidth', 1)
    elseif strcmp(opt.wigner_plot_var, 'rel_sum')
        plot(r.ts, r.relXVar, 'color', colors(2,:), 'displayname', '$\Delta \hat x_{-}^2$')
        hold on 
        plot(r.ts, r.sumPVar, 'color', colors(3,:), 'displayname', '$\Delta \hat p_{+}^2$', 'linewidth', 1) 
    elseif strcmp(opt.wigner_plot_var, 'rel_rel')
        plot(r.ts, r.relXVar, 'color', colors(2,:), 'displayname', '$\Delta \hat x_{-}^2$')
        hold on 
        plot(r.ts, r.relPVar, 'color', colors(3,:), 'displayname', '$\Delta \hat p_{-}^2$', 'linewidth', 1) 
    end


    for i = 1:numel(ts_snapshot)
        t_loc = r.ts(snapshot_idx(i));
        if t_loc == 0
            t_loc = r.dt;
        end

        the_xline = xline(t_loc,'handlevisibility','off','color', 'k', 'linewidth',0.2);
        set(the_xline,'alpha', 0.4)
    end

    if isfield(opt, 'var_ylim')
        ylim(opt.var_ylim);
    end

    if isfield(opt, 'var_xscale')
        set(gca, 'xscale', opt.var_xscale);
    end

    set(gca,'yscale', 'log');

    if strcmp(get(gca,'xscale'), 'linear')
       xlim(xlim + 0.2 * ms *[-1, 0]) 
       xticks([0, round(r.T/2/ms,1)*ms, r. T])
    else
       xlim(xlim .* [8e-1, 1] )
       xticks([10^(-6), 10^(-4), 10^(-2)])
    end


    leg = legend;
    leg.Location = 'northoutside';
    leg.Orientation = 'horizontal';
    leg.Box = 'off';
    leg.ItemTokenSize = [18,18];
    xlabel('$t$ (s)')

    annotation('textbox', [.15 .47 .3 .3], 'string',[ opt.label ],'interpreter','latex','edgecolor','none')        

    annotation('textbox', [.4 .47 .3 .3], 'string', opt.variables_label ,'interpreter','latex','edgecolor','none')        
    %annotation('textbox', [.56 .42 .3 .33], 'string', opt.variables_label ,'interpreter','latex','edgecolor','k','backgroundcolor','w','linewidth',0.5,'facealpha', 0.8)        

    ylim([1e-3, 1e3])
    yticks([1e-2, 0.5, 1e2])
    yticklabels({'$10^{-2}$', '$0.5$', '$10^{2}$'})



    %%%%%%%%%% WIGNER ELLIPSE %%%%%%%%%%%%
    f2=figure;
    %subplot(2,1,2)
    %axes(ha(2));
    ellipse_contour_value_normalized = 0.5;



    W = Wigner_ellipses(r, opt.wigner_plot_var, ellipse_contour_value_normalized, 3000);

    for i = 1:numel(ts_snapshot)
        idx = snapshot_idx(i);           

        p1 = plot(W.ellipses{idx}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$', 'linewidth', linewidthVals(i));
        p1.Color = [p1.Color alphaVals(i)];
        hold on

        %p2 = plot(W.centers(1, idx), W.centers(2, idx), 'k.','markersize',10, 'handlevisibility','off');
        p2 = scatter(W.centers(1, idx), W.centers(2, idx),'k.','handlevisibility','off','MarkerFaceAlpha', alphaVals(i), 'MarkerEdgeAlpha',  alphaVals(i));
        %p2.Color = [p2.Color alphaVals(i)];

        axis equal
    end
    p1.HandleVisibility = 'on';
    p = plot(W.centers(1, 1 : snapshot_idx(end)), W.centers(2, 1 : snapshot_idx(end)),'k-','linewidth', 0.5, 'handlevisibility','off');
    p.Color = [p.Color 0.2];
    p=plot(nan, 'k.-', 'color',p.Color, 'displayname', '$\mathbf{m}$', 'linewidth',p.LineWidth);
    p.Color = [p.Color 0.2];


    leg = legend;
    leg.Location = 'northoutside';
    leg.Orientation = 'horizontal';
    leg.Box = 'off';
    leg.ItemTokenSize = [18,18];

    %set(gca,'XAxisLocation','top')
    set(gca,'YAxisLocation','right')

    if strcmp(opt.wigner_plot_var, 'A1')
        xlabel('$x$')
        ylabel('$p$')
    else
        xlabel('$x_{-}$')
        ylabel('$p_{+}$')
    end


    cst = 0.2;
    xlim(xlim + cst * [-1 1])
    ylim(ylim + cst * [-1 1])
    %ylim([-max(abs(ylim)), max(abs(ylim))])
    % 
    pos = [1641 1581 col_width/2.1 120];
    set(f1, 'position', pos)
    set(f2, 'position', pos)

    txt_font_size = 8;
    set( findall(f1, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f2, '-property', 'fontsize'), 'fontsize', txt_font_size);


    txt_obj = findall(f1,'Type','text');

    %xlim([-max(abs(xlim)), max(abs(xlim))])
    %ylim([-max(abs(ylim)), max(abs(ylim))])

    prefix = [fig_root '/' opt.savename '_'];
    suffix = ['.pdf'];
    savename = @(str) [prefix str suffix];


    if save_plots 
        exportgraphics(f1,savename('var'),'BackgroundColor','none')
        exportgraphics(f2,savename('wig'),'BackgroundColor','none')
    end



end



%% plot options

function opt = opts(r)

    opt = {};
    
    constants
    
    switch r.p.label
        case 'a'
            opt.ts_snapshot =  linspace(0, 0.5*pi/r.w, 4);
            opt.wigner_plot_var =  'A1';
            %opt.var_xscale = 'linear';
            opt.var_xscale = 'linear';
            opt.label = '(a)';
            opt.savename = 'fig3_a';
            
            opt.variables_label = {...
                ['$\omega = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
%                 ['$\kappa_1 = ' num2str(r.k11/r.k) '$']...
%                 ['$\kappa_2 = ' num2str(r.k12/r.k) '$']...
                };


        case 'b'
            %opt.ts_snapshot =  linspace(0, 10/r.k^2, 4);
            opt.ts_snapshot =  logspace(-6, -3, 4);
            opt.wigner_plot_var =  'A1';
            opt.var_xscale = 'log';
            opt.label = '(b)';
            opt.savename = 'fig3_b';

            opt.variables_label = {...
%                 ['$\omega = ' num2str(r.w1/w_01ms) '$']... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_1 = ' num2str(r.k11/r.k) '$']...
%                 ['$\kappa_2 = ' num2str(r.k12/r.k) '$']...
                };


        case 'c'
            %opt.ts_snapshot =  linspace(0, 10/r.k^2, 4);
            %opt.ts_snapshot =  [linspace(0, 5/r.k^2, 3), linspace(0.2*pi/r.w, 0.5*pi/r.w, 2)];    
            opt.ts_snapshot =  logspace(-6,-2.8,5);
%              opt.ts_snapshot =  linspace(0, 0.5*pi/r.w, 4);
                
            opt.wigner_plot_var =  'A1';
            opt.var_xscale = 'log';
            opt.label = '(c)';
            opt.savename = 'fig3_c';
            
            opt.variables_label = {...
                ['$\omega = ' num2str(r.w1/w_01ms) '$']... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_1 = ' num2str(r.k11/r.k) '$']...
%                 ['$\kappa_2 = ' num2str(r.k12/r.k) '$']...
                };


        case 'd'
            opt.ts_snapshot =  linspace(0, 0.5*pi/r.w, 4);
            opt.wigner_plot_var =  'A1';
            opt.label = '(d)';
            opt.savename = 'fig3_d';
            
            opt.variables_label = {...
                ['$\omega = ' num2str(r.w1/w_01ms) '$']... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_1 = ' num2str(r.k11/r.k) '$']...
                ['$\kappa_2 = ' num2str(r.k12/r.k) '$']...
                };

            
            opt.var_ylim = [-0.04, 0.04] + 0.5;
            opt.var_xscale = 'log';
            

        case 'e'
            opt.ts_snapshot =  [1e-5, linspace(0, 0.3*pi/r.w, 4)];
            %opt.ts_snapshot =  logspace(-6, -2.8, 4);
            opt.wigner_plot_var =  'rel_sum';
            opt.var_xscale = 'log';
            opt.label = '(a)';
            opt.savename = 'fig4_a';
            
            opt.variables_label = {...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };            

        case 'f'
            %opt.ts_snapshot =  linspace(0, 0.3*pi/r.w, 4);
            opt.ts_snapshot =  [1e-5, 1e-4, linspace(0, 0.4*pi/r.w, 4)];
            %opt.ts_snapshot = logspace(-6, -2.5,4);
            %opt.wigner_plot_var =  'rel_rel';
            opt.wigner_plot_var =  'rel_sum';
            opt.var_xscale = 'log';
            opt.label = '(b)';
            opt.savename = 'fig4_b';
            
            opt.variables_label = {...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };            
            

        otherwise
            error('no label')
    end
            
    opt.ts_snapshot = sort(unique(opt.ts_snapshot));
    [~, snapshot_idx] = arrayfun(@(t) min(abs(r.time.ts - t)), opt.ts_snapshot); % find indices that coincides with desired snap shots
    opt.snapshot_idx = snapshot_idx;
    
end

