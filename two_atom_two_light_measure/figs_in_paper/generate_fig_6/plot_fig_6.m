clear all
close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
%datafolder_root = '/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/paper_figs/fig_2_3/data';


%data = 'rs_03-Nov-2021 14:21:45'; 
data = 'OU_initial_steadystate';
%data = 'T=25';
%data = 'T=100';


if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end


load([datafolder_root '/' data '.mat'])
stochastic_project_defaults

include = {};

% no omega
include{end+1} = '1 oscillator 1 p laser without omega';
include{end+1} = '1 oscillator 2 xp laser without omega';
include{end+1} = '2 oscillator 2 xp laser without omega';

% omega
include{end+1} = '1 oscillator 1 p laser with omega';
include{end+1} = '2 oscillator 2 xp laser with omega wrong sign';
include{end+1} = '2 oscillator 2 xp laser with omega';


fx_linestyle = '-';
fp_linestyle = '-';

%fx_color = [colors(1,:), 1];
%fp_color = [colors(1,:)  1];

color_gold = [255, 215, 0] / 255;

with_shading = 1;
save_plots = 0;

%figure
%set(gcf, 'position', [1641 1581 244 478])
%tiledlayout(2,1)

%    ha = tight_subplot(2, 1, 0.1, 0.1, 0.1);

%axes(ha(2));

%f1=figure;
%ax1 = axes;
%f2=figure;
%ax2 = axes;
tic
for k = 1:numel(rs)


    r = moments_from_results(rs{k});

    r.gammabx
    r.sigmabx
    
    r.gammabp
    r.sigmabp

    if ~any(cellfun(@(x) strcmp(r.p.label, x), include)) % check if label is included?
        continue;
    end

    [opt, r, r_orig] = opts(r);

    f1=figure;
    f2=figure;
    
    pos = [1641 1581 col_width/2.1 120];
    set(f1, 'position', pos)
    set(f2, 'position', pos + [col_width/2 0 0 0])

    %%%%%%%%%% VARIANCE %%%%%%%%%%%%
    
    figure(f1);
    
    ax1 = axes;
    if opt.var_plot
        loglog(ax1, r.ts, r.varFp, fp_linestyle, 'color', colors(3,:), 'displayname', '$\Delta f_p^2$');
        hold on
        loglog(ax1, r.ts, r.varFx, fx_linestyle, 'color', colors(2,:), 'displayname', '$\Delta f_x^2$');        
        
        %loglog(ax1, r.ts, r.varFp_PQS, ':', 'color', colors(3,:), 'displayname', 'PQS');
        %loglog(ax1, r.ts, r.varFx_PQS, ':', 'color', colors(2,:), 'displayname', 'PQS');
        
    end
    
    %xlim(xlim .* [4e-1, 0.5e1] )
    xlim([0.5e-6, 2e-1])
    ylim([1e-6, 1e1])
    
    if isfield(opt, 'powerline')
        for i = 1:numel(opt.powerline)
            %plot(r.ts, opt.powerline{i}.x, 'color', [0 0 0, 0.3],'linewidth',0.35, 'handlevisibility','off')
            plot([opt.powerline{i}.x(2), opt.powerline{i}.x(end)], [opt.powerline{i}.y(2), opt.powerline{i}.y(end)], 'color', [0 0 0, 0.4],'linewidth',0.3, 'handlevisibility','off')
        end
    end
    
    xlabel('$t$ (s)')

    leg = legend;
    %leg.NumColumns = 2;
    leg.Location = 'northoutside';
    leg.ItemTokenSize = [18,18];

    
    %annotation('textbox', [.15 .42 .3 .3], 'string',[ opt.label ],'interpreter','latex','edgecolor','none')   
    annotation('textbox', [.7 .42 .3 .3], 'string',[ opt.label ],'interpreter','latex','edgecolor','none')   
    
    annotation('textbox', [.2 .22 .3 .3], 'string', opt.variables_label, 'interpreter','latex','edgecolor','none')        

    
    
    set([ax1, ], 'linewidth',0.5, 'ticklength', [0.01 0.025])
    set([ax1, ], 'ticklength', [0.02 0.025])
    
    xticks([1e-6, 1e-4, 1e-2,])
    %yticks([1e-6, 1e-4, 1e-2, 1e0])
    yticks([1e-5, 1e-3, 1e-1, ])
    
    %%%%%%%%%% MEAN %%%%%%%%%%%%
    
    figure(f2);
    ax2 = axes;
    
    %xstart= 0.25; xend= 0.55;
    %ystart= 0.3;  yend= 0.6;
    %ax2 = axes('position',[xstart ystart xend-xstart yend-ystart ]);
    
    
    if opt.mean_plot
        
        hold on

        p_fp_mean_est = plot(ax2, r.ts, r.m6, fp_linestyle, 'color', colors(3,:), 'displayname', '$\langle f_p \rangle$', 'linewidth', 0.5);
        p_fx_mean_est = plot(ax2, r.ts, r.m5, fx_linestyle, 'color', colors(2,:), 'displayname', '$\langle f_x \rangle$', 'linewidth', 0.5); 
        
        if with_shading 
            legend('AutoUpdate','off')

            [~, unique_idxs] = unique(r.ts);
            x_shade  = r.ts(unique_idxs);

            y1_shade = r.m6(unique_idxs) - sqrt(r.varFp(unique_idxs));
            y2_shade = r.m6(unique_idxs) + sqrt(r.varFp(unique_idxs));
            shade(  x_shade, y1_shade, '-w', ...
                    x_shade, y2_shade, '-w', 'linestyle', 'none',...
                    'FillAlpha', 0.3, 'FillColor', colors(3,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

            y1_shade = r.m5(unique_idxs) - sqrt(r.varFx(unique_idxs));
            y2_shade = r.m5(unique_idxs) + sqrt(r.varFx(unique_idxs));
            shade(  x_shade, y1_shade, '-w', ...
                    x_shade, y2_shade, '-w', 'linestyle', 'none',...
                    'FillAlpha', 0.3, 'FillColor', colors(2,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

            legend('AutoUpdate','on')
        end
        
        p_fp_mean = plot(ax2, r.ts, r.OUp.X, '-', 'color',   colors(6,:), 'displayname', '$f_p$', 'linewidth', 0.5);
        p_fx_mean = plot(ax2, r.ts, r.OUx.X, '-' , 'color',  colors(1,:), 'displayname', '$f_x$', 'linewidth', 0.5); 
        
        
        if isfield(opt, 'stationary_var_x')
            yline(+0.5*sqrt(opt.stationary_var_x), 'linewidth', 0.3, 'handlevisibility','off', 'alpha', 0.3)
            yline(-0.5*sqrt(opt.stationary_var_x), 'linewidth', 0.3, 'handlevisibility','off', 'alpha', 0.3)
        end
        
        
        %p_fp_mean_est = plot(ax2, r.ts, r.mFp_PQS, ':', 'color', colors(3,:), 'displayname', 'PQS', 'linewidth', 2);
        %p_fx_mean_est = plot(ax2, r.ts, r.mFx_PQS, ':', 'color', colors(2,:), 'displayname', 'PQS', 'linewidth', 2); 
        
        
        if isfield(opt, 'mean_xscale')
            set(ax2, 'xscale', opt.mean_xscale);
        end    


        if strcmp(get(ax2,'xscale'), 'linear')
           %xlim(xlim + 2 * ms *[-1, 1]) 
           xlim(xlim + 5 * ms *[-1, 1]) 
           %xticks([0, round(r.T/2/ms,1)*ms, r. T])
        else
           xlim(xlim .* [8e-1, 1.2] )
           %xticks([10^(-6), 10^(-4), 10^(-2)])
        end
    end
    
    xlabel('$t$ (s)')
    
    
    %txt_x = strrep(split(sprintf('$e_x=%.1e$', tracking_error(r.OUx, r.m5))), '0', '');
    txt_x = split(sprintf('%.1e', r.tracking_error_x),'e');
    txt_x{2} = strrep(txt_x{2},'0','');
    txt_x = ['$e_x = ' txt_x{1} ' \times 10^{' txt_x{2} '}$'];
    
    txt_p = split(sprintf('%.1e', r.tracking_error_p),'e');
    txt_p{2} = strrep(txt_p{2},'0','');
    txt_p = ['$e_p = ' txt_p{1} ' \times 10^{' txt_p{2} '}$'];
    
    %annotation('textbox', [.28 .38 .3 .3], 'string', sprintf('$e_x = %.1e$\n$e_p$ = %.1e',  tracking_error(r.OUx, r.m5), tracking_error(r.OUp, r.m6)), 'interpreter','latex','edgecolor','none')
    %annotation('textbox', [.35 .42 .3 .3], 'string', [txt_x sprintf('\n') txt_p], 'interpreter','latex','edgecolor','none')
    
    if isfield(opt, 'OU_variables_labels')
        OUtxt1 = annotation('textbox', [.28 .16 .3 .3], 'string', opt.OU_variables_labels{1}, 'interpreter','latex','edgecolor','none');
        annotation('textbox', OUtxt1.Position + [0.28 0 0 0], 'string', opt.OU_variables_labels{2}, 'interpreter','latex','edgecolor','none')
    end
    
    if isfield(opt, 'gold_frame')
        %gold_frame_1 = annotation(f1,'textbox',[0.15 0.18 0.75 0.525], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1.5);
        %gold_frame_2 = annotation(f2,'textbox',[0.135 0.18 0.76 0.525], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1.5);
        
        gold_frame_1 = annotation(f1,'textbox',[0.14  0.17 0.77  0.543], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1);
        gold_frame_2 = annotation(f2,'textbox',[0.125 0.17 0.785 0.543], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1);
    end

    
    leg = legend;
    leg.Location = 'northoutside';
    leg.ItemTokenSize = [18,18];
    leg.NumColumns = 2;
    
    ylim([-1.4, 1.4])
    yticks([-1, 0, 1])
    %
    %figure
    %W = Wigner_ellipses(r_orig, 'A1', 0.5, 100);
    %plot(W.major_axis_tilt_angles)
    %figure
    %plot((W.ellipses{end}{:}))
    
    
    %
    
    txt_font_size = 8;
    set( findall(f1, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f2, '-property', 'fontsize'), 'fontsize', txt_font_size);


    txt_obj = findall(f1,'Type','text');

    %lines = findobj(f2,'Type','Line');
    %for i = 1:numel(lines)
    %   lines(i).LineWidth = 0.5;
    %end
    
    %xlim([-max(abs(xlim)), max(abs(xlim))])
    %ylim([-max(abs(ylim)), max(abs(ylim))])

    prefix = [fig_root '/' opt.savename '_'];
    suffix = ['.pdf'];
    savename = @(str) [prefix str suffix];


    if save_plots 
        exportgraphics(f1,savename('var'), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f2,savename('mean'),'BackgroundColor','none','ContentType','vector')
    end


end
toc


%% plot options

function [opt, r, r_orig] = opts(r)

    opt = {};
    
    constants
    
    opt.sqrt_var = 0; 
    opt.mean_xscale = 'linear';
    opt.var_xscale = 'log';
    
    
    switch r.p.label
        case '1 oscillator 1 p laser without omega'
           opt.savename='fig6_a';
           %opt.mean_xscale = 'linear';
           opt.label = '(a)';
           
           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  1e-12 ./ (opt.powerline{1}.x).^3;   
           opt.powerline{1}.label =  '$1/t^3$';   
           
           opt.mean_plot = 1;
           opt.var_plot = 1;

           %opt.OU_variables_labels{1} = {['$\gamma_x = ' num2str(r.gammabx) '$'], ['$\gamma_p = ' num2str(r.sigmabx) '$']};   
           %opt.OU_variables_labels{2} = {['$\sigma_x = ' num2str(r.gammabx) '$'], ['$\sigma_p = ' num2str(r.sigmabx) '$']};
           
           opt.variables_label = {...
%                 ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
%                 ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                 ['$\kappa_{1} = ' num2str(r.k11/r.k) '$'] ...
%                 ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };
            
           opt.stationary_var_x =  2*r.sigmabx/(r.gammabx);
            
            
            
        case '1 oscillator 2 xp laser without omega'
           opt.savename='fig6_b';
           %opt.mean_xscale = 'linear';
           opt.label = '(b)';

           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  0.5e-4 ./ (opt.powerline{1}.x).^1 ;  
           opt.powerline{1}.label =  '$1/t$';   
           
           
           opt.mean_plot = 1;
           opt.var_plot  = 1;
           
           
           
           opt.variables_label = {...
%                 ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
%                 ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{1} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{2} = ' num2str(r.k12/r.k) '$']
                };
           
            
            
        case '2 oscillator 2 xp laser without omega'
           opt.savename='fig6_c';
           opt.label = '(c)';
           %opt.mean_xscale = 'linear';

           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  1e-12 ./ (opt.powerline{1}.x).^3;   
           opt.powerline{1}.label =  '$1/t^3$';   
           
           opt.mean_plot = 1;
           opt.var_plot  = 1;
           
       %    opt.gold_frame = 1;
           
           opt.variables_label = {...
%                 ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
%                 ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };
           
        case '1 oscillator 1 p laser with omega'
           opt.savename='fig6_e_UNUSED';
           opt.label = '(a)';
           %opt.mean_xscale = 'linear';

           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  0.5e-4 ./ (opt.powerline{1}.x).^1 ;
           opt.powerline{1}.label =  '$1/t$';   
           
           opt.mean_plot = 1;
           opt.var_plot = 1;
           
           %opt.OU_variables_labels{1} = {['$c_x = ' num2str(r.cx / 1.5e4)   '$'], ['$\gamma_x = ' num2str(r.gammabx) '$'], ['$\gamma_p = ' num2str(r.sigmabx) '$']};   
           %opt.OU_variables_labels{2} = {['$c_p = ' num2str(r.cp / 1.5e4)   '$'], ['$\sigma_x = ' num2str(r.gammabx) '$'], ['$\sigma_p = ' num2str(r.sigmabx) '$']};
           
           
           
           opt.variables_label = {...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
%                 ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{1} = ' num2str(r.k11/r.k) '$'] ...
%                 ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };
        case '2 oscillator 2 xp laser with omega wrong sign'
           opt.savename='fig6_f_UNUSED';
           opt.label = '(b)';
           %opt.mean_xscale = 'linear';

           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  0.5e-4 ./ (opt.powerline{1}.x).^1 ;   
           opt.powerline{1}.label =  '$1/t$';   
           
           opt.mean_plot = 1;
           opt.var_plot = 1;
           
           opt.variables_label = {...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };
           
            
        case '2 oscillator 2 xp laser with omega'
           opt.savename='fig6_d';
           opt.label = '(d)';
           %opt.mean_xscale = 'linear';

           opt.powerline{1}.x =  r.ts;   
           opt.powerline{1}.y =  1e-12 ./ (opt.powerline{1}.x).^3 ;   
           opt.powerline{1}.label =  '$1/t^3$';   
           
        %   opt.gold_frame = 1;
%            opt.powerline{2}.x = [1e-5 1e-4 1e-1];
%            opt.powerline{2}.y =  0.5e-7 ./ (opt.powerline{2}.x).^1 ;   
%            opt.powerline{2}.label =  '$1/t$';   
           
           opt.mean_plot = 1;
           opt.var_plot = 1;
           
            opt.variables_label = {...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$'] ...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] 
                };                
                        
           
            
            
            
        otherwise
            error('no label')
    end
       
            
    r.tracking_error_x = tracking_error(r.OUx, r.m5);
    r.tracking_error_p = tracking_error(r.OUp, r.m6);
    
    r_orig = r;
    
    %ts_plot_interval = 100;    
    %ts_reduced = r.ts([1, 2 : ts_plot_interval : r.nt]);
    
    % current
    %dt_plot_target = 1e-5;
    %ts_reduced = [0, r.dt, dt_plot_target : dt_plot_target : r.T]; 
    
    
    dt_plot_target1 = 1e-5;
    dt_plot_target2 = 1e-4;
    
    ts_reduced = [[0, r.dt, r.dt:dt_plot_target1 : 1*ms], [1*ms : dt_plot_target2 : r.T]]; 
    
    
    %ts_reduced = [0, logspace(log10(r.ts(2)), log10(r.T), 1000)];
    
    r = reduce_time_resolution(r, ts_reduced);
    
%     ts_plot_interval = 10;    
%     nt = numel(r.ts); 
%     fn = fieldnames(r);
%     for k=1:numel(fn)
%         field = r.(fn{k});
%         if isnumeric(field)
%             field_dimensions = size(field, 2);
%             
%             if field_dimensions == nt % is this time dependent?
%                 r.(fn{k}) = field([1:2, 2:ts_plot_interval:end]);
%             end 
%         end
%     end
%     r.OUx.X = r.OUx.X([1:2, 2:ts_plot_interval:end]);
%     r.OUp.X = r.OUp.X([1:2, 2:ts_plot_interval:end]);
    
            
    
    
end

