%rng('default');

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)
% Forces: acting on indep. on XA1 and PA1
% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H/hbar = +w1/2(XA1^2 + PA1^2) + w2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

%%%%%%% TIME %%%%%%%%

T = 20*ms;
%T = 20*ms;
%dt = 1.0e-6;
dt = 1e-5;


w = 0;
k =  0.1*sqrt(1.83e6); 


nws = 10;
ncs = 10;
nks = 10;

%ws = linspace(0, 3 * w_1ms, nws);
%cs = linspace(1e3, 1.5e5, ncs);
%ks = linspace(0.1*sqrt(1.83e6), sqrt(1.83e6), nks);

ws = 5 * w_1ms * [0, logspace(-1, 0, nws-1)];
cs = 1.5e5 * logspace(-2, 0, ncs);
ks = sqrt(1.83e6) * logspace(-1.5, 0, nks);

params = {};


%%%%%%% FORCE %%%%%%%%

% F = c * f

fx0  = 1;
cx  = 1.5e5;
gammabx = 0e2*s^(-1);
sigmabx = 0*1e1*s^(-1);

fp0  = -fx0;
cp  = cx;
gammabp = 0e2*s^(-1);
sigmabp = 0*1e1*s^(-1);


m0 = [0; 0; 0; 0; 0; 0];
A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

%%%%%%%%%%%%



%%%%%%% RNG %%%%%%%%

OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

E_covar_initial_val = 10000;
with_PQS = 0;


k =  0.1*sqrt(1.83e6); 
rs_vary_cs = {};
for j = 1:numel(cs)
    time = Time('ts', 0:dt:T);  

    OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
    OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

 %   if isfield(p, 'w1')         

    r = moments_from_results(propagate_with_F(A0, m0, k, k, k, -k, w, w, cs(j), OUx, cs(j), OUp, measurement_seed_num,'approx_mpinv', 'off')); 

    if with_PQS
        r = PQS(r, 'E_covar_initial_val', E_covar_initial_val);
    end

    rs_vary_cs{j} = r;
end


rs_vary_ks = {};
for j = 1:numel(ks)
    time = Time('ts', 0:dt:T);  

    OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
    OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

 %   if isfield(p, 'w1')         

    r = moments_from_results(propagate_with_F(A0, m0, ks(j), ks(j), ks(j), -ks(j), 0, 0, cx, OUx, cp, OUp, measurement_seed_num,'approx_mpinv', 'off')); 

    if with_PQS
        r = PQS(r, 'E_covar_initial_val', E_covar_initial_val);
    end

    rs_vary_ks{j} = r;
end



rs_vary_ws = {};
for j = 1:numel(ws)
    time = Time('ts', 0:dt:T);  

    OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
    OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

 %   if isfield(p, 'w1')         

    w = ws(j);

    r = moments_from_results(propagate_with_F(A0, m0, k, k, k, -k, w, -w, cx, OUx, cp, OUp, measurement_seed_num,'approx_mpinv', 'off')); 

    r.w = w;
    r.k = k;

    if with_PQS
        r = PQS(r, 'E_covar_initial_val', E_covar_initial_val);
    end

    rs_vary_ws{j} = r;
end


d = sqrt(eps);
dw = d;
dk = d;
dc = d;

r0     = moments_from_results(propagate_with_F(A0, m0, k, k, k, -k, w, -w, cx, OUx, cp, OUp, measurement_seed_num,'approx_mpinv', 'off')); 
reps = moments_from_results(propagate_with_F(A0, m0, k + dk, k + dk, k + dk, -(k + dk), (w + dw), -(w + dw), (cx+dc), OUx, (cp + dc), OUp, measurement_seed_num,'approx_mpinv', 'off')); 



r0.w = w ;
r0.k1 = k ;
r0.cp = cp ;
reps.w = w + dw;
reps.k1 = k + dk;
reps.cp = cp + dc;
reps.d = d;


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
savename = [datafolder_root '/rs_' char(datetime) '.mat'];
save(savename, 'rs_vary_cs', 'rs_vary_ws','rs_vary_ks', 'ws', 'cs','ks','reps','r0')

return

%% 

        filter = 2000:r.nt;
        ts_filtered = ts(filter);
        varFx_filtered = varFx(filter);

        eq = 'a/x';

    %    figure
        x = ts_filtered' ;
        y = varFx_filtered';
        f = fit(x,y, eq);
        as(end+1) = f.a

ts = r.ts;
varFx = r.varFx;


ts_filtered = ts(filter);
varFx_filtered = varFx(filter);

%figure
%loglog(ts_filtered, varFx_filtered)
%hold on


figure
x = ts_filtered' ;
y = varFx_filtered';

eq = 'a/x';

f = fit(x,y, eq)
%figure
loglog(ts_filtered, f(ts_filtered))
hold on
loglog(ts_filtered, y,'--')


%cftool(ts_filtered, varFx_filtered)