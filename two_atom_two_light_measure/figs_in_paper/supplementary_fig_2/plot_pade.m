clear all
close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
%datafolder_root = '/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/paper_figs/fig_2_3/data';



%data = '';


if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end


load([datafolder_root '/' data '.mat'])
stochastic_project_defaults


%set(0, 'DefaultAxesFontSize',20)
%set(0, 'DefaultLegendFontSize',20)


save_plots = 1;
lw = 2;
%set(0, 'DefaultAxesLineWidth', 1.0)
%% Find cp, ks dependence in denominator at t-> inf and w=0

min_T = 0.5*1e-2;
filter = find(rs_vary_cs{1}.ts > min_T); % long time limit


% FIND C DEPENDENCE
cs_fit_coeff = [];
figure
subplot(2,3,1)

varFp_color = colors(2,:);
fit_color = colors(1,:);
for i = 1:numel(cs)
    
    r = rs_vary_cs{i};
    
    
    if i == 1
        show = 'on';
    else
        show = 'off';
    end
    
    loglog(r.ts, r.varFp, 'color', varFp_color ,'displayname','$\Delta f_x^2$','handlevisibility',show,'linewidth', lw)
    hold on

    x = r.ts(filter);
    y = r.varFp(filter);
    %loglog(x,y,'k','displayname','$\Delta f_x^2(t\rightarrow \infty)$','handlevisibility',show)

    f = fit(x', y' , 'a/(x^3) ' );
    loglog(x, f(x),':','color', fit_color,'displayname','$a/t^3$ fit','handlevisibility',show,'linewidth', lw)
    
    cs_fit_coeff(i) = f.a;
end
%leg = legend;
xlabel('$t$ (s)')
%title('vary $c_p$')
title('(a)')

%figure
subplot(2,3,4)
plot(cs(1:end-1), diff(1./cs_fit_coeff)./diff(cs),'.-','markersize',12,'displayname', '$\frac{\Delta b}{\Delta c_p}$','linewidth',1)
xlabel('$c_p$')
leg = legend;
leg.Location = 'northwest';
leg.Box = 'off';
leg.FontSize = 13;


% FIND K DEPENDENCE
%figure
ks_fit_coeff = [];
subplot(2,3,2)
for i = 1:numel(ks)
    
    if i == 1
        show = 'on';
    else
        show = 'off';
    end
    
    r = rs_vary_ks{i};
    loglog(r.ts, r.varFp, 'color', varFp_color,'displayname','$\Delta f_p^2$','handlevisibility',show,'linewidth', lw)
    hold on

    x = r.ts(filter);
    y = r.varFp(filter);
    %loglog(x,y,'k','displayname','$\Delta f_x^2(t\rightarrow \infty)$','handlevisibility',show)

    f = fit(x', y' , 'a/(x^3) ');
    
    loglog(x, f(x),':','color', fit_color,'displayname','fit','handlevisibility',show,'linewidth', lw)
    
    ks_fit_coeff(i) = f.a;
end
leg = legend;
leg.Location = 'Southwest';
%leg.Location = 'northoutside';
leg.Box = 'off';
leg.FontSize = 13;
xlabel('$t$ (s)')


title('(b)')
%title('vary $\kappa$')

subplot(2,3,5)
%figure
plot(ks(1:end-1), diff(1./ks_fit_coeff)./diff(ks),'.-','markersize',12,'displayname', '$\frac{\Delta b}{\Delta \kappa}$','linewidth',1)
xlabel('$\kappa$')
leg = legend;
leg.Location = 'northwest';
leg.Box = 'off';
leg.FontSize = 13;


% FIND W DEPENDENCE

%figure
subplot(2,3,3)
ws_fit_coeff = []; 
for i = 1:numel(ws)
    
    if i == 2
        show = 'on';
    else
        show = 'off';
    end
    
    r = rs_vary_ws{i};
    loglog(r.ts, r.varFp, 'color', varFp_color,'displayname','$\Delta f_p^2$','handlevisibility',show,'linewidth', lw)
    hold on

    x = r.ts(filter);
    y = r.varFp(filter);
    %loglog(x,y,'k','displayname','$\Delta f_x^2(t\rightarrow \infty)$','handlevisibility',show)

    if r.w ~= 0
        f = fit(x', y' , 'a/(x) ');
        loglog(x, f(x),':','color', fit_color, 'displayname','fit','handlevisibility',show,'linewidth', lw)
        ws_fit_coeff(end+1) = f.a;
    end
    
    r.varFp(end) / (r.w^2 / (r.cp^2 * r.k1^2 * r.ts(end)) )
end
%leg = legend;
xlabel('$t$ (s)')
%title('vary $\omega$')
title('(c)')

%figure
subplot(2,3,6)
plot(ws(2:end-1), diff(ws_fit_coeff)./diff(ws(2:end)),'.-','markersize',12,'displayname', '$\frac{\Delta b}{\Delta \omega}$','linewidth',1)
xlabel('$\omega$')
leg = legend;
leg.Location = 'northwest';
leg.Box = 'off';
leg.FontSize = 13;


varFp_asymp         = @(w, k, c, t) w^2/(2 * k^2 * c^2 * t);
varFp_asymp_deriv_w = @(w, k, c, t)  w/(k^2 * c^2 * t);
varFp_asymp_deriv_k = @(w, k, c, t) -w^2/(k^3 * c^2 * t);
varFp_asymp_deriv_c = @(w, k, c, t) -w^2/(k^2 * c^3 * t);



% compare total derivative d(varFx)/deps = d(varFx)/ dw + d(varFx)/ dk + d(varFx)/ dc, where e.g. w = w0 + eps
LHS = (varFp_asymp(reps.w, reps.k1, reps.cp, r.ts(end)) - varFp_asymp(r0.w, r0.k1, r0.cp, r.ts(end))) / reps.d
RHS = (varFp_asymp_deriv_w(r0.w, r0.k1, r0.cp, r.ts(end)) + varFp_asymp_deriv_k(r0.w, r0.k1, r0.cp, r.ts(end)) + varFp_asymp_deriv_c(r0.w, r0.k1, r0.cp, r.ts(end)))



pause(0.01)
autoArrangeFigures(2,4,2)
%plot(x, f(x)' .* x.^3)

set(gcf,'position', [3871, 1688, 2 * col_width, 300])


prefix = [fig_root '/supplementary_'];
suffix = ['.pdf'];
savename = @(str) [prefix str suffix];



if save_plots 
    exportgraphics(gcf,savename('fig3'), 'BackgroundColor','none','resolution',600)
end
    
return

figure
semilogy(cs, cs_fit_coeff,'.')

