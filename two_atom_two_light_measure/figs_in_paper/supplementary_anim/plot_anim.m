clear all
close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
%datafolder_root = '/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/paper_figs/fig_2_3/data';


%data = 'rs_03-Nov-2021 14:21:45'; 

data = 'T=25';
%data = 'T=25_centered';
%data = 'T=50';


if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end


load([datafolder_root '/' data '.mat'])
stochastic_project_defaults

include = {};

include{end+1} = '2 oscillator 2 xp laser with omega PQS';


fx_linestyle = '-';
fp_linestyle = '-';

%fx_color = [colors(1,:), 1];
%fp_color = [colors(1,:)  1];

color_gold = [255, 215, 0] / 255;

with_shading = 0;
save_plots = 0;

%figure
%set(gcf, 'position', [1641 1581 244 478])
%tiledlayout(2,1)

%    ha = tight_subplot(2, 1, 0.1, 0.1, 0.1);

%axes(ha(2));

%f1=figure;
%ax1 = axes;
%f2=figure;
%ax2 = axes;
tic
for k = 1:numel(rs)


    r = moments_from_results(rs{k});


    if ~any(cellfun(@(x) strcmp(r.p.label, x), include)) % check if label is included?
        continue;
    end

    
    [opt, r, r_orig] = opts(r);

    if 0
        f1=figure;
        %f2=figure; 
        %f3=figure; 


        %pos = [1641 1581 col_width 120];
        pos = [1641 1290 col_width 449];
        set(f1, 'position', pos)
        %set(f2, 'position', pos +   [col_width 0 0 70])
        %set(f3, 'position', pos + 2*[col_width 0 0 0])

        %%%%%%%%%% VARIANCE %%%%%%%%%%%%

        figure(f1);

        %ax1 = axes;
        ax1 = subplot(5,1,1:2);

        if opt.var_plot
            loglog(ax1, r.ts, r.varFp, ':', 'color', colors(3,:), 'displayname', '$\Delta f_p^2$');
            hold on
            loglog(ax1, r.ts, r.varFx, ':', 'color', colors(2,:), 'displayname', '$\Delta f_x^2$');        

            loglog(ax1, r.ts, r.varFp_PQS, fp_linestyle, 'color', colors(3,:), 'displayname', '$\Delta f_p^2$ PQS');
            loglog(ax1, r.ts, r.varFx_PQS, fx_linestyle', 'color', colors(2,:), 'displayname', '$\Delta f_x^2$ PQS');        

            %loglog(ax1, r.ts, r.varFp_PQS, ':', 'color', colors(3,:), 'displayname', 'PQS');
            %loglog(ax1, r.ts, r.varFx_PQS, ':', 'color', colors(2,:), 'displayname', 'PQS');

        end

        %xlim(xlim .* [4e-1, 0.5e1] )
        xlim([0.5e-6, 2e-1])
        ylim([1e-6, 1e1])

        if isfield(opt, 'powerline')
            for i = 1:numel(opt.powerline)
                %plot(r.ts, opt.powerline{i}.x, 'color', [0 0 0, 0.3],'linewidth',0.35, 'handlevisibility','off')
                plot([opt.powerline{i}.x(2), opt.powerline{i}.x(end)], [opt.powerline{i}.y(2), opt.powerline{i}.y(end)], 'color', [0 0 0, 0.4],'linewidth',0.3, 'handlevisibility','off')
            end
        end

        xlabel('$t$ (s)')

        leg = legend;
        leg.NumColumns = 2;
        leg.Location = 'northoutside';
        leg.ItemTokenSize = [10,18];


        %annotation('textbox', [.15 .42 .3 .3], 'string',[ opt.label ],'interpreter','latex','edgecolor','none')   
        annotation('textbox', [.7 .42 .3 .3], 'string',[ opt.label ],'interpreter','latex','edgecolor','none')   

        annotation('textbox', [.15 .2 .3 .3], 'string', opt.variables_label, 'interpreter','latex','edgecolor','none')        

        txt_x = split(sprintf('%.1e', r.tracking_error_x),'e');
        txt_x{2} = strrep(txt_x{2},'0','');
        txt_x = ['$e_x = ' txt_x{1} ' \times 10^{' txt_x{2} '}$'];

        txt_p = split(sprintf('%.1e', r.tracking_error_p),'e');
        txt_p{2} = strrep(txt_p{2},'0','');
        txt_p = ['$e_p = ' txt_p{1} ' \times 10^{' txt_p{2} '}$'];


        txt_x_PQS = split(sprintf('%.1e', r.tracking_error_x_PQS),'e');
        txt_x_PQS{2} = strrep(txt_x_PQS{2},'0','');
        txt_x_PQS = ['$e_x = ' txt_x_PQS{1} ' \times 10^{' txt_x_PQS{2} '}$ (PQS)'];

        txt_p_PQS = split(sprintf('%.1e', r.tracking_error_p_PQS),'e');
        txt_p_PQS{2} = strrep(txt_p_PQS{2},'0','');
        txt_p_PQS = ['$e_p = ' txt_p_PQS{1} ' \times 10^{' txt_p_PQS{2} '}$ (PQS)'];


        %annotation('textbox', [.35 .0 .4 .25], 'string', [txt_x sprintf('\n') txt_p sprintf('\n') txt_x_PQS sprintf('\n') txt_p_PQS], 'interpreter','latex','edgecolor','none')
        annotation('textbox', [.35 .0 .4 .25], 'string', [txt_x sprintf('\n') txt_x_PQS], 'interpreter','latex','edgecolor','none')


        xticks([1e-6, 1e-4, 1e-2,])
        %yticks([1e-6, 1e-4, 1e-2, 1e0])
        yticks([1e-5, 1e-3, 1e-1, ])

        %%%%%%%%%% MEAN %%%%%%%%%%%%

        %figure(f2);
        ax2 = subplot(5,1,3:5);
        %ax2 = axes;

        %xstart= 0.25; xend= 0.55;
        %ystart= 0.3;  yend= 0.6;
        %ax2 = axes('position',[xstart ystart xend-xstart yend-ystart ]);


        if opt.mean_plot

            hold on
            p_fx_mean = plot(ax2, r.ts, r.OUx.X, '-' , 'color',  colors(1,:), 'displayname', '$f_x$', 'linewidth', 1); 

            %n_smooth = round(3/(r.OUx.drift_scaling * r.dt))
            %n_smooth = 25;
            %p_fx_mean_sma = plot(ax2, r.ts, smooth(r.OUx.X, n_smooth), '-' , 'color',  colors(5,:), 'displayname', '$f_x$ SMA', 'linewidth', 1); 

            %p_fp_mean_est = plot(ax2, r.ts, r.mFp_PQS, fp_linestyle, 'color', colors(3,:), 'displayname', '$\langle f_p \rangle$ PQS', 'linewidth', 0.5);
            p_fx_mean_est     = plot(ax2, r.ts, r.m5, ':', 'color', colors(3,:), 'displayname', '$\langle f_x \rangle$', 'linewidth', 1); 
            p_fx_mean_est_PQS = plot(ax2, r.ts, r.mFx_PQS, fx_linestyle, 'color', colors(2,:), 'displayname', '$\langle f_x \rangle$ PQS', 'linewidth', 2); 
            %p_fx_mean_est_shifted = plot(ax2, r.ts, r.m5_shifted, ':', 'color', colors(3,:), 'displayname', '$\langle f_x \rangle$ shifted', 'linewidth', 0.5); 


            if with_shading 
                legend('AutoUpdate','off')

                [~, unique_idxs] = unique(r.ts);
                x_shade  = r.ts(unique_idxs);

                y1_shade = r.mFp_PQS(unique_idxs) - sqrt(r.varFp_PQS(unique_idxs));
                y2_shade = r.mFp_PQS(unique_idxs) + sqrt(r.varFp_PQS(unique_idxs));
                shade(  x_shade, y1_shade, '-w', ...
                        x_shade, y2_shade, '-w', 'linestyle', 'none',...
                        'FillAlpha', 0.3, 'FillColor', colors(3,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

                y1_shade = r.mFx_PQS(unique_idxs) - sqrt(r.varFx_PQS(unique_idxs));
                y2_shade = r.mFx_PQS(unique_idxs) + sqrt(r.varFx_PQS(unique_idxs));
                shade(  x_shade, y1_shade, '-w', ...
                        x_shade, y2_shade, '-w', 'linestyle', 'none',...
                        'FillAlpha', 0.3, 'FillColor', colors(2,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

                legend('AutoUpdate','on')
            end



            %p_fp_mean = plot(ax2, r.ts, r.OUp.X, '-', 'color',   colors(6,:), 'displayname', '$f_p$', 'linewidth', 0.5);



            %p_fp_mean_est = plot(ax2, r.ts, r.mFp_PQS, ':', 'color', colors(3,:), 'displayname', 'PQS', 'linewidth', 2);
            %p_fx_mean_est = plot(ax2, r.ts, r.mFx_PQS, ':', 'color', colors(2,:), 'displayname', 'PQS', 'linewidth', 2); 


            if isfield(opt, 'mean_xscale')
                set(ax2, 'xscale', opt.mean_xscale);
            end    


            if strcmp(get(ax2,'xscale'), 'linear')
               xlim(xlim + 2 * ms *[-1, 1]) 
               %xticks([0, round(r.T/2/ms,1)*ms, r. T])
            else
               xlim(xlim .* [8e-1, 1.2] )
               %xticks([10^(-6), 10^(-4), 10^(-2)])
            end
        end

        xlabel('$t$ (s)')

        if isfield(opt, 'OU_variables_labels')
            OUtxt1 = annotation('textbox', [.28 .16 .3 .3], 'string', opt.OU_variables_labels{1}, 'interpreter','latex','edgecolor','none');
            annotation('textbox', OUtxt1.Position + [0.28 0 0 0], 'string', opt.OU_variables_labels{2}, 'interpreter','latex','edgecolor','none')
        end

        if isfield(opt, 'gold_frame')
            gold_frame_1 = annotation(f1,'textbox',[0.15 0.18 0.75 0.525], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1.5);
            gold_frame_2 = annotation(f2,'textbox',[0.135 0.18 0.76 0.525], 'FitBoxToText','off', 'EdgeColor',[1 0.843137264251709 0],'linewidth',1.5);
        end


        leg = legend;
        leg.Location = 'northoutside';
        leg.ItemTokenSize = [10,18];
        leg.NumColumns = 3;

        %ylim([-1.4, 1.4])
        %ylim([-0.1, 1.1])
        ylim([0.1 0.7])
        %xlim([0.005 inf])
        xlim([0.01 inf])
        yticks([0.1, 0.4, 0.7])
        %yticks([-1, 0, 1])
        %


        txt_font_size = 8;
        set( findall(f1, '-property', 'fontsize'), 'fontsize', txt_font_size);
    %    set( findall(f2, '-property', 'fontsize'), 'fontsize', txt_font_size);


        txt_obj = findall(f1,'Type','text');
    end
    %%%%%%% WIGNER ELLIPSE
    
    ts_snapshot = opt.ts_snapshot;
    snapshot_idx = opt.snapshot_idx;
    
    f3 = figure;
    
    set(gcf, 'position', [1361 771 col_width col_width])
    
    ax3 = axes;
    %n_alphas = 20;
    %alphas = linspace(0.5, 0.9, n_alphas);
    %plot_idxs = round(linspace(1, r.nt, n_alphas));
    %hold on
    %relXmean_pqs = 1/sqrt(2) * (r.mEs(1,:)-r.mEs(3,:));
    %sumPmean_pqs = 1/sqrt(2) * (r.mEs(2,:)+r.mEs(4,:));
    %for i = 1:numel(plot_idxs)-1
     %   t1 = plot_idxs(i);
    %    t2 = plot_idxs(i+1);
        % plot(r.relXMean(t1:t2), r.sumPMean(t1:t2), 'handlevisibility', 'off', 'color', [0 0 0 alphas(i)])
%         plot(relXmean_pqs(t1:t2), sumPmean_pqs(t1:t2), 'handlevisibility', 'off', 'color', [colors(1,:) alphas(i)])
    %end
    
    %smoothing_degree = 100;
    %plot(smooth(r_orig.OUx.X, smoothing_degree), smooth(r_orig.OUp.X, smoothing_degree), 'handlevisibility', 'off', 'color', [colors(2,:) 1], 'linewidth',1)
    %hold on
    %plot(r_orig.mFx_PQS, r_orig.mFp_PQS, 'handlevisibility', 'off', 'color', [0 0 0 0.6])
    
    plt_true = plot(r_orig.OUx.X(1), r_orig.OUp.X(1), 'handlevisibility', 'off', 'color', [0, 0, 0, 0.05], 'linewidth',1);
    hold on
    plt_true_dot = plot(r_orig.OUx.X(1), r_orig.OUp.X(1), '.', 'handlevisibility', 'off', 'color', [0,0,0], 'markersize',10);
    
    
    %ellipse_contour_value_normalized = 0.001;
    %W = Wigner_ellipses(r, 'rel_sum', ellipse_contour_value_normalized, 100);
    %W = Wigner_ellipses(r, 'forces', 0.5, 100);
    W = Wigner_ellipses(r, 'forces_pqs', 0.5, 100);
    
    %alphaVals = [linspace(0.5, 0.8, numel(ts_snapshot)-1), 1];
    %linewidthVals = linspace(0.5, 2, numel(ts_snapshot));
    
    plt_ellipse = plot(W.ellipses{1}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$');
    plt_dot    = scatter(W.centers(1, 1), W.centers(2, 1),'k.','handlevisibility','off');
    
    axis equal

    %xlim([0,1.1])
    %ylim([-1.4, -0.4])
    
    area = 0.3 * [-1, 1];
    xlim(r_orig.OUx.X(1) + area)
    ylim(r_orig.OUp.X(1) + area)
    
    
    for i = 1:5:r.nt
       
        set(plt_true, 'xdata', r_orig.OUx.X(1:i), 'ydata', r_orig.OUp.X(1:i))
        set(plt_true_dot, 'xdata', r_orig.OUx.X(i), 'ydata', r_orig.OUp.X(i))        
        set(plt_ellipse, 'xdata', W.ellipses{i}{1}, 'ydata', W.ellipses{i}{2})
        set(plt_dot, 'xdata', W.centers(1, i), 'ydata', W.centers(2, i))
        
        xlim( W.centers(1, i) + area)
        ylim( W.centers(2, i) + area)
    
        
        pause(0.001)
    end
            
    return
    
    
    
    for i = 1:numel(ts_snapshot)
        idx = snapshot_idx(i);           

        p1 = plot(W.ellipses{idx}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$', 'linewidth', linewidthVals(i));
        p1.Color = [p1.Color alphaVals(i)];
        hold on

        %p2 = plot(W.centers(1, idx), W.centers(2, idx), 'k.','markersize',10, 'handlevisibility','off');
        p2 = scatter(W.centers(1, idx), W.centers(2, idx),'k.','handlevisibility','off','MarkerFaceAlpha', alphaVals(i), 'MarkerEdgeAlpha',  alphaVals(i));
        %p2.Color = [p2.Color alphaVals(i)];

        axis equal
    end

    %pause(0.001);
    %ylim(gca, [-1.1,-0.7])
    
    
    xlabel('$f_x$')
    ylabel('$f_p$')
    

    %lines = findobj(f2,'Type','Line');
    %for i = 1:numel(lines)
    %   lines(i).LineWidth = 0.5;
    %end
    
    %xlim([-max(abs(xlim)), max(abs(xlim))])
    %ylim([-max(abs(ylim)), max(abs(ylim))])

    set( findall(f3, '-property', 'fontsize'), 'fontsize', txt_font_size);
    
    set([ax1, ax3], 'linewidth',0.5)
    set([ax1, ax3], 'ticklength', [0.02 0.025])
    
    prefix = [fig_root '/' opt.savename '_'];
    suffix = ['.pdf'];
    savename = @(str) [prefix str suffix];

    
    

    if save_plots 
        exportgraphics(f1,savename(''), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f3,savename('test_'), 'BackgroundColor','none','ContentType','vector')
    %    exportgraphics(f1,savename('var'), 'BackgroundColor','none','ContentType','vector')
    %    exportgraphics(f2,savename('mean'),'BackgroundColor','none','ContentType','vector')
    end


end
toc


%% plot options

function [opt, r, r_orig] = opts(r)

    opt = {};
    
    constants
    
    opt.sqrt_var = 0; 
    opt.mean_xscale = 'linear';
    opt.var_xscale = 'log';
    
    
    switch r.p.label
       
            
        case '2 oscillator 2 xp laser with omega PQS'
           opt.savename='fig8';
           opt.label = '';
           %opt.mean_xscale = 'linear';

         %  opt.powerline{1}.x =  r.ts;   
         %  opt.powerline{1}.y =  1e-12 ./ (opt.powerline{1}.x).^3 ;   
         %  opt.powerline{1}.label =  '$1/t^3$';   
           
           %opt.gold_frame = 0;
%            opt.powerline{2}.x = [1e-5 1e-4 1e-1];
%            opt.powerline{2}.y =  0.5e-7 ./ (opt.powerline{2}.x).^1 ;   
%            opt.powerline{2}.label =  '$1/t$';   
           
           opt.mean_plot = 1;
           opt.var_plot = 1;
           
           opt.variables_label = {...
                ['$\omega_1 = ' num2str(r.w1/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\omega_2 = ' num2str(r.w2/w_01ms) '$'] ... '\omega_{1\mathrm{ms}}$'], ...
                ['$\kappa_{x_-} = ' num2str(r.k11/r.k) '$'] ...
                ['$\kappa_{p_+} = ' num2str(r.k12/r.k) '$']
                };                
                        
           
           %opt.ts_snapshot =  [1e-5, linspace(1e-5, 5*pi/r.w, 10), r.T];
           
           %opt.ts_snapshot =  [1e-3, linspace(1e-4, 5*pi/r.w, 5), r.T];
           opt.ts_snapshot =  [1e-6, linspace(1e-3, 5*pi/r.w, 9)];

            
        otherwise
            error('no label')
    end
    
    opt.ts_snapshot = sort(unique(opt.ts_snapshot));
    [~, snapshot_idx] = arrayfun(@(t) min(abs(r.time.ts - t)), opt.ts_snapshot); % find indices that coincides with desired snap shots
    opt.snapshot_idx = snapshot_idx;
    
    
    %integration_idxs =   r.ts > 0;
    integration_idxs =   r.ts > 0.01;
    
    r.tracking_error_x = tracking_error(r.OUx, r.m5, 'integration_idxs', integration_idxs);
    r.tracking_error_p = tracking_error(r.OUp, r.m6, 'integration_idxs', integration_idxs);
    
    r.tracking_error_x_PQS = tracking_error(r.OUx, r.mFx_PQS, 'integration_idxs', integration_idxs);
    r.tracking_error_p_PQS = tracking_error(r.OUp, r.mFp_PQS, 'integration_idxs', integration_idxs);
    
    r_orig = r;
    
    %ts_plot_interval = 100;    
    %ts_reduced = r.ts([1, 2 : ts_plot_interval : r.nt]);
    
    
    %dt_plot_target = 1e-5;
    %ts_reduced = [0, r.dt, dt_plot_target : dt_plot_target : r.T]; 
    
    
    dt_plot_target1 = 1e-5;
    dt_plot_target2 = 1e-5;
    
    
    % find optimal shift for forward field, based on minimizing the error w. respect to the pqs estimator
    mses = [];
    shifts = [];
    for i = 1:sum(integration_idxs == 1)
        back_shift = -i + 1;
        shifts(i) = back_shift;
        mses(i) = mse_shifted_and_truncated(r.mFx_PQS(integration_idxs), r.m5(integration_idxs), back_shift);
        
    end
    
    [optimal_mse, optimal_shift_idx] = min(mses);
    optimal_shift = shifts(optimal_shift_idx);
    
    r.m5_shifted = circshift(r.m5, optimal_shift);
    integration_idxs = integration_idxs & r.ts <= (r.ts(end) + optimal_shift * r.dt); % do not include the 'circled around' shifted indexs
    r.tracking_error_x_shifted = tracking_error(r.OUx, r.m5_shifted, 'integration_idxs', integration_idxs);
    
    ts_reduced = [[0, r.dt, r.dt:dt_plot_target1 : 1*ms], [1*ms : dt_plot_target2 : r.T]]; 
    
    

    ts_reduced = r.ts; 
    
    %ts_reduced = [0, logspace(log10(r.ts(2)), log10(r.T), 1000)];
    
    %r = reduce_time_resolution(r, ts_reduced);
    
%     ts_plot_interval = 10;    
%     nt = numel(r.ts); 
%     fn = fieldnames(r);
%     for k=1:numel(fn)
%         field = r.(fn{k});
%         if isnumeric(field)
%             field_dimensions = size(field, 2);
%             
%             if field_dimensions == nt % is this time dependent?
%                 r.(fn{k}) = field([1:2, 2:ts_plot_interval:end]);
%             end 
%         end
%     end
%     r.OUx.X = r.OUx.X([1:2, 2:ts_plot_interval:end]);
%     r.OUp.X = r.OUp.X([1:2, 2:ts_plot_interval:end]);
    
    
end



%% optimize delay


function res = mse_shifted_and_truncated(pqs, forward, shift)
    
    shift = floor(shift);
    forward_shifted = circshift(forward, shift);
    
    forward_shifted(end: -1 : end-(shift-1)) = [];
    pqs(end: -1 : end-(shift-1)) = [];
    
    n = numel(forward_shifted);
    
    res =  1/n * sum((pqs - forward_shifted).^2);

end

