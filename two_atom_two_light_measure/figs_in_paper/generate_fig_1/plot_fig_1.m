clear all
close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
%datafolder_root = '/Users/au446513/projects/backaction_evasion/matlab/two_atom_two_light_measure/numerical/mains/paper_figs/fig_2_3/data';


data = 'rs_2021-11-16_07-43-37';



if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end


load([datafolder_root '/' data '.mat'])
stochastic_project_defaults

include = {};
include{end+1} = '2 oscillator 2 xp laser with omega';


fx_linestyle = '-';
fp_linestyle = '-';

%fx_color = [colors(1,:), 1];
%fp_color = [colors(1,:)  1];

with_shading = 1;

x_log = 0;
with_text = 0;
save_plots = 1;


%figure
%set(gcf, 'position', [1641 1581 244 478])
%tiledlayout(2,1)

%    ha = tight_subplot(2, 1, 0.1, 0.1, 0.1);

%axes(ha(2));

%f1=figure;
%ax1 = axes;
%f2=figure;
%ax2 = axes;
tic
for k = 1:numel(rs)


    r = moments_from_results(rs{k});


    if ~any(cellfun(@(x) strcmp(r.p.label, x), include)) % check if label is included?
        continue;
    end

    [opt, r] = opts(r);
    ts_snapshot = opt.ts_snapshot;
    snapshot_idx = opt.snapshot_idx;

    alphaVals = [linspace(0.5, 0.8, numel(ts_snapshot)-1), 1];
    linewidthVals = linspace(1, 2, numel(ts_snapshot));
    
    f1=figure;
    f2=figure;
    f3=figure;
    f4=figure;
    f5=figure;
    
    pos_fs  = [1641 1581 80 60];
    pos_wig = [1641 1581 col_width/2.1 120];

    
    set(f1, 'position', pos_fs + 1*[col_width/2 0 0 0])
    set(f3, 'position', pos_fs + 2*[col_width/2 0 0 0])
    set(f2, 'position', pos_fs + 3*[col_width/2 0 0 0])
    set(f4, 'position', pos_fs + 4*[col_width/2 0 0 0])
    set(f5, 'position', pos_wig + 5*[col_width/2 0 0 0])
    
    
    
    %%%%%%%%%% MEAN %%%%%%%%%%%%
    
    figure(1)
    ax1 = axes;
    axis equal
    p_fx_mean_est = plot(ax1, r.ts, r.m5, fx_linestyle, 'color', colors(2,:), 'displayname', '$\langle f_x \rangle$', 'linewidth', 1); 
    hold on
    
    
    figure(2)
    ax2 = axes;
    p_fp_mean_est = plot(ax2, r.ts, r.m6, fp_linestyle, 'color', colors(1,:), 'displayname', '$\langle f_p \rangle$', 'linewidth', 1);
    hold on
    
    
    if with_shading 
      %  legend('AutoUpdate','off')

        [~, unique_idxs] = unique(r.ts);
        x_shade  = r.ts(unique_idxs);

        y1_shade = r.m6(unique_idxs) - sqrt(r.varFp(unique_idxs));
        y2_shade = r.m6(unique_idxs) + sqrt(r.varFp(unique_idxs));
        shade(ax2,  x_shade, y1_shade, '-w', ...
                x_shade, y2_shade, '-w', 'linestyle', 'none',...
                'FillAlpha', 0.3, 'FillColor', colors(1,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

        y1_shade = r.m5(unique_idxs) - sqrt(r.varFx(unique_idxs));
        y2_shade = r.m5(unique_idxs) + sqrt(r.varFx(unique_idxs));
        shade(ax1,  x_shade, y1_shade, '-w', ...
                x_shade, y2_shade, '-w', 'linestyle', 'none',...
                'FillAlpha', 0.3, 'FillColor', colors(2,:), 'FillType', [1,2; 2,1],'handlevisibility','off');

   %     legend('AutoUpdate','on')
    end

    
    figure(3)
    ax3 = axes;
    p_fx_mean = plot(r.ts, r.OUx.X, '-' , 'color',  colors(2,:), 'displayname', '$f_x$', 'linewidth', 1); 
    
    figure(4)
    ax4 = axes;
    p_fp_mean = plot(r.ts, r.OUp.X, '-', 'color',   colors(1,:), 'displayname', '$f_p$', 'linewidth', 1);
    
 
    
    if x_log
        set([ax1, ax2, ax3, ax4], 'xscale','log')
    end
    
    
    
    %ylim([-1.4, 1.4])
    %yticks([-1, 0, 1])
    
    
    %ax3.YLim([1.6, 2.2])
    
    %return
    
%     ax3.XLim = ax1.XLim;
%     ax3.YLim = ax1.YLim;
%     ax4.XLim = ax2.XLim;
%     ax4.YLim = ax2.YLim;
    ax1.XLim = ax3.XLim;
    ax1.YLim = ax3.YLim;
    ax2.XLim = ax4.XLim;
    ax2.YLim = ax4.YLim;

    
    %linkaxes([ax3, ax1],'xy');
    %linkaxes([ax4, ax2],'xy');
    
    if with_text
        title(ax1, '$\langle f_x \rangle$');
        title(ax2, '$\langle f_p \rangle$');
        title(ax3, '$f_x $');
        title(ax4, '$f_p$');
        text(ax3, 0.0028, 1, '$\gamma_x, \sigma_x$')
        text(ax4, 0.001, -0.55, '$\gamma_p, \sigma_p$')
    end
    
    set(ax1, 'Layer', 'Top')
    set(ax2, 'Layer', 'Top')
    set(ax3, 'Layer', 'Top')
    set(ax4, 'Layer', 'Top')
    
    
    
    %figure(1)
  %  MagInset(f1, ax1, [0, 1e-4, 0.5, 1], [1e-3 5e-3 0.5 0.6], {});

    
    if save_plots
        set(ax1,'box','off') 
        set(ax2,'box','off') 
        set(ax3,'box','off') 
        set(ax4,'box','off') 
        
        xticks(ax1, {})
        xticks(ax2, {})
        xticks(ax3, {})
        xticks(ax4, {})
        yticks(ax1, {})
        yticks(ax2, {})
        yticks(ax3, {})
        yticks(ax4, {})
    end

    
    
    figure(6)
    loglog( r.ts, r.varFx, 'color', colors(2,:), 'displayname', '$\Delta f_x^2$')
    figure(7)
    loglog( r.ts, r.varFp, 'color', colors(1,:), 'displayname', '$\Delta f_p^2$')
    
    
    %%%%%%%%% WIGNER ELLIPSE %%%%%%%%%%%
    
    figure(f5)
    ax5 = axes;
    ellipse_contour_value_normalized = 0.5;


    
    
    
    
   
    W = Wigner_ellipses(r, 'rel_sum', ellipse_contour_value_normalized, 150);

    for i = 1:numel(ts_snapshot)
        idx = snapshot_idx(i);           

        p1 = plot(W.ellipses{idx}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$', 'linewidth', linewidthVals(i));
        p1.Color = [p1.Color alphaVals(i)];
        hold on

        %p2 = plot(W.centers(1, idx), W.centers(2, idx), 'k.','markersize',10, 'handlevisibility','off');
        p2 = scatter(W.centers(1, idx), W.centers(2, idx),'k.','handlevisibility','off','MarkerFaceAlpha', alphaVals(i), 'MarkerEdgeAlpha',  alphaVals(i));
        %p2.Color = [p2.Color alphaVals(i)];

        axis equal
    end
    p1.HandleVisibility = 'on';
    p = plot(W.centers(1, 1 : snapshot_idx(end)), W.centers(2, 1 : snapshot_idx(end)),'k-','linewidth', 0.5, 'handlevisibility','off');
    p.Color = [p.Color 0.2];
    p=plot(nan, 'k.-', 'color',p.Color, 'displayname', '$\mathbf{m}$', 'linewidth',p.LineWidth);
    p.Color = [p.Color 0.2];
    
    set(ax5, 'Layer', 'Top')
    xlim([-8.2, 1.2])
    
    xlabel('$x_-$')
    ylabel('$p_+$')
    if save_plots
        xticklabels({})
        yticklabels({})
    end
    
    %%%%%%%%%%%%%%%%%%%%%
    
    
    txt_font_size = 13;
    set( findall(f1, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f2, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f3, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f4, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f5, '-property', 'fontsize'), 'fontsize', txt_font_size);
    
    set([ax1, ax2, ax3, ax4, ax5], 'linewidth', 1.5)
    

    txt_obj = findall(f1,'Type','text');

    %lines = findobj(f2,'Type','Line');
    %for i = 1:numel(lines)
    %   lines(i).LineWidth = 0.5;
    %end
    
    %xlim([-max(abs(xlim)), max(abs(xlim))])
    %ylim([-max(abs(ylim)), max(abs(ylim))])

    prefix = [fig_root '/' 'fig1_'];
    suffix = ['.pdf'];
    savename = @(str) [prefix str suffix];


    if save_plots 
        exportgraphics(f1,savename('fx_mean'), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f2,savename('fp_mean'), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f3,savename('fx_real'), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f4,savename('fp_real'), 'BackgroundColor','none','ContentType','vector')
        exportgraphics(f5,savename('wig'), 'BackgroundColor','none','ContentType','vector')
    end


end
toc


%% plot options

function [opt, r] = opts(r)

    opt = {};
    
    constants  
    
    dt_plot_target = 1e-5;
    ts_reduced = [0, r.dt, dt_plot_target : dt_plot_target :r.T]; 
    %ts_reduced = r.ts;
    
    %ts_reduced = [0, logspace(log10(r.ts(2)), log10(r.T), 1000)];
    
    ts_reduced = r.ts;
    
    %r = reduce_time_resolution(r, ts_reduced);
    
    %opt.ts_snapshot =  [1e-5, 1e-4, linspace(0, 0.4*ms, 4)];
    opt.ts_snapshot =  [r.dt, 0.25e-3, 0.5e-3, 0.75e-3, 0.95e-3];
    opt.ts_snapshot = sort(unique(opt.ts_snapshot));
    [~, snapshot_idx] = arrayfun(@(t) min(abs(ts_reduced - t)), opt.ts_snapshot); % find indices that coincides with desired snap shots
    opt.snapshot_idx = snapshot_idx;
end

