clear all 
%close all



%rng('default');

stochastic_project_defaults

%%%%
% Atoms: (XA1, PA1) with positive mass and (XA2, PA2) with negative mass 
% Light: (XL1, PL1) and (XL2, PL2)
% Forces: acting on indep. on XA1 and PA1
% y = (m = X1, P1, X2, P2, | n = XL1, PL1, XL2, PL2)'

% H/hbar = +w1/2(XA1^2 + PA1^2) + w2/2(XA1^2 + PA2^2) + k11*PA1PL1 + k21*PA2PL1 + k12XA1XL2 + k22XA2XL2

% After interaction: homodyne detection of XL1 and PL2
%%% 

constants

%% physical parameters

%%%%%%% TIME %%%%%%%%

%T = 1000*ms;
T = 5*ms;
%dt = 1.0e-6;


k =  0.1*sqrt(1.83e6); 
w =  1 * w_1ms;

params = {};


%%%%%%% FORCE %%%%%%%%

% F = c * f

fx0  = 1;
cx  = 3e4;
gammabx = 1e2*s^(-1);
sigmabx = 2*1e1*s^(-1);

fp0  = -1;
cp  = cx;
gammabp = 1*gammabx;
sigmabp = 1*sigmabx;


m0 = [-2; 2; 0; 0; fx0; fp0];
%A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
%if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
%if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end


A0 = diag([1, 1, 1, 1, 1, 1]); 


%%%%%%%%%%%%

dt1 = 1e-6;
dt2 = 1e-6;

params{end+1} = struct(...
    'label', '2 oscillator 2 xp laser with omega', ...
    'A0', A0, 'm0', m0, 'dt', dt1, 'approx_mpinv', 'off', ...
    'w1', 1*w, 'w2', -1*w, 'k11', 1*k, 'k21', 1*k, 'k12', 1*k, 'k22', -1*k ...
);




%%%%%%% RNG %%%%%%%%

OU_seed_x = 1; % 0: random 
OU_seed_p = 3; % 0: random 
measurement_seed_num = 1; % 0: random 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RUN %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

E_covar_initial_val = 10000;


nruns = 1;
rs = {};

with_PQS = 0;

normalize = 0;
logx = 1;

tic
params = fliplr(params); % flip so (possibly) low dt in cell{5} is at front and is treated in first parfor batch
for i = 1:nruns
    for j = 1:numel(params)
         
        p = params{j};
        
        dt = p.dt;
        time = Time('ts', 0:dt:T);  
        
        OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed_x);
        OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed_p);
        
     %   if isfield(p, 'w1')         
        
        
        r = (propagate_with_F(p.A0, p.m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, cx, OUx, cp, OUp, i*measurement_seed_num, 'approx_mpinv', p.approx_mpinv)); 
        
        r.p = p;
        r.w = w;
        r.k = k;
        
        if with_PQS
            r = PQS(r, 'E_covar_initial_val', E_covar_initial_val,'approx_mpinv', p.approx_mpinv);
        end
        
        rs{j} = r;
        
        
    end
end

[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
savename = [datafolder_root '/rs_' char(datetime) '.mat'];
save(savename, 'rs')
