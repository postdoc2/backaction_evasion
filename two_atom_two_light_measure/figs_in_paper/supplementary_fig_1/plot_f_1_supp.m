clear all
close all
clc

constants


[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];

%data = 'rs_2022-01-04_15-13-31'; % <--- in supplementary paper

if ~exist('data', 'var') % if no data chosen, just take most recently generated
    d = dir([datafolder_root '/*mat']);
    [~, newest_idx] = max([d.datenum]);
    data = d(newest_idx).name(1:end-4);
end

load([datafolder_root '/' data '.mat'])
stochastic_project_defaults

save_plots = 1;

%% Analytical forms for w1=-w2, k11=k12=k1, k21=-k22=k2, and no field

f1 = figure;
%subplot(2,2,1)
subplot(2,1,1)

r = moments_from_results(rs{1});

A11 = squeeze(r.gs(1,1,:))';
A22 = squeeze(r.gs(2,2,:))';
A33 = squeeze(r.gs(3,3,:))';
A44 = squeeze(r.gs(4,4,:))';

if r.w == 0
    sinwt_w   = r.ts;
    sin2wt_2w = r.ts;
else
    sinwt_w = sin(r.w*r.ts)/r.w;
    sin2wt_2w = sin(2*r.w*r.ts)/(2*r.w);
end

relX0 = r.relXVar(1);
sumP0 = r.sumPVar(1);

Kp_sq = r.k2^2 + r.k1^2;
Km_sq = r.k2^2 - r.k1^2;

nx = relX0 + (sumP0 - relX0)*sin(r.w*r.ts).^2 + 2*relX0 * sumP0*( Kp_sq*r.ts - sin2wt_2w*Km_sq );
np = sumP0 + (relX0 - sumP0)*sin(r.w*r.ts).^2 + 2*relX0 * sumP0*( Kp_sq*r.ts + sin2wt_2w*Km_sq );
d =  (1 + 2*relX0*Kp_sq*r.ts ).*(1 + 2*sumP0*Kp_sq*r.ts ) + 2*sinwt_w * Km_sq .* (cos(r.w*r.ts) * (relX0 - sumP0) - 2*sinwt_w * Km_sq * relX0 * sumP0);
%d =  (1 + 2*Var_mu1*Kp*r.ts ).*(1 + 2*Var_mu2*Kp*r.ts ) + 2*cos(r.w*r.ts).*sinwt_w * Km * (Var_mu1 - Var_mu2) - 4*sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2;
%d =  (1 + 2*Var_mu1*Kp*r.ts ).*(1 + 2*Var_mu2*Kp*r.ts ) + 2*sin2wt_2w * Km * (Var_mu1 - Var_mu2) - 4*sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2; % Works, but some inaccuracy wrt. numerics because of finite dt

r.relXVar_ana = nx./d;
r.sumPVar_ana = np./d;

lw = 2;
the_line_alpha = 0.8;

loglog(r.ts, r.relXVar, 'linewidth', lw, 'color', colors(1,:), 'displayname', '$\Delta \hat x_-^2$')
hold on
loglog(r.ts, r.sumPVar, 'linewidth', lw, 'color', colors(2,:), 'displayname', '$\Delta \hat p_+^2$')

loglog(r.ts, r.sumPVar_ana, 'k-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])
loglog(r.ts, r.relXVar_ana, 'k-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])

leg = legend;
leg.Location = 'southwest';
leg.Box = 'off';


title('(a)')


%%% Analytical means, k1=k2, A(0) = c * [1,1,1,1]^T

r = moments_from_results(rs{2});

idxs = 1:numel(r.ts)-1;
dmxm_ana = (cos(r.wt) - 1)*r.relXMean(idxs) + r.sumPMean(idxs)*sin(r.wt) - sqrt(2)*(r.k2t*cos(r.wt)*r.DeltaL2./(2*r.k2t^2 + 1./(2*r.relXVar(idxs))) - r.k1t*sin(r.wt)*r.DeltaL1./(2*r.k1t^2 + 1./(2*r.sumPVar(idxs))));
dmpp_ana = (cos(r.wt) - 1)*r.sumPMean(idxs) - r.relXMean(idxs)*sin(r.wt) + sqrt(2)*(r.k2t*sin(r.wt)*r.DeltaL2./(2*r.k2t^2 + 1./(2*r.relXVar(idxs))) + r.k1t*cos(r.wt)*r.DeltaL1./(2*r.k1t^2 + 1./(2*r.sumPVar(idxs))));

[mxm_ana, mpp_ana] = deal(zeros(size(r.m1)));
mxm_ana(1) = r.relXMean(1);
mpp_ana(1) = r.sumPMean(1);
for i = 1:r.nt-1
    mxm_ana(i+1) = mxm_ana(i) + dmxm_ana(i);
    mpp_ana(i+1) = mpp_ana(i) + dmpp_ana(i);
end

subplot(2,1,2)
%subplot(2,2,3)
hold on
plot(r.ts, r.relXMean, 'linewidth', lw, 'displayname', '$m_{x_-}$')
plot(r.ts, r.sumPMean, 'linewidth', lw, 'displayname', '$m_{p_+}$')

plot(r.ts, mxm_ana,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])
plot(r.ts, mpp_ana,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])


leg = legend;
leg.Location = 'southwest';
leg.Box = 'off';

title('(b)')
set(gca,'xscale','log')
xlabel('$t$ (s)')

%subplot(2,2,3)
%plot(r.ts(idxs), dmxm_ana - diff(r.relXMean))


pause(0.001)
autoArrangeFigures(2,2,2)


%subplot(2,2,[2,4])

f2=figure;
ellipse_contour_value_normalized = 0.5;

W = Wigner_ellipses(r, 'rel', ellipse_contour_value_normalized, 1000);
%opt.ts_snapshot =  linspace(0, 0.5*pi/r.w, 4);
opt.ts_snapshot =  linspace(0, r.T, 8);
opt.ts_snapshot = sort(unique(opt.ts_snapshot));
[~, snapshot_idx] = arrayfun(@(t) min(abs(r.time.ts - t)), opt.ts_snapshot); % find indices that coincides with desired snap shots
opt.snapshot_idx = snapshot_idx;

alphaVals = [linspace(0.7, 0.9, numel(opt.ts_snapshot)-1), 1];
linewidthVals = linspace(0.5, 2, numel(opt.ts_snapshot));

 for i = 1:numel(opt.ts_snapshot)
        idx = snapshot_idx(i);           

        p1 = plot(W.ellipses{idx}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$', 'linewidth', linewidthVals(i));
        p1.Color = [p1.Color alphaVals(i)];
        hold on

        %p2 = plot(W.centers(1, idx), W.centers(2, idx), 'k.','markersize',10, 'handlevisibility','off');
        p2 = scatter(W.centers(1, idx), W.centers(2, idx),'k.','handlevisibility','off','MarkerFaceAlpha', alphaVals(i), 'MarkerEdgeAlpha',  alphaVals(i));
        %p2.Color = [p2.Color alphaVals(i)];
        
        
        axis equal
end
p1.HandleVisibility = 'on';
p = plot(W.centers(1, :), W.centers(2, :),'k-','linewidth', 0.5, 'handlevisibility','on', 'displayname', '$\mathbf{m}$');
p.Color = [p.Color, 0.2];
xlabel('$x_{-}$')
ylabel('$p_{+}$')


title('(c)')
    




%%%%%% Single field
f3 = figure ;

%%% gamma = 0
r = moments_from_results(rs{3});

ax1 = subplot(2,1,1);
varFp_ana = r.varFp(1) ./(1 + 2/3 * r.cp^2 * r.varFp(1) * r.k1^2 * r.ts.^3 .* ( (1 + r.sumPVar(1) * r.k1^2 * r.ts) ./  (1 + 4 * r.sumPVar(1) * r.k1^2 * r.ts)) );
%varFp_ana2 = r.varFp(1) * (1 + 2 * r.k1^2 * r.ts )./( 1 +  2 * r.k1^2 * r.ts + 2/3 * r.k1^2 * r.cp^2 * r.varFp(1) * r.ts.^3 + 1/3 * r.k1^4 * r.cp^2 * r.varFp(1) * r.ts.^4); % <-- rewritten, also works

loglog(r.ts, r.varFp, 'color', colors(2,:), 'displayname', '$\Delta f_p^2$','linewidth',lw)
hold on
loglog(r.ts, varFp_ana,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])
%loglog(r.ts, varFp_ana2,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0.5,0, the_line_alpha])
leg = legend;
leg.Location = 'southwest';
leg.Box = 'off';

title('(a)')

%%% gamma ≠ 0
r = moments_from_results(rs{4});
ax2 = subplot(2,1,2);


varFp_ana = r.varFp(1) * exp(-2*r.gammabp*r.ts)./(1 + r.varFp(1) * r.cp^2*r.k1^2./(r.gammabp^4*(2*r.k1^2 * r.ts + 1)).* ...
                ( ...
                   -(4*r.k1^2 + r.gammabp*(1 + 2*r.k1^2*r.ts)).*exp(-2*r.gammabp*r.ts)  ...
                   + 4*(2*r.k1^2 + r.gammabp).*exp(-r.gammabp*r.ts) ...
                   + (2*r.gammabp*r.ts*(r.gammabp + r.k1^2) - (4*r.k1^2 + 3*r.gammabp))...
                )...
               );

           
varFp_ana2 = 1/(1 + r.varFp(1) * r.cp^2 / ( r.gammabp^2) * (1 +  r.k1^2/r.gammabp)) * r.varFp(1) * exp(-2 * r.gammabp * r.ts);           

loglog(r.ts, r.varFp, 'color', colors(2,:), 'displayname', '$\Delta f_p^2$','linewidth',lw)
hold on
loglog(r.ts, varFp_ana,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])
%loglog(r.ts, varFp_ana2,'-', 'linewidth', 0.5,'handlevisibility','off','color', [0,0,0, the_line_alpha])

linkaxes([ax1 ax2])

xlabel('$t$ (s)')
title('(b)')

prefix = [fig_root '/supplementary_fig_'];
suffix = ['.pdf'];
savename = @(str) [prefix str suffix];


pos =  [3871 1821 col_width 265];
set(f1, 'position', pos);
set(f2, 'position', [1641 1828 col_width 231])
set(f3, 'position', pos);


if save_plots 
    %exportgraphics(f1, savename('1'), 'BackgroundColor','none','ContentType','vector')
    exportgraphics(f1, savename('1_1'), 'BackgroundColor','none','resolution',300)
    exportgraphics(f2, savename('1_2'), 'BackgroundColor','none','resolution',300)
    exportgraphics(f3, savename('2'),   'BackgroundColor','none','resolution',200)
    %exportgraphics(f2, savename('mean'),'BackgroundColor','none','ContentType','vector')
end
