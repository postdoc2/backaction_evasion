clear all 
close all


stochastic_project_defaults

constants

T = 20*ms;
dt = 1.0e-6;
time = Time('ts', 0:dt:T);

k =  0.25*sqrt(1.83e6); 
w =  0.1 * w_1ms;

params = {};

%% Analytical forms for w1=-w2, k11=k12=k1, k21=-k22=k2, and no force

m0 = [1; 3; 0; 0; 0; 0];
OU_seed = 1; % 0: random 
measurement_seed_num = 1; % 0: random 


%%%%%%%%% Field free

%%% Analytical variance
k1 = 1.5*k;
k2 = 0.5*k;

params{end+1} = struct(... 
    'label', 'variance', ...
    'A0', diag([1.2, 1.5, 1.7, 1.1, 1, 1]), 'm0', m0, ...
    'w1', w, 'w2', -w, 'k11', k1, 'k21', k1, 'k12', k2, 'k22', -k2, ...
    'fx0', 0, 'cx', 0, 'gammabx', 0, 'sigmabx', 0, ...
    'fp0', 0, 'cp', 0, 'gammabp', 0, 'sigmabp', 0  ...
);

%%% Analytical mean
params{end+1} = struct(...
    'label', 'mean', ...
    'A0', diag([1,1,1,1,1,1]), 'm0', m0, ...
    'w1', w, 'w2', -w, 'k11', k, 'k21', k, 'k12', k, 'k22', -k, ...
    'fx0', 0, 'cx', 0, 'gammabx', 0, 'sigmabx', 0, ...
    'fp0', 0, 'cp', 0, 'gammabp', 0, 'sigmabp', 0  ...
);

%%%%%%%%% Single field, fp, no osc.

w = 0;
cp = 1.5e4;
fp0 = 1;

%%% Gamma = 0
gammabp = 0;

params{end+1} = struct(...
    'label', 'mean', ...
    'A0', diag([1,1,1,1,1,1]), 'm0', m0, ...
    'w1', w, 'w2', -w, 'k11', k, 'k21', k, 'k12', k, 'k22', -k, ...
    'fx0', 0, 'cx', 0, 'gammabx', 0, 'sigmabx', 0, ...
    'fp0', fp0, 'cp', cp, 'gammabp', gammabp, 'sigmabp', 0  ...
);


%%% Gamma ≠ 0
gammabp = 5e2;

params{end+1} = struct(...
    'label', 'mean', ...
    'A0', diag([1,1,1,1,1,1]), 'm0', m0, ...
    'w1', w, 'w2', -w, 'k11', k, 'k21', k, 'k12', k, 'k22', -k, ...
    'fx0', 0, 'cx', 0, 'gammabx', 0, 'sigmabx', 0, ...
    'fp0', fp0, 'cp', cp, 'gammabp', gammabp, 'sigmabp', 0  ...
);

%% 

for j = 1:numel(params)
         
        p = params{j};
        
        OUx = OrnsteinUhlenbeckProcess(time, p.gammabx, 0, p.sigmabx).Ito_SDE(p.fx0, 'rngseed', OU_seed);
        OUp = OrnsteinUhlenbeckProcess(time, p.gammabp, 0, p.sigmabp).Ito_SDE(p.fp0, 'rngseed', OU_seed + 1);
        
        r = (propagate_with_F(p.A0, p.m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, p.cx, OUx, p.cp, OUp, measurement_seed_num)); 
        
        rs{j} = r;
end




[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
savename = [datafolder_root '/rs_' char(datetime) '.mat'];
save(savename, 'rs')

return

A11 = squeeze(r.gs(1,1,:))';
A22 = squeeze(r.gs(2,2,:))';
A33 = squeeze(r.gs(3,3,:))';
A44 = squeeze(r.gs(4,4,:))';

if r.w == 0
    sinwt_w   = r.ts;
    sin2wt_2w = r.ts;
else
    sinwt_w = sin(r.w*r.ts)/r.w;
    sin2wt_2w = sin(2*r.w*r.ts)/(2*r.w);
end

relX0 = r.relXVar(1);
sumP0 = r.sumPVar(1);

Kp_sq = r.k2^2 + r.k1^2;
Km_sq = r.k2^2 - r.k1^2;

nx = relX0 + (sumP0 - relX0)*sin(r.w*r.ts).^2 + 2*relX0 * sumP0*( Kp_sq*r.ts - sin2wt_2w*Km_sq );
np = sumP0 + (relX0 - sumP0)*sin(r.w*r.ts).^2 + 2*relX0 * sumP0*( Kp_sq*r.ts + sin2wt_2w*Km_sq );
d =  (1 + 2*relX0*Kp_sq*r.ts ).*(1 + 2*sumP0*Kp_sq*r.ts ) + 2*sinwt_w * Km_sq .* (cos(r.w*r.ts) * (relX0 - sumP0) - 2*sinwt_w * Km_sq * relX0 * sumP0);
%d =  (1 + 2*Var_mu1*Kp*r.ts ).*(1 + 2*Var_mu2*Kp*r.ts ) + 2*cos(r.w*r.ts).*sinwt_w * Km * (Var_mu1 - Var_mu2) - 4*sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2;
%d =  (1 + 2*Var_mu1*Kp*r.ts ).*(1 + 2*Var_mu2*Kp*r.ts ) + 2*sin2wt_2w * Km * (Var_mu1 - Var_mu2) - 4*sinwt_w.^2 * Km^2 * Var_mu1 * Var_mu2; % Works, but some inaccuracy wrt. numerics because of finite dt

r.relXVar_ana = nx./d;
r.sumPVar_ana = np./d;

figure
loglog(r.ts, r.relXVar_ana)
hold on
loglog(r.ts, r.relXVar,'--')
loglog(r.ts, r.sumPVar_ana)
loglog(r.ts, r.sumPVar,'--')



%%% Analytical means, k1=k2, A(0) = c * [1,1,1,1]^T

A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

k11 =  k;
k21 =  k;
k12 =  k;
k22 = -k;


r = (moments_from_results(propagate_with_F(A0, m0, k11, k12, k21, k22, w1, w2, cx, OUx, cp, OUp, measurement_seed_num))); 

idxs = 1:numel(r.ts)-1;
dmxm_ana = (cos(r.wt) - 1)*r.relXMean(idxs) + r.sumPMean(idxs)*sin(r.wt) - sqrt(2)*(r.k2t*cos(r.wt)*r.DeltaL2./(2*r.k2t^2 + 1./(2*r.relXVar(idxs))) - r.k1t*sin(r.wt)*r.DeltaL1./(2*r.k1t^2 + 1./(2*r.sumPVar(idxs))));
dmpp_ana = (cos(r.wt) - 1)*r.sumPMean(idxs) - r.relXMean(idxs)*sin(r.wt) + sqrt(2)*(r.k2t*sin(r.wt)*r.DeltaL2./(2*r.k2t^2 + 1./(2*r.relXVar(idxs))) + r.k1t*cos(r.wt)*r.DeltaL1./(2*r.k1t^2 + 1./(2*r.sumPVar(idxs))));

[mxm_ana, mpp_ana] = deal(zeros(size(r.m1)));
mxm_ana(1) = r.relXMean(1);
mpp_ana(1) = r.sumPMean(1);
for i = 1:r.nt-1
    mxm_ana(i+1) = mxm_ana(i) + dmxm_ana(i);
    mpp_ana(i+1) = mpp_ana(i) + dmpp_ana(i);
end


%normalize = 1; logx = 1;

%plotting(r, normalize, logx, {'mean', 'meanrel', 'meaninc', 'var', 'varrel', 'varreldiff', 'fmean', 'fvar'}) 

%plot(r.dmxm_ana);
figure
hold on
plot(r.ts, mxm_ana)
plot(r.ts, r.relXMean,'--')
plot(r.ts, mpp_ana)
plot(r.ts, r.sumPMean,'--')
set(gca,'xscale','log')

figure
plot(r.ts(idxs), dmxm_ana - diff(r.relXMean))




    %subplot(2,1,2)
    %axes(ha(2));
    f3=figure;
    ellipse_contour_value_normalized = 0.5;

    W = Wigner_ellipses(r, 'rel', ellipse_contour_value_normalized, 100);

    opt.ts_snapshot =  linspace(0, 0.5*pi/r.w, 4);
    opt.ts_snapshot = sort(unique(opt.ts_snapshot));
    [~, snapshot_idx] = arrayfun(@(t) min(abs(r.time.ts - t)), opt.ts_snapshot); % find indices that coincides with desired snap shots
    opt.snapshot_idx = snapshot_idx;
    
    alphaVals = [linspace(0.2, 0.5, numel(opt.ts_snapshot)-1), 1];
    linewidthVals = linspace(0.5, 2, numel(opt.ts_snapshot));
    
    for i = 1:numel(opt.ts_snapshot)
        idx = snapshot_idx(i);           

        p1 = plot(W.ellipses{idx}{:}, 'color', colors(1,:),'handlevisibility','off', 'displayname','$\mathbf{\gamma}$', 'linewidth', linewidthVals(i));
        p1.Color = [p1.Color alphaVals(i)];
        hold on

        %p2 = plot(W.centers(1, idx), W.centers(2, idx), 'k.','markersize',10, 'handlevisibility','off');
        p2 = scatter(W.centers(1, idx), W.centers(2, idx),'k.','handlevisibility','off','MarkerFaceAlpha', alphaVals(i), 'MarkerEdgeAlpha',  alphaVals(i));
        %p2.Color = [p2.Color alphaVals(i)];

        axis equal
    end
    p1.HandleVisibility = 'on';
    p = plot(W.centers(1, 1 : snapshot_idx(end)), W.centers(2, 1 : snapshot_idx(end)),'k-','linewidth', 0.5, 'handlevisibility','off');
    p.Color = [p.Color 0.2];
    p=plot(nan, 'k.-', 'color',p.Color, 'displayname', '$\mathbf{m}$', 'linewidth',p.LineWidth);
    p.Color = [p.Color 0.2];


    leg = legend;
    leg.Location = 'northoutside';
    leg.Orientation = 'horizontal';
    leg.Box = 'off';
    leg.ItemTokenSize = [18,18];

    %set(gca,'XAxisLocation','top')
    set(gca,'YAxisLocation','right')

    if strcmp(opt.wigner_plot_var, 'A1')
        xlabel('$x$')
        ylabel('$p$')
    else
        xlabel('$x_{-}$')
        ylabel('$p_{+}$')
    end


    % 
    pos = [1641 1581 col_width/2.1 120];
    set(f1, 'position', pos)
    set(f2, 'position', pos)

    txt_font_size = 8;
    set( findall(f1, '-property', 'fontsize'), 'fontsize', txt_font_size);
    set( findall(f2, '-property', 'fontsize'), 'fontsize', txt_font_size);


    txt_obj = findall(f1,'Type','text');

%%%%%%

pause(0.001)
autoArrangeFigures(2,2,2)




return
[this_path, this_name, ~] = fileparts(mfilename('fullpath'));
datafolder_root = [this_path '/data'];
savename = [datafolder_root '/rs_' char(datetime) '.mat'];
save(savename, 'rs')
