classdef Time 
   
    properties (SetAccess = private)
       
        T 
        dt
        nt
        ts
        
    end
    
    methods
       
        function obj = Time(varargin)
            %% varargin must contain appropriate combinations fields 'T', 'dt', 'nt', or 'ts'
            
            p = inputParser;
            
            validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x > 0);
            
            addParameter(p, 'T',  [], validScalarPosNum);
            addParameter(p, 'dt', [], validScalarPosNum);
            addParameter(p, 'nt', [], validScalarPosNum);
            addParameter(p, 'ts', [], @(x) isnumeric(x) && isvector(x));
            
            parse(p, varargin{:});
            
            T = p.Results.T;
            dt = p.Results.dt;
            nt = p.Results.nt;
            ts = p.Results.ts;
            
            
            if ~isempty(dt)
                if dt == 0
                   error('time:dtzero','dt must be nonzero');
                end
            end
            
            if      ~isempty(ts)
                
               obj.ts = ts;
               obj.dt = ts(2)-ts(1);
               obj.nt = numel(ts);
               obj.T  = ts(end);
                
            elseif  ~isempty(T) && ~isempty(dt)
                
                obj.dt = dt;
                obj.nt = floor(T/dt) + 1;
                obj.T  = (obj.nt-1)*dt;

                obj.ts = 0 : dt : obj.T;
                
            
            elseif  ~isempty(T) && ~isempty(nt)                
                
                obj.T = T;
                obj.nt = nt;
                obj.dt = T/(nt-1);
                obj.ts = 0 : obj.dt : obj.T;
                
                
            elseif  ~isempty(nt) && ~isempty(dt)
                
                obj.nt = nt;
                obj.dt = dt;
                
                obj.T = (nt-1)*dt;
                obj.ts = 0 : dt : obj.T;
                            
            else
                
                if any(size(ts) ~= 0) && isempty(ts)
                    error('time:tsemptybutpassed','ts was passed in but was empty')
                end
                
                error('time:insufficientparams','Insufficient parameters')
            end

            if isempty(obj.ts) || isempty(obj.T) || isempty(obj.nt) || isempty(obj.dt)
               error('Something was empty') 
            end

        end
        
        function [time, idxs] = from(obj, tmin, tmax)
            
            if tmin >= tmax
                error('time:from:tminlargerthantmax','tmin must be lower than tmax');
            end
            
            min_idx = find(obj.ts >= tmin, 1);
            max_idx = find(obj.ts >= tmax, 1);
            
            if tmin < obj.ts(1)   
                min_idx = 1;
                warning('time:from:tminlowerbound','tmin out of lower bounds'); 
            elseif tmin > obj.ts(end)
                min_idx = obj.nt - 1;
                warning('time:from:tminupperbound','tmin out of upper bounds'); 
            end
            if tmax < obj.ts(1)    
                max_idx = 2;
                warning('time:from:tmaxlowerbound','tmax out of lower bounds'); 
            elseif tmax > obj.ts(end)  
                max_idx = obj.nt;
                warning('time:from:tmaxupperbound','tmax out of upper bounds'); 
            end    
            
            idxs = [min_idx, max_idx];
            time = Time('ts', obj.ts(min_idx : max_idx));
            
        end        
        
        function r = length(obj)
           r = length(obj.ts);
        end
        
        function r = numel(obj)
           r = numel(obj.ts);
        end
    end
    
end