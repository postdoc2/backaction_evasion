function var_is_class(variable, class_requirement)
name = inputname(1);

if ~isa(variable, class_requirement)
    error('class(%s) = %s -- must be of class %s', name, class(variable), class_requirement)
end


end