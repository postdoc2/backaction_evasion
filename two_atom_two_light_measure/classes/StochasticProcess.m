classdef (Abstract) StochasticProcess 

    methods (Abstract)
       
        drift_function(obj, t, X)
        diffusion_function(obj, t, X)
        diffusion_function_derivative(obj, t, X)
        
    end
    
    %%
    
    properties
       
        time
        
        X0
        X
        W
       
        number_of_samples
        use_euler_stepping = 1;
    end
    
    methods

        function obj = StochasticProcess(time)
            
            if ~isa(time, 'Time')
               error('time input must be of class Time');
            end
            
            obj.time = time;

        end
        
        
        function obj = Ito_SDE(obj, X0, varargin)

            defaultSeed = 0;
            p = inputParser;
            addParameter(p, 'rngseed', defaultSeed);
            parse(p, varargin{:});
            rngseed = p.Results.rngseed;
            
            
            % Number of samples are determined by number of elements in X0
            
            % Check if input X0 layout is [x,y,z, ...] and transpose it in that case
            Xdims = size(X0);
            if Xdims(2) > 1
                if Xdims(1) > 1
                    error(['Need at least one singleton dim in X0 - found size(X0) = ' num2str(Xdims)])
                end
                X0 = X0';
            end
            obj.X0 = X0;
            obj.number_of_samples = numel(X0);
            
            
            if rngseed ~= 0
                rng_state = rng;
                rng(rngseed)
            end
            dW = sqrt(obj.time.dt)*randn(obj.number_of_samples, obj.time.nt-1);
            if rngseed ~= 0
                rng(rng_state)
            end
            
            obj.W  = [zeros(obj.number_of_samples,1), cumsum(dW,2)];
            
            X = zeros(obj.number_of_samples, obj.time.nt);
            X(:,1) = obj.X0;

            for i = 2:obj.time.nt

                dW = obj.W(:,i)-obj.W(:,i-1);
                X_prev = X(:,i-1);
                t = obj.time.ts(i);

                if obj.use_euler_stepping
                    X(:,i) = X_prev + drift_function(obj, t, X_prev)*obj.time.dt + diffusion_function(obj, t, X_prev).*dW;
                else
                    X(:,i) =  X_prev  ... 
                             +  drift_function(obj, t, X_prev) * obj.time.dt ...
                             +  diffusion_function(obj, t, X_prev) .* dW  ...
                             +  0.5*diffusion_func(obj, t, X_prev) .* diffusion_function_derivative(obj, t, X_prev).*((dW).^2 - obj.time.dt);
                end
            end

            obj.X = X;
        end
    end
    
    

   
end

