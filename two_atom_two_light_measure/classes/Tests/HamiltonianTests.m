classdef HamiltonianTests < matlab.unittest.TestCase
   
  
    
    properties (TestParameter)
        
        
        
        quantum_var_type = {OscillatorSystem('atom1').x, OscillatorSystem('atom1').p}
        %quantum_vars_type = {param.x, param.p}
        scaling_type = {2, sym('c')};
        
        quantum_var_type2 = {
            {OscillatorSystem('atom1').x, OscillatorSystem('atom1').x},
            {OscillatorSystem('atom1').x, OscillatorSystem('atom1').p},
            {OscillatorSystem('atom1').p, OscillatorSystem('atom1').x},
            {OscillatorSystem('atom1').p, OscillatorSystem('atom1').p},
            }
        
        
        %scalings = {2, };
        
    end
    
    methods (Test)
        
        function LinearTermTest(testCase, quantum_var_type, scaling_type)
            
            % Normal constructor
            term1 = LinearTerm(scaling_type, quantum_var_type);
            
            testCase.assertEqual( isequal(term1.scaling, scaling_type), true)
            testCase.assertEqual(strcmp(term1.variable_1.id,  quantum_var_type.id), true)
            
            % From multiplying oscillator variables
            term2 = scaling_type * quantum_var_type;
            testCase.assertEqual(isequal(term2.scaling, scaling_type), true)
            testCase.assertEqual(strcmp(term2.variable_1.id,  quantum_var_type.id), true)
            
            term3 = quantum_var_type * scaling_type;
            testCase.assertEqual(isequal(term3.scaling, scaling_type), true)
            testCase.assertEqual(strcmp(term3.variable_1.id,  quantum_var_type.id), true)
            
            term4 = scaling_type * term1 ;
            testCase.assertEqual(isequal(term4.scaling, scaling_type^2), true)
            testCase.assertEqual(strcmp(term4.variable_1.id,  quantum_var_type.id), true)
            
            
            term5 =  term2 * scaling_type;
            testCase.assertEqual(isequal(term5.scaling, scaling_type^2), true)
            testCase.assertEqual(strcmp(term5.variable_1.id,  quantum_var_type.id), true)
            
            
            % plus / minus overload for same term
            
            negateterm = -term1;
            testCase.assertEqual(negateterm.scaling, -scaling_type)
            
            sumterm = term1 + term1;
            testCase.assertEqual(sumterm.scaling, scaling_type + scaling_type)
            testCase.assertEqual(sumterm.variable_1.id, quantum_var_type.id)
            
            diffterm = term1 - term1;
            testCase.assertEqual(diffterm.scaling, scaling_type - scaling_type)
            testCase.assertEqual(diffterm.variable_1.id, quantum_var_type.id)
            
            diffterm = -term1 + term1;
            testCase.assertEqual(diffterm.scaling, scaling_type - scaling_type)
            testCase.assertEqual(diffterm.variable_1.id, quantum_var_type.id)
            
            diffterm = -term1 - term1;
            testCase.assertEqual(diffterm.scaling, -scaling_type - scaling_type)
            testCase.assertEqual(diffterm.variable_1.id, quantum_var_type.id)
        end
       
        
         function QuadraticTermTest(testCase, quantum_var_type2, scaling_type)
            
             var1 = quantum_var_type2{1};
             var2 = quantum_var_type2{2};
             
             % Normal constructor
             term1 = QuadraticTerm(scaling_type, var1, var2);
             
             testCase.assertEqual(isequal(term1.scaling, scaling_type), true)
             testCase.assertTrue(strcmp(term1.variable_1.id, var1.id))
             testCase.assertTrue(strcmp(term1.variable_2.id, var2.id))
             
             % From multiplications
             
             lin_term1 = LinearTerm(scaling_type, var1);
             lin_term2 = LinearTerm(scaling_type, var2);
             
             term2 = lin_term1 * lin_term2;
             
             testCase.assertEqual(isequal(term2.scaling, scaling_type^2), true)
             testCase.assertTrue(strcmp(term2.variable_1.id, var1.id))
             testCase.assertTrue(strcmp(term2.variable_2.id, var2.id))
             
             
             term3 = scaling_type * lin_term2 * lin_term1;
             
             testCase.assertEqual(isequal(term3.scaling, scaling_type^3), true)
             testCase.assertTrue(strcmp(term3.variable_1.id, var2.id))
             testCase.assertTrue(strcmp(term3.variable_2.id, var1.id))
             
             
             term4 = scaling_type * scaling_type * var1 * var2;
             
             testCase.assertEqual(isequal(term4.scaling, scaling_type^2), true)
             testCase.assertTrue(strcmp(term4.variable_1.id, var1.id))
             testCase.assertTrue(strcmp(term4.variable_2.id, var2.id))
             
             % Plus / minus for same term
             
             negateterm = -term1;
             testCase.assertEqual(negateterm.scaling, -scaling_type)
             
             sumterm = term1 + term1;
             testCase.assertEqual(sumterm.scaling, 2*scaling_type)
             
             diffterm = term1 - term1;
             testCase.assertEqual(diffterm.scaling, scaling_type - scaling_type)
             
             diffterm = -term1 + term1;
             testCase.assertEqual(diffterm.scaling, scaling_type - scaling_type)
             
             diffterm = - term1 - term1;
             testCase.assertEqual(diffterm.scaling, -scaling_type - scaling_type)
             
         end
         
         function HamiltonianTest(testCase, scaling_type)
             
             A1 = OscillatorSystem('A1');
             A2 = OscillatorSystem('A2');
             
             x1 = A1.x;
             p1 = A1.p;
             
             x2 = A2.x;
             p2 = A2.p;
             
             lin_term1 = scaling_type * x1;
             lin_term2 = scaling_type * p1;
             
             quad_term1 = scaling_type * x1*p2;
             quad_term2 = scaling_type * x2^2;
             
             % Constructor
             H = Hamiltonian(lin_term1);
             terms = H.terms;
             testCase.assertLength(terms, 1)
             testCase.assertEqual(terms{1}.variable_1.id, x1.id)
             
             
             H = Hamiltonian(lin_term1, lin_term2);
             terms = H.terms;
             testCase.assertLength(terms, 2)
             testCase.assertEqual(terms{1}.variable_1.id, x1.id)
             testCase.assertEqual(terms{2}.variable_1.id, p1.id)
             
             H = Hamiltonian(lin_term1, quad_term1, quad_term2, lin_term2);
             terms = H.terms;
             testCase.assertLength(terms, 4)
             testCase.assertEqual(terms{1}.variable_1.id, x1.id)
             
             testCase.assertEqual(terms{2}.variable_1.id, x1.id)
             testCase.assertEqual(terms{2}.variable_2.id, p2.id)
             
             testCase.assertEqual(terms{3}.variable_1.id, x2.id)
             testCase.assertEqual(terms{3}.variable_2.id, x2.id)
             
             testCase.assertEqual(terms{4}.variable_1.id, p1.id)
             
           
             % Plus / minus overloads
             H = Hamiltonian(lin_term1) + Hamiltonian(quad_term1);
             testCase.assertLength(H.terms, 2)
             testCase.assertEqual(terms{1}.variable_1.id, x1.id)
             testCase.assertEqual(terms{2}.variable_1.id, x1.id)
             testCase.assertEqual(terms{2}.variable_2.id, p2.id)
             
             H1 = H + quad_term2;
             terms = H1.terms;
             testCase.assertLength(terms,3)
             testCase.assertEqual(terms{3}.variable_1.id, x2.id)
             testCase.assertEqual(terms{3}.variable_2.id, x2.id)
             
             H2 = lin_term2 + H;
             terms = H2.terms;
             testCase.assertLength(terms,3)
             testCase.assertEqual(terms{1}.variable_1.id, p1.id)
             
             
             H = Hamiltonian(lin_term1) + Hamiltonian(lin_term1);  
             testCase.assertLength(H.terms, 1);
             
             
             Hnegated = -(Hamiltonian(lin_term1) + Hamiltonian(quad_term1));
             testCase.assertLength(Hnegated.terms, 2);
             testCase.assertEqual(Hnegated.terms{1}.scaling, -scaling_type);
             testCase.assertEqual(Hnegated.terms{2}.scaling, -scaling_type);
             
             
             H = -1*lin_term1 + 2*quad_term1;
             Hzero = H - H;
             testCase.assertLength(Hzero.terms, 0);
             
             
             Hdouble = H + H;
             testCase.assertLength(Hdouble.terms, 2);
             testCase.assertEqual(Hdouble.terms{1}.scaling, 2*H.terms{1}.scaling);
             testCase.assertEqual(Hdouble.terms{2}.scaling, 2*H.terms{2}.scaling);
             
             
             % print string
             H = lin_term1 + 2*lin_term2 + 3*quad_term1 - 4*quad_term2;
             H.str()
             res = str2sym(H.str())
             symvar(res)
             
         end
         
    end
    
end

