classdef TimeTests < matlab.unittest.TestCase
   
    methods (Test)
        
        function PositionAndMomentumTests(testCase)
            
            system_label = 'atom';
            
            osc = OscillatorSystem(system_label);
            
            % Check system labels
            testCase.verifyEqual(strcmp(osc.x.system_label, osc.p.system_label), true)
            testCase.verifyEqual(strcmp(osc.x.system_label, osc.system_label),   true)
            
            % Check if gaussian
            testCase.verifyEqual(osc.x.is_gaussian, true)
            testCase.verifyEqual(osc.p.is_gaussian, true)
            
            % Check complementarity
            testCase.verifyEqual(strcmp(osc.x.id, osc.p.complement.id), true)
            testCase.verifyEqual(strcmp(osc.p.id, osc.x.complement.id), true)
            
            testCase.verifyEqual(strcmp(osc.x.id, osc.x.complement.complement.id), true)
            testCase.verifyEqual(strcmp(osc.p.id, osc.p.complement.complement.id), true)
            
            
            
        end
        
    end
    
end