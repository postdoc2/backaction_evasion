classdef TimeTests < matlab.unittest.TestCase

    properties
        time
        
        ts 
        nt
        T
        dt
    end
    
    
    methods(TestClassSetup)
       
        function setup(tc)
            tc.ts = linspace(0,1,101);
            tc.nt = numel(tc.ts);
            tc.T  = tc.ts(end);
            tc.dt = tc.ts(2)-tc.ts(1);
            tc.time = Time('ts', tc.ts);
            
        end
        
        
    end
        
    
    methods (Test)
        
        function TimeConstructor(tc)

%             ts = linspace(0,1,101);
%             nt = numel(ts);
%             T = ts(end);
%             dt = ts(2)-ts(1);
            
            tc.verifyError(@() Time(),            'time:insufficientparams')
            tc.verifyError(@() Time('dt', tc.dt), 'time:insufficientparams')
            tc.verifyError(@() Time('T', tc.T),   'time:insufficientparams')
            tc.verifyError(@() Time('nt', tc.nt), 'time:insufficientparams')
            
            tc.verifyError(@() Time('ts', 1:-1), 'time:tsemptybutpassed')
            
            
            test_constructor(Time('ts', tc.ts))
            
            test_constructor(Time('T', tc.T, 'dt', tc.dt))
            test_constructor(Time('dt', tc.dt, 'T', tc.T))
            
            test_constructor(Time('T', tc.T, 'nt', tc.nt))
            test_constructor(Time('nt', tc.nt, 'T', tc.T))
            
            test_constructor(Time('dt', tc.dt, 'nt', tc.nt))
            test_constructor(Time('nt', tc.nt, 'dt', tc.dt))
            
            function test_constructor(time)
                
                tc.verifyEqual(time.ts, tc.ts, 'AbsTol', 1e-12)
                tc.verifyEqual(time.nt, tc.nt)
                tc.verifyEqual(time.dt, tc.dt, 'AbsTol', 1e-12)
                tc.verifyEqual(time.T, tc.T,   'AbsTol', 1e-12)

            end
            
            
            
        end
       
        
        function TimeFrom(tc)
            
            time = Time('ts', 0:10);
            
            tc.verifyError(@() time.from(6,5), 'time:from:tminlargerthantmax')
            
            tc.verifyWarning(@() time.from(-10, 5), 'time:from:tminlowerbound')
            tc.verifyWarning(@() time.from(5, 15),  'time:from:tmaxupperbound')
            
            [time_, idxs] = time.from(0, 10);
            
            %tc.verifyWarning(@() time.from(9*tc.T, 10*tc.T), 'time:from:tminupperbound')
            
            
        end

    end
    
end