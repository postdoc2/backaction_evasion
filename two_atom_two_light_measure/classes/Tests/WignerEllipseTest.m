classdef WignerEllipseTest < matlab.unittest.TestCase

    properties
        r
    end
    
    
    methods(TestClassSetup)
       
        function setup(tc)
            constants
            
            T = 10*ms;
            dt = 1.0e-6;
            time = Time('ts', 0:dt:T);

            k =  0.1*sqrt(1.83e6); 
            w =  0.1 * w_1ms;


            fx0  = 4;
            cx  = 150;
            gammabx = 0e2*s^(-1);
            sigmabx = 0*1e1*s^(-1);

            fp0  = 4;
            cp  = 150;
            gammabp = 0e2*s^(-1);
            sigmabp = 0*1e1*s^(-1);

            m0 = [0; 0; 0; 0; 0; 0];

            A0 = diag([1, 1, 1, 1, 2*sigmabx^2/gammabx, 2*sigmabp^2/gammabp]); 
            if A0(5,5) < 1 || isnan(A0(5,5)) || isinf(A0(5,5)); A0(5,5) = 1; end
            if A0(6,6) < 1 || isnan(A0(6,6)) || isinf(A0(6,6)); A0(6,6) = 1; end

            OU_seed = 1; % 0: random 
            measurement_seed_num = 1; % 0: random 
            
            E_covar_initial_val = 10000;
            OUx = OrnsteinUhlenbeckProcess(time, gammabx, 0, sigmabx).Ito_SDE(fx0, 'rngseed', OU_seed);
            OUp = OrnsteinUhlenbeckProcess(time, gammabp, 0, sigmabp).Ito_SDE(fp0, 'rngseed', OU_seed+1);

            
            p = struct('w1',   w, 'w2', -  w, 'k11', k, 'k21', k, 'k12',   k, 'k22', -k);
            
            tc.r = analytical_forms( propagate_with_F(A0, m0, p.k11, p.k12, p.k21, p.k22, p.w1, p.w2, cx, OUx, cp, OUp, 1*measurement_seed_num)); 
            
        end
        
        
    end
        
    
    methods (Test)
        
        function WignerEllipse(tc)

            ellipse_contour_value_normalized = 0.73; 
            
            r = tc.r;
            idx = 2000;

            covar = r.gs([1,2], [1,2], idx);
            m = r.ys([1,2], idx);
            x1 = linspace(-10,10,1000);
            x2 = x1;

            [X1,X2] = meshgrid(x1,x2);
            X = [X1(:) X2(:)];
            y = mvnpdf(X,m',covar);
            y = reshape(y,length(x2),length(x1));
            
            y_norm = y / max(max(y));
            figure
            imagesc(x1,x2, y_norm);
            colorbar
            hold on
            
            W = Wigner_ellipses(r, 'A1', ellipse_contour_value_normalized, 100);
            
            
            plot(W.ellipses{idx}{:},'displayname', ['Wigner ellipse'])
            
            plot(sqrt(W.scale) * [0, W.major_axis_vectors(1, idx)] + W.centers(1,idx), sqrt(W.scale) * [0, W.major_axis_vectors(2, idx)] + W.centers(2,idx),'displayname','major axis')
            plot(sqrt(W.scale) * [0, W.minor_axis_vectors(1, idx)] + W.centers(1,idx), sqrt(W.scale) * [0, W.minor_axis_vectors(2, idx)] + W.centers(2,idx),'displayname','minor axis')
            
            angles = linspace(0, W.major_axis_tilt_angles(idx), 50);
            s = 1;
            plot(s*cos(angles) +  W.centers(1,idx), s*sin(angles) + W.centers(2,idx) ,'k','displayname','tilt angle')
            
            
            contour(x1, x2, y_norm,[ellipse_contour_value_normalized, ellipse_contour_value_normalized], 'k--','displayname','contour line')
            
            title(['Compare normalized gaussian surface contour at height '  num2str(ellipse_contour_value_normalized,2) ' to my calc.'])
           
            leg = legend;
            
            %figure
            %plot(abs(W.major_axis_tilt_angles))
        end
   
        
        
        function WignerEllipseEvolution(tc)
           
            ellipse_contour_value_normalized = 0.5; 
            
            r = tc.r;
            
            W = Wigner_ellipses(r, 'rel', ellipse_contour_value_normalized, 100);
            
            figure
            x = W.ellipses{1}{1};
            y = W.ellipses{1}{2};
            p = plot(x, y, 'displayname', ['Wigner ellipse']);
           
            xlim([-5,5])
            ylim([-5,5])
            
            for i = 1:r.nt
                set(p,'xdata', W.ellipses{i}{1}, 'ydata', W.ellipses{i}{2})
                pause(0.01)
            end
            
            
            
            
        end
    end
    
    
    
    
end