
clc
%%

addpath('../Variables')
testCase1 = VariablesTests;
testCase1.run


%%


addpath('../Hamiltonian')
testCase2 = HamiltonianTests;
testCase2.run

%result = run(testCase2, 'HamiltonianTest')

%% 
addpath('../')
testCase3 = OrnsteinUhlenbeckTest;
testCase3.run