classdef OrnsteinUhlenbeckTest < matlab.unittest.TestCase
    
    methods (Test)
        
        function OU_Test(testCase)
            s  = 1;
            ms = 1e-3;
            mm = 1e-3;
            GHz = 1e9;
            nm = 1e-9;
            pT = 1e-12;
            h    = 6.626070040e-34; % [J*s]
            hbar = h/(2*pi);

            T  = 5*ms;
            ntimesteps = 1e4;
            ts = linspace(0,T,ntimesteps);
            dt = ts(2)-ts(1);

            f = 1;
            gammab = f*1e2*s^(-1);
            sigmab = f*2e3*pT^2*s^(-1);


            X0 = 0*ones(500,1)*pT;

            OU = OrnsteinUhlenbeckProcess(Time('ts', ts), gammab, 0, sigmab)
            OU = OU.Ito_SDE(X0,'rngseed', 2)

            figure
            %plot(ts/ms,OU.X/pT) 


            meanXs = mean(OU.X,1);
            stdXs  = std(OU.X,0,1);

            hold on
            plot(ts/ms,meanXs/pT)
            plot(ts/ms,(meanXs + stdXs)/pT)
            plot(ts/ms,(meanXs - stdXs)/pT)
            plot(ts/ms,(meanXs + 2*stdXs)/pT)
            plot(ts/ms,(meanXs - 2*stdXs)/pT)
            plot(ts/ms,(meanXs + 3*stdXs)/pT)
            plot(ts/ms,(meanXs - 3*stdXs)/pT)


            if gammab>0
                plot(ts/ms, X0(1)*exp(-gammab*ts)/pT,'--','linewidth',3)
                yline(+sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
                yline(-sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
                yline(+2*sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
                yline(-2*sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
                yline(+3*sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
                yline(-3*sqrt(sigmab/(2*gammab))/pT,'linewidth',3)
            end

        end 
    end

end