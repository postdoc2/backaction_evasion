classdef OrnsteinUhlenbeckProcess < StochasticProcess
    %ORNSTEINUHLENBECKPROCESS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
       
        drift_scaling
        drift_offset
        diffusion_scaling
        
    end
    
    
    methods
        
        function obj = OrnsteinUhlenbeckProcess(ts, drift_scaling, drift_offset, diffusion_scaling)
            
            obj = obj@StochasticProcess(ts);
            
            obj.drift_scaling       = drift_scaling;
            obj.drift_offset        = drift_offset;
            obj.diffusion_scaling   = diffusion_scaling;
            
        end
        
    end
    
    
    methods 
       
        function val = drift_function(obj, ~ , X)
            val = obj.drift_scaling * (obj.drift_offset - X);  
        end
        
        function val =  diffusion_function(obj, ~, ~)
            val = sqrt(obj.diffusion_scaling);
        end
        
        function val = diffusion_function_derivative(~, ~, ~)
            val = 0;
        end
    end
end

