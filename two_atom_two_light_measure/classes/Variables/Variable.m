classdef (Abstract) Variable < handle
    
    properties (SetAccess = protected)
        is_gaussian
        label
        id
    end
    
    methods
        function obj = Variable(label, is_gaussian)
            obj.label = label;
            obj.is_gaussian = is_gaussian;
            
            obj.id = DataHash(rand(5));
        end
        
    
    end
end

