classdef (Abstract) QuantumVariable < Variable
   
    properties (SetAccess = {?OscillatorSystem})
        system_label
        complement
    end
    
    properties (SetAccess = private)
        full_label
    end
    
    methods
        
        function obj = QuantumVariable(label, system_label)
            
            is_gaussian = true;
            obj@Variable(label, is_gaussian)
            
            obj.system_label = system_label;     
            
            if isempty(system_label)
                obj.full_label = label;
            else
                obj.full_label = [label '_' system_label];
            end
            
            
        end
        
        function r = mtimes(left, right)
            
            if     (isa(left, 'double') || isa(left, 'sym')) && isa(right, 'QuantumVariable')
                
                r = LinearTerm(left, right);
                
            elseif isa(left, 'QuantumVariable') && (isa(right, 'double') || isa(right, 'sym'))
                
                r = LinearTerm(right, left);
                
            elseif isa(left, 'QuantumVariable') && isa(right, 'QuantumVariable')
                
                r = QuadraticTerm(1, left, right);
                
            else
               
                error(['No matching multiplication between types ' class(left) ' and ' class(right)])
                
            end
            
        end
        
        function r = mpower(base, exponent)
           
            
            if isa(exponent, 'double') || isa(exponent, 'sym')
                
                if      exponent == 1 || isequal(exponent, sym(1))
                    r = LinearTerm(1, base);
                elseif  exponent == 2 || isequal(exponent, sym(2))
                    r = QuadraticTerm(1, base, base);
                else
                    error('Exponent must be either 1 or 2!')
                end
            else
                 error(['No matching power between types ' class(base) ' and ' class(exponent)])
            end
            
        end
        
        function r = sym(obj)
           
            r = sym(obj.full_label, 'real');
            
        end
        
        function r = plus(left, right)
           
            if      isa(left, 'QuantumVariable') && isa(right, 'QuantumVariable')
                
                r = LinearTerm(1, left) + LinearTerm(1,right);    
                
            elseif  isa(left, 'QuantumVariable') && isa(right, 'Term') 
                
                r = LinearTerm(1, left) + right;
            
            else
                
                error(['No matching + between types ' class(left) ' and ' class(right)])
                
            end
            
        end
       
        
    end
    
end