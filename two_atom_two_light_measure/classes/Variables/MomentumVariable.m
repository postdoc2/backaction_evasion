classdef (InferiorClasses = {?sym}) MomentumVariable < QuantumVariable
   
    
    methods
       
        function obj = MomentumVariable(system_label)
           
            obj@QuantumVariable('p', system_label);
            
        end
        
        
        
    end
    
end