classdef (InferiorClasses = {?sym}) PositionVariable < QuantumVariable
   
    
    methods
       
        function obj = PositionVariable(system_label)
           
            obj@QuantumVariable('x', system_label);
            
        end
        
        
       
    end
    
end