classdef OscillatorSystem < handle
    
    properties (SetAccess = private)
        
        x
        p
        system_label
        
    end

    
    methods 
        
        function obj = OscillatorSystem(system_label)
           
            obj.x = PositionVariable(system_label);
            obj.p = MomentumVariable(system_label);
            
            obj.x.complement = obj.p;
            obj.p.complement = obj.x;
            
            obj.system_label = system_label;
            
            
        end
        
    end
end
