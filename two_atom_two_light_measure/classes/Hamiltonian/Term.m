classdef (Abstract) Term < handle
    
    properties 
        scaling
    end
    
    
    
    methods
    
        function obj = Term(scaling)
            
            if isa(scaling,'char')
                scaling = sym(scaling);
            end
            
            if ~( isa(scaling, 'sym') || isa(scaling, 'double') )
                error('scaling must be a double or sym!')
            end
            obj.scaling = scaling;
            
        end
        
        function r=H(obj)
           
            r = Hamiltonian(obj);
            
        end 
        
        
        function r = str(obj)
            
            
            if      isa(obj.scaling, 'double')
                r = num2str(obj.scaling);
            elseif  isa(obj.scaling, 'sym')
                r = char(obj.scaling);
            end
            
            
        end
    end
        
       
    methods % overloads
        
        function r = plus(left, right)
            if isa(left,'Term') && isa(right,'Term')
                r = Hamiltonian(left, right);
            else
                error(['No matching plus between types ' class(left) ' and ' class(right)]) 
            end
        end
        
        
                
    end
end

