classdef (InferiorClasses = {?sym}) LinearTerm < Term
   
    properties (SetAccess = protected)
        variable_1
        id
    end
    
    methods 
        
        function obj = LinearTerm(scaling, variable_1)
           
            obj@Term(scaling)
            
            if ~isa(variable_1, 'QuantumVariable') 
                error('variable_1 must be a QuantumVariable!')
            end
            
            
            obj.variable_1 = variable_1;
            obj.id = DataHash( obj.variable_1.id );
            
        end
        
        
        function r = str(obj)
           
            r = [str@Term(obj) '*' obj.variable_1.full_label];
            
        end
        

    end
    
    
    methods % overloads
        
        function r = mtimes(left, right)
            
            if (isa(left, 'double') || isa(left,'sym')) && isa(right, 'LinearTerm')
                
                r = LinearTerm(left*right.scaling, right.variable_1);

            elseif isa(left, 'LinearTerm') && (isa(right, 'double') || isa(right,'sym'))
                
                r = LinearTerm(right*left.scaling, left.variable_1);
            
            elseif isa(left, 'LinearTerm') && isa(right, 'LinearTerm')
                r = QuadraticTerm(left.scaling * right.scaling, left.variable_1, right.variable_1);
            
            elseif isa(left, 'LinearTerm') && isa(right, 'QuantumVariable')
                
                r = QuadraticTerm(left.scaling, left.variable_1, right);
                
            elseif isa(left, 'QuantumVariable') && isa(right, 'LinearTerm') 
                
                r = QuadraticTerm(right.scaling, right.variable_1, left);
                
            elseif (isa(left, 'LinearTerm') && isa(right, 'QuadraticTerm')) || ( isa(left, 'QuadraticTerm') &&  isa(right, 'LinearTerm') ) || ( isa(left, 'QuadraticTerm') &&  isa(right, 'Quadratic') ) 
                error('Only linear or quadratic terms allowed!');
            else
                
                error(['No matching multiplication between types ' class(left) ' and ' class(right)])
                
            end
        end
        
        
        function r = mpower(base, exponent)
            
            if isa(exponent, 'double') || isa(exponent, 'sym')
                
                if      exponent == 1 || isequal(exponent, sym(1))
                    r = LinearTerm(1, base.variable_1);
                elseif  exponent == 2 || isequal(exponent, sym(2))
                    r = QuadraticTerm(1, base.variable_1, base.variable_1);
                else
                    error('Exponent must be either 1 or 2!')
                end
            else
                 error(['No matching power between types ' class(base) ' and ' class(exponent)])
            end
            
        end
        
        function r = sym(obj)
           
            r = obj.scaling * sym(obj.variable_1);
            
        end
        
        
        function r = plus(left, right)
            
            if      isa(left, 'LinearTerm') && isa(right, 'LinearTerm')
                if strcmp(left.variable_1.id, right.variable_1.id)
                   r = LinearTerm(left.scaling + right.scaling, left.variable_1);
                else
                   r = Hamiltonian(left, right);
                end
            
            elseif  isa(left, 'LinearTerm') && isa(right, 'QuantumVariable') 
                
                r = left + LinearTerm(1, right);
                
            else
                error(['No matching plus between types ' class(left) ' and ' class(right)])
            end
        end
        
        function r = minus(left, right)
            
            r = plus(left, -right);
            
        end
        
        function r = uminus(obj)
           r = LinearTerm(-obj.scaling, obj.variable_1);
        end
        
        

    end
    
end