classdef (InferiorClasses = {?sym, ?LinearTerm}) QuadraticTerm < LinearTerm
   
    properties (SetAccess = private)
        variable_2
    end
    
    methods 
        
        function obj = QuadraticTerm(scaling, variable_1, variable_2)
            
            if ~isa(variable_2, 'QuantumVariable') 
                error('variable_2 must be a QuantumVariable!')
            end
            
            
            obj@LinearTerm(scaling, variable_1)
            obj.variable_2 = variable_2;
            
            obj.id = DataHash( obj.variable_1.id + obj.variable_2.id);
            
            
        end
    
        
        function r = str(obj)
           
            if strcmp(obj.variable_1.id, obj.variable_2.id)
                r = [str@LinearTerm(obj) '^2'];
            else
                r = [str@LinearTerm(obj) '*' obj.variable_2.full_label];
            end
            
        end        
        
    end
    
    methods % overloads
        function r = mtimes(left, right)
           
            if      (isa(left, 'double') || isa(left, 'sym')) && isa(right, 'QuadraticTerm')
                r = QuadraticTerm(left * right.scaling, right.variable_1, right.variable_2);
            elseif  isa(left, 'QuadraticTerm') && (isa(right, 'double') || isa(right, 'sym'))
                r = QuadraticTerm(right * left.scaling, left.variable_1, left.variable_2);
            else
                 error(['No matching multiplication between types ' class(left) ' and ' class(right)])
            end
            
        end
        
        function r = plus(left, right)
            
            if      isa(left, 'QuadraticTerm') && isa(right, 'QuadraticTerm')
                if strcmp(left.variable_1.id, right.variable_1.id) && strcmp(left.variable_2.id, right.variable_2.id)
                   r = QuadraticTerm(left.scaling + right.scaling, left.variable_1, left.variable_2 );
                else
                   r = Hamiltonian(left, right);
                end
            
            elseif (isa(left,'QuadraticTerm') && isa(right, 'LinearTerm')) || (isa(left,'LinearTerm') && isa(right, 'QuadraticTerm'))
                r = Hamiltonian(left, right);
            
            elseif  isa(left, 'QuadraticTerm') && isa(right, 'QuantumVariable') 
                
                r = left + LinearTerm(1, right);
                
                
            else
                error(['No matching plus between types ' class(left) ' and ' class(right)])
            end
        end
        
        function r = uminus(obj)
           r = QuadraticTerm(-obj.scaling, obj.variable_1, obj.variable_2);
        end
        
        %function r = sym(obj)
           
         %   r = obj.scaling * sym(obj.variable_1) * sym(upper(obj.variable_2.full_label),'real'); % makes sure right is always correctly right in print by captilizing
            
        %end
    end
    
    
    
end