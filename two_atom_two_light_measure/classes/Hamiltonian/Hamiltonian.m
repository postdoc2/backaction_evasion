classdef (InferiorClasses = {?LinearTerm, ?QuadraticTerm}) Hamiltonian < handle
   
    properties (SetAccess = {?LinearTerm, ?QuadraticTerm}) 
        terms
    end
    
    methods
       
        function obj = Hamiltonian(varargin)
            
            
            if length(varargin) == 1 && isa(varargin{1}, 'cell')
                obj.terms = varargin{1};                
            else
                obj.terms = varargin; 
            end
            
            for i = 1:numel(obj.terms)
                    if ~isa(obj.terms{i}, 'Term')
                       error(['Element at index ' num2str(i) ' was not of type ''Term''']) 
                    end
            end
            
            combine_like_terms(obj);
        
        end
        
        
        function combine_like_terms(obj)
            
            all_terms = obj.terms;
            
            term_ids = cellfun(@(term) term.id, all_terms,  'UniformOutput' , false);
            unique_term_ids = unique(term_ids, 'stable'); % stable retains ordering
            n_unique = numel(unique_term_ids);
            
            if n_unique == numel(all_terms) % if there are no repeting id's
                return
            end
            
            new_terms = {}; % cell(1, n_unique);
            
            for i = 1:n_unique
                same_ids_i = strcmp(term_ids, unique_term_ids{i});
                
                same_terms = all_terms(same_ids_i);
                
                same_terms_sum = same_terms{1};
                for j = 2:numel(same_terms)
                    same_terms_sum = same_terms_sum + same_terms{j};
                end
                
                if same_terms_sum.scaling ~= 0
                    new_terms{end+1} = same_terms_sum;
                end
                
            end
            
            obj.terms = new_terms;
        end
       
        
        
        function r=str(obj)
           
            r = '';
            for i = 1:numel(obj.terms)          
                if i == 1
                    r = [obj.terms{i}.str()];
                else
                    
                    term_str = obj.terms{i}.str();
                    
                    term_is_negative = strcmp(term_str(1), '-');
                    
                    if term_is_negative
                        r = [r ' - ' term_str(2:end)];
                    else
                        r = [r ' + ' term_str];
                    end
                    
                    
                end
            end
            
        end
        
    end
    
    
    
    methods % Overloads
        
        function r = plus(left, right)
           
            if      isa(left, 'Hamiltonian') && isa(right, 'Hamiltonian')
                r = Hamiltonian([left.terms, right.terms]);
            elseif  isa(left, 'Hamiltonian') && isa(right, 'Term') 
                r = Hamiltonian(left.terms{:}, right);
            elseif  isa(left, 'Term')  && isa(right, 'Hamiltonian') 
                r = Hamiltonian(left, right.terms{:});
            else
                error(['No matching plus between types ' class(left) ' and ' class(right)])
            end
            
        end
        
        function r = uminus(obj)
           
            old_terms = obj.terms;
            
            negated_terms = cell(1, numel(old_terms));
            
            for i = 1:numel(old_terms)
                negated_terms{i} = -old_terms{i};
            end
            
            r = Hamiltonian(negated_terms{:});
            
        end
        
        function r = minus(left, right)
            
            r = plus(left, -right);
            
        end
        
        
        function r = mtimes(left, right)
           
            if      (isa(left, 'double') || isa(left, 'sym')) && isa(right, 'Hamiltonian')
                    factor = left;
                    H = right;
            elseif  isa(left, 'Hamiltonian') && (isa(right, 'double') || isa(right, 'sym'))
                    factor = right;
                    H = left;
            else
                error(['No matching multiplication between types ' class(left) ' and ' class(right)])
            end
            
            
            scaled_terms = H.terms;
            
            for i = 1:numel(scaled_terms)
                scaled_terms{i} = factor*scaled_terms{i};
            end
            
            r = Hamiltonian(scaled_terms);
        end
        
    end
    
end