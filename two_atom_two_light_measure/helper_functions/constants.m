%% constants
s  = 1;
ms = 1e-3;
mus = 1e-6;
ns = 1e-9;
mm = 1e-3;
GHz = 1e9;
nm = 1e-9;
pT = 1e-12;
h    = 6.626070040e-34; % [J*s]
hbar = h/(2*pi);


w_1ns = (2*pi/(1*ns));
w_1mus = (2*pi/(1*mus));
w_1ms = (2*pi/(1*ms));
w_5ms = 5 * w_1ms;
w_10ms = 10 * w_1ms;

w_01ms = 0.1 * w_1ms;
