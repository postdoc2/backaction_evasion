classdef GaussianPropagator 
   
    properties
       
        S
        mdim
        ndim
        dim 
        L
        v
        pi_mat
        
        time
        gs
        ys
        measurement_record
    end
    
    methods 
       
        function obj = GaussianPropagator(mdim, ndim, S, L, v, pi_mat, A0, m0, time)
            
            obj.mdim = mdim;
            obj.ndim = ndim;
            obj.dim = mdim + ndim;
            obj.S = S;
            obj.L = L;
            obj.v = v;
            obj.pi_mat = pi_mat;
        
            obj.time = time;
        end
        
        function obj = propagate(obj)
            
            
            
            if ~isequal(A0', A0)
               disp('A0 was not symmetric - symmetrizing with upper triangle...')  
               A0 = triu(A0) + triu(A0)' - diag(diag(A0))
            end            
            B0 = eye(obj.ndim, obj.ndim);
            C0 = zeros(obj.mdim, obj.ndim);
            n0 = zeros(obj.ndim,1);
            
            g = [A0,  C0;
                 C0', B0];
             
            y = [m0; n0];
            
            obj.ys = zeros([size(y0), nt]);
            obj.gs = zeros([size(g0), nt]);
            
            obj.ys(:,1,1) = y;
            obj.gs(:,:,1) = g;
            
            for i = 1: obj.time.nt - 1
                
                if isa(obj.v, 'function_handle')
                    v = obj.v(i);
                else
                    v = obj.v;
                end
                
                y = obj.S * y + v;
                g = obj.S * g * obj.S' + obj.L;
                
                m = y(1:obj.mdim,:);
                n = y(obj.mdim + 1:end, :);
                
                A = g(1:obj.mdim, 1:obj.mdim);
                B = g(obj.mdim + 1:end, obj.mdim + 1:end);
                C = g(1:obj.mdim, obj.mdim+1:end);
                
                mpinv = pinv(obj.pi_mat * B * obj.pi_mat);
                
                A = A - C * mpinv * C;
                m = m + C * mpinv * (( measurement_record(:,i) - n ).* diag(obj.pi_mat));
                
                y = [m; n0];
                g = [A,   C0;
                     C0', B0]; 
                
                 obj.ys(:,1,i+1) = y;
                 obj.gs(:,:,i+1) = g;
            end
                
            
        end
    end
    
end