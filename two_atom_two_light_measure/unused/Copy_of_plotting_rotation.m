function plotting_rotation(r, varargin)
%%%

p = inputParser(varargin);

chkBool = @(x) isnumeric(x) && isscalar(x) && (x == 0 || x == 1);

p.addParameter('plot_A1',  0, chkBool)
p.addParameter('plot_A2',  0, chkBool)
p.addParameter('plot_rel', 1, chkBool)

%%%
angles = linspace(0,2*pi,100);

hold on
ellipse_v1 = plot(nan);
ellipse_v2 = plot(nan);
ellipse    = plot(nan,'linewidth',3);
mean_dot   = plot(nan,'.','markersize',20);
dot_trace  = plot(nan,'k','linewidth',1);
ax_walk = gca;

axis equal

xlim([-5,5])
ylim([-5,5])

opt = 3;

if opt == 1
    xlabel('$x_1$')
    ylabel('$p_1$')
    t_string = '$\Delta x_1 \Delta p_1 = $';
    mn = [r.m1; r.m2];
    the_title = title(ax_walk, [t_string]);
elseif opt == 2
    xlabel('$x_2$')
    ylabel('$p_2$')
    mn = [r.m3; r.m4];
    t_string = '$\Delta x_2 \Delta p_2 = $';
    the_title = title(ax_walk, [t_string]);
else
    xlabel('$x_1-x_2$')
    ylabel('$p_1+p_2$')
    
    t_string = '$\Delta (x_1-x_2) \Delta (p_1+p_2) = $';
    
    mn = [r.m1 - r.m3; r.m2 + r.m4];
    the_title = title(ax_walk, [t_string]);
end

for idx = 1:1:r.nt
    
    if opt == 1
        covar = 0.5*r.gs([1,2],[1,2], idx);
    elseif opt == 2
        covar = 0.5*r.gs([3,4],[3,4], idx);
    else
        covar = zeros(2,2);
        
        covar(1,1) = r.relXVar(idx);
        covar(2,2) = r.sumPVar(idx);
        covar(1,2) = r.relXsumPcovar(idx);
    end
    
    covar = triu(covar) + triu(covar)' - diag(diag(covar));

    [vecs, vals] = eig(covar);

    v1 = sqrt(vals(1,1))*vecs(:,1);
    v2 = sqrt(vals(2,2))*vecs(:,2);

    set(ellipse_v1, 'xdata', [0, v1(1)] + mn(1,idx) ,'ydata',[0, v1(2)] + mn(2,idx));
    set(ellipse_v2, 'xdata', [0, v2(1)] + mn(1,idx) ,'ydata',[0, v2(2)] + mn(2,idx));
    %plot([0, v1(1)], [0 v1(2)])
    %hold on
    %plot([0, v2(1)], [0 v2(2)])
    %plot()
    
    p_vec = mn(:,idx) + v1*cos(angles) + v2*sin(angles);

    set(ellipse, 'xdata', p_vec(1,:), 'ydata', p_vec(2,:));
    set(mean_dot,'xdata', mn(1,idx), 'ydata', mn(2,idx));
    set(dot_trace,'xdata', mn(1,1:idx), 'ydata', mn(2,1:idx));
    
    %plot(p(1,:), p(2,:))
    
    %title(ax_walk, [t_string num2str(sqrt(covar(1,1)*covar(2,2)),3)])
    the_title.String = [t_string num2str(sqrt(covar(1,1)*covar(2,2)),3)];
    if idx == 1
        pause()
    end
   % pause()
    pause(0.01)
end

%r.varxA1xA2(end)
%r.gs(:,:, idx)
end