function process = Ito_SDE(initial_value, ts, W, drift_func, diffusion_func, diffusion_deriv, euler)

nt = numel(ts);
nsamples = size(W,1);
dt = ts(2)-ts(1);

process = zeros(nsamples,nt);
process(:,1) = initial_value;

for i = 2:nt
    
    dW = W(:,i)-W(:,i-1);
    prev = process(:,i-1);
    t = ts(i);
    
    if euler
        process(:,i) = prev + drift_func(t,prev)*dt + diffusion_func(t,prev).*dW;
    else
        process(:,i) =  prev  ... 
                 +  drift_func(t,prev)*dt ...
                 +  diffusion_func(t,prev).*dW  ...
                 +  0.5*diffusion_func(t,prev).*diffusion_deriv(t,prev).*((dW).^2 - dt);
    end
end

end