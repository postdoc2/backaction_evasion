classdef MeasurementRecord 
   
    properties (SetAccess = private)
        measured_channels
        measurement_records
    end
    
    methods
        
        function obj = MeasurementRecord(time, ndim, measured_channels, rngseed)
            % channels is a vector of the measured observables in the n vector
            obj.measured_channels = measured_channels;
            
            obj.measurement_records = zeros(ndim, time.nt-1);
            %prev_rng = rng;
            %rng(rngseed);
            %obj.measurement_records = sqrt(0.5) * randn(numel(channels),time.nt-1);
            %rng(prev_rng);
        end
        
        
    end
end