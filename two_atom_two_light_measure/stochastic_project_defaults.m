[this_path, ~, ~] = fileparts(mfilename('fullpath'));

project_root = this_path;
ignore_dirs = {'resources'};
addpath_recurse(project_root, ignore_dirs)

fig_root = [project_root '/figs_in_paper'];

datetime.setDefaultFormats('default','yyyy-MM-dd_HH-mm-ss')

%%

colors = brewermap(64,'YlGnBu');
colors = colors([20 30 40 55 64],:);
%colors = colors(round(linspace(20, 64, 5)),:);
colors = colors([3 1 5 2 4 ], :);

orange = [255,130,50]; orange = orange/255;
colors = [colors; orange];

plotColors = 0;

set(groot,'defaultAxesColorOrder',colors);
if plotColors
    figure
    for i = 1:numel(colors)/3
    plot([0, 1], [i i],'linewidth',5); hold on;
    end
    ylim([-1, size(colors,1)+1])
end


%%

set(groot, 'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');  
set(groot, ...
   'DefaultTextInterpreter', 'LaTeX', ...
   'DefaultAxesTickLabelInterpreter', 'LaTeX', ...
   'DefaultAxesFontName', 'LaTeX', ...
   'DefaultLegendInterpreter', 'LaTeX')

%%
col_width = 245;

%%

axisFontSize = 10;
legendFontSize = 10;


set(0, 'DefaultAxesFontSize',axisFontSize)
set(0, 'DefaultLegendFontSize',legendFontSize)
set(0, 'DefaultAxesLineWidth', 1.0)