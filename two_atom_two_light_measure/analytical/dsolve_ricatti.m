function [A_solved, W_solved, U_solved] = dsolve_ricatti(A0, F, G, D, E, print_matrices)

if print_matrices
    A0 = A0
    F = F
    G = G
    D = D
    E = E 
end

W0 = A0;
ndim = size(W0,1);

U0 = eye(ndim,ndim);
v0 = [
        reshape(transpose(W0), [ndim^2,1]);
        reshape(transpose(U0), [ndim^2,1]);
     ];
%W = sym('W_%d%d', [4 4],'real')  
%U = sym('U_%d%d', [4 4],'real')  

%G = sym('G_%d%d', [ndim ndim],'real')  
%F = sym('F_%d%d', [ndim ndim],'real')  



eval(['syms w(t) u(t) [' num2str(ndim) ' ' num2str(ndim) ']']);
w;
u;

if ndim == 1 % if ndim == 1, the eval'd w(t) above has elements wi, i.e. no second idx wi_j 
    str_idx_from_idx = @(i,j) [num2str(i)]; 
else
    str_idx_from_idx = @(i,j) [num2str(i) '_' num2str(j)];
end

vstr = '';
for i = 1:ndim
    for j = 1:ndim
        wij = ['w' str_idx_from_idx(i,j)];
        vstr = [vstr wij '; '];
    end
end
for i = 1:ndim
    for j = 1:ndim
        uij = ['u' str_idx_from_idx(i,j)];
        vstr = [vstr uij '; '];
    end
end

v = eval(['[' vstr ']']);

vdot = [
            reshape(transpose(-D*w + G*u), [ndim^2,1]);
            reshape(transpose(+F*w + E*u), [ndim^2,1]);
       ];

odes  = diff(v) == vdot;
conds =    v(0) == v0; 

S = dsolve(odes,conds);

W_solved_str = '';
U_solved_str = '';
for i = 1:ndim
    for j = 1:ndim
        W_solved_str = [W_solved_str ' S.w' str_idx_from_idx(i,j)];
        U_solved_str = [U_solved_str ' S.u' str_idx_from_idx(i,j)];
    end
    W_solved_str = [W_solved_str '; '];
    U_solved_str = [U_solved_str '; '];
end
W_solved = eval(['[' W_solved_str ']']);
U_solved = eval(['[' U_solved_str ']']);

A_solved = simplify(W_solved / U_solved);
%S.w1_1
end
