function [G, D, E, F] = RicattiMatricesExtractor(A, Adot)

% Extracts the matrices for:
% Adot = G - (D*A + A*E) - A*F*A

% Reqs:
% Adot must have had dt divided out 
% Adot must consist of sym elements 'A_%d%d' ---- maybe change to A_%d_%d later!

mdim = size(A,1);

A0 = sym('a0_%d%d', [mdim mdim],'real');

%% Locate constant, linear, quadratic term

syms dummy
Adot_cst = subs(Adot, A, 0*A);
Adot_lin = subs(subs(expand(subs(Adot - Adot_cst, A, A*dummy)),dummy^2,0), dummy, 1);
Adot_qua = simplify(Adot - Adot_cst - Adot_lin);

%% G and F are fairly straightforward

G = Adot_cst;

% For F: invoke symmetry of A 
lower = {};
upper = {};
for i = 1:mdim
    for j = 1:i-1        
        lower{end+1} = ['A_' num2str(i)  num2str(j)];
        upper{end+1} = ['A_' num2str(j)  num2str(i)];
    end
end
F = -simplify(subs(A \ Adot_qua / A, lower, upper));

%%

D = sym('D_%d%d', size(A),'real');
E = sym('E_%d%d', size(A),'real');

E_out = E;
D_out = D;

sys = collect(-(D*A + A*E) - Adot_lin, A); % We want D and E such that -(DA + AE) - Adot_lin = 0
system_terms = children(sys); % gather all terms for all entries in the system

% loop over all matrix entries
for i = 1 : size(system_terms,1)
    for j = 1 : size(system_terms,2)
        disp(['(' num2str(i) ',' num2str(j) ')']);
        terms_ij = system_terms{i,j};
        % loop over all terms in (i,j) entry
        for l = 1:numel(terms_ij)
            
            
            term = terms_ij{l};
        
            all_syms_in_term = symvar(term); % all symbols
            
            % locate E and D if in this term and extract their matrix index 
            
            
            %tic
            %D_syms_in_term = all_syms_in_term(has(all_syms_in_term, D));
            D_syms_in_term = all_syms_in_term(has(all_syms_in_term, D(i,:)));
            %toc
            %tic
            D_str = sym2str(D_syms_in_term); 
            
           % toc
            if ~isempty(D_str)
                tic
                D_idxs = [nan nan]; 
                count = 1;
                for u = 1:numel(D_str)
                    idx = str2double(D_str(u));
                    if ~isnan(idx)
                        D_idxs(count) = idx;
                        count = count + 1;
                    end
                end
                D_idxs;
                
                
                if isequal(D_out(D_idxs(1), D_idxs(2)) , sym(0))
                    continue;
                end
            end
            
            %E_syms_in_term = all_syms_in_term(has(all_syms_in_term, E));
            E_syms_in_term = all_syms_in_term(has(all_syms_in_term, E(:,j)));
            
            E_str = sym2str(E_syms_in_term);
            if ~isempty(E_str)
                E_idxs = [nan nan]; 
                count = 1;
                for u = 1:numel(E_str)
                    idx = str2double(E_str(u));
                    if ~isnan(idx)
                        E_idxs(count) = idx;
                        count = count + 1;
                    end
                end
                E_idxs;
                
                if isequal(E_out(E_idxs(1), E_idxs(2)) , sym(0))
                    continue;
                end
            end
            
            
            
            
            % zero all A, E, D --- leftover is then the variables we need to assign the E and D matrix entries
            leftover_symbols = sum(subs(subs(subs(all_syms_in_term, A, 0*A), E, 0*E), D, 0*D)); 
            
            
            
            if isequal(leftover_symbols, sym(0)) % if there are no leftover symbols then the D and E elements must be zero 
                
                if ~isempty(D_str)
                    D_out(D_idxs(1), D_idxs(2)) = 0;
                end
                
                if ~isempty(E_str)
                    E_out(E_idxs(1), E_idxs(2)) = 0;
                end
            else % if there are leftover symbols, set all A occurences to 1 and solve (D_idxs + leftover) == 0, and set D(idxs) = -leftover but only if not already 0
                term_without_A = subs(term, A, ones(size(A)));
                
                if ~isempty(D_str)
                    
                    if ~isequal(D_out(D_idxs(1), D_idxs(2)) , sym(0))
                        D_out(D_idxs(1), D_idxs(2)) = solve(term_without_A == 0, D_syms_in_term);
                    end
                end
                
                if ~isempty(E_str)
                    if ~isequal(E(E_idxs(1), E_idxs(2)) , sym(0))
                        E_out(E_idxs(1), E_idxs(2)) = solve(term_without_A == 0, E_syms_in_term);
                    end
                end
                
            end
            
            
        end
    end
end

unresolved_terms    = symvar([E_out, D_out]); % check if there are unresolved terms (on diagonal)
unresolved_terms_DE = unresolved_terms(has(unresolved_terms,[D, E]));
if ~isempty(unresolved_terms_DE)
    solution = solve(-(D_out*A + A*E_out) - Adot_lin, unresolved_terms_DE);
    names = fieldnames(solution);
    vals  = struct2cell(solution);

    D_out = subs(D_out, names, vals);
    E_out = subs(E_out, names, vals);
end

D = D_out;
E = E_out;

end